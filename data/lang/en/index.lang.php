<?php

/**
 * Index Seite Sprachvariablen
 * 
 * @author     Stefan Profanter
 * @copyright  Copyright (c) 2013, Stefan Profanter
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License
 * @since      1.0.0-0
 * @version    1.0.0-0
 */
$l = array();

//Tabs
$l['index.tabs.state'] = 'Status';
$l['index.tabs.data'] = 'Data';
$l['index.tabs.remoteRPi'] = 'remote Raspberry Pi';

//Allgemein
$l['index.all.total'] = 'Total';
$l['index.all.usage'] = 'Usage';
$l['index.all.free'] = 'Free';
$l['index.tooltip.edit'] = 'Edit';
$l['index.tooltip.delete'] = 'Delete';

//Systemstatus
$l['index.state.time.box.title'] = 'System time';
$l['index.state.time.box.uptime'] = 'Uptime';
$l['index.state.time.box.lastStart'] = 'last startup';

$l['index.state.system.box.title'] = 'System';
$l['index.state.system.box.cpuusage'] = 'CPU usage';
$l['index.state.system.box.spufreq'] = 'CPU fequency';
$l['index.state.system.box.cputemp'] = 'Core temperature';
$l['index.state.system.box.memoryusage'] = 'Memory';
$l['index.state.system.box.swapusage'] = 'Swap';

$l['index.state.storage.box.title'] = 'Sotrage';
$l['index.state.storage.box.partition'] = 'Partition';
$l['index.state.storage.box.mountpoint'] = 'Mountpoint';
$l['index.state.storage.box.usage'] = 'Usage';

$l['index.state.network.box.title'] = 'Network';
$l['index.state.network.box.interface'] = 'Interface';
$l['index.state.network.box.rx'] = 'Received';
$l['index.state.network.box.tx'] = 'Sent';
$l['index.state.network.box.drop'] = 'Error/Dropped';

$l['index.data.os.box.title'] = 'OS';
$l['index.data.os.box.os'] = 'OS';
$l['index.data.os.box.kernel'] = 'Kernel Version';
$l['index.data.os.box.firmware'] = 'Firmware Version';
$l['index.data.os.box.memory'] = 'Memory structure';
$l['index.data.os.box.sytem'] = 'System';
$l['index.data.os.box.video'] = 'Video';
$l['index.data.os.box.hostname'] = 'Hostname';

$l['index.data.sysdata.box.title'] = 'System data';
$l['index.data.sysdata.box.cpu'] = 'CPU';
$l['index.data.sysdata.box.cpuFeatures'] = 'CPU properties';
$l['index.data.sysdata.box.cpuFreq'] = 'CPU fequency';
$l['index.data.sysdata.box.serial'] = 'Serialnumber';
$l['index.data.sysdata.box.rev'] = 'Revision';

$l['index.data.video.box.title'] = 'Video license';
$l['index.data.video.box.h264'] = 'h264';
$l['index.data.video.box.mpeg2'] = 'MPEG2';
$l['index.data.video.box.vc1'] = 'VC-1';

$l['index.data.usb.box.title'] = 'USB devices';

$l['index.data.pcc.box.title'] = 'PCC Info';
$l['index.data.pcc.box.version'] = 'Version';
$l['index.data.pcc.box.installDate'] = 'Installations Datum';
$l['index.data.pcc.box.memory'] = 'Required memory';
$l['index.data.pcc.box.phpVersion'] = 'PHP Version';

//Remote RPi
$l['index.remote.create.button'] = 'insert RPi';

$l['index.remote.table.name'] = 'Name';
$l['index.remote.table.lasteConnect'] = 'last connection';
$l['index.remote.table.ip'] = 'IP';
$l['index.remote.table.port'] = 'Port';
$l['index.remote.table.options'] = 'Options';

$l['index.remote.create.desc'] = 'Here you can insert a new remote Raspberry Pi for remote management';
$l['index.remote.create.name'] = 'Name';
$l['index.remote.create.name.desc'] = 'enter an identifying name for the remote RasPI';
$l['index.remote.create.ip'] = 'IP address';
$l['index.remote.create.ip.desc'] = 'IP address of the remote RasPI';
$l['index.remote.create.port'] = 'Port';
$l['index.remote.create.port.desc'] = 'Port on which the server listens for connections';

//Einstellungen
$l['index.settings.buttons.reset'] = 'Reset';
$l['index.settings.buttons.save'] = 'Save';
$l['index.settings.buttons.close'] = 'Close';

$l['index.settings.tabs.1'] = 'Common';
$l['index.settings.tabs.2'] = 'Auto Update';
$l['index.settings.tabs.3'] = 'Updates';

$l['index.settings.form.desc.1'] = 'PCC common settings';
$l['index.settings.form.desc.2'] = 'RPi status page settings';
$l['index.settings.form.desc.3'] = 'PCC Update';

$l['index.settings.form.lang.title'] = 'Language';
$l['index.settings.form.lang.desc'] = 'choose the PCC display language';
$l['index.settings.form.lang.values.1'] = 'German';
$l['index.settings.form.lang.values.2'] = 'English';

$l['index.settings.form.autoUpdate.title'] = 'Auto Update';
$l['index.settings.form.autoUpdate.desc'] = 'Automatic status file updates';

$l['index.settings.form.updateIntervall.title'] = 'Update interval';
$l['index.settings.form.updateIntervall.desc'] = 'Update interval';
$l['index.settings.form.updateIntervall.values.1'] = '1 sec.';
$l['index.settings.form.updateIntervall.values.2'] = '2 sec.';
$l['index.settings.form.updateIntervall.values.3'] = '3 sec.';
$l['index.settings.form.updateIntervall.values.4'] = '4 sec.';
$l['index.settings.form.updateIntervall.values.5'] = '5 sec.';
$l['index.settings.form.updateIntervall.values.6'] = '10 sec.';
$l['index.settings.form.updateIntervall.values.7'] = '20 sec.';
$l['index.settings.form.updateIntervall.values.8'] = '30 sec.';
$l['index.settings.form.updateIntervall.values.9'] = '60 sec.';

$l['index.settings.form.searchUpdates.title'] = 'search for updates';
$l['index.settings.form.searchUpdates.desc'] = 'Automatically search for updates';

$l['index.settings.form.searchIntervall.title'] = 'Search interval';
$l['index.settings.form.searchIntervall.desc'] = 'Interval for automatically searching for updates';
$l['index.settings.form.searchIntervall.values.1'] = 'daily';
$l['index.settings.form.searchIntervall.values.2'] = 'weekly';
$l['index.settings.form.searchIntervall.values.3'] = 'monthly';

//Benutzer
$l['index.user.guest'] = 'Guest';
$l['index.user.table.user'] = 'User';
$l['index.user.table.registerDate'] = 'Registered since';
$l['index.user.table.options'] = 'Options';

$l['index.user.buttons.createUser'] = 'Create user';
$l['index.user.buttons.back'] = 'back';
$l['index.user.buttons.reset'] = 'reset';
$l['index.user.buttons.save'] = 'save';

$l['index.user.tabs.1'] = 'User data';
$l['index.user.tabs.2'] = 'User rights';
$l['index.user.tabs.3'] = 'Admin rights';

$l['index.user.tabs.1.dec'] = 'General user data';
$l['index.user.tabs.2.dec'] = 'here you can set up the user rights';
$l['index.user.tabs.3.dec'] = 'here you can set up the administrative user rights';

$l['index.user.form.name'] = 'Username';
$l['index.user.form.name.desc'] = 'Name of new user';
$l['index.user.form.pass'] = 'Password';
$l['index.user.form.pass.desc'] = 'User password';
$l['index.user.form.pass1'] = 'Repeat password';
$l['index.user.form.pass1.desc'] = 'repeat password';

$l['index.user.form.premmission.showState'] = 'Satus page';
$l['index.user.form.premmission.showState.desc'] = 'User can view RasPI status page';
$l['index.user.form.premmission.showData'] = 'Data page';
$l['index.user.form.premmission.showData.desc'] = 'User can view RasPi system properties';
$l['index.user.form.premmission.remoteRPi'] = 'Remote page';
$l['index.user.form.premmission.remoteRPi.desc'] = 'User can view list of remote RasPis';
$l['index.user.form.premmission.showRemoteRPiState'] = 'Remote status';
$l['index.user.form.premmission.showRemoteRPiState.desc'] = 'User can view remote RasPi status page';
$l['index.user.form.premmission.showRemoteRPiData'] = 'Remote data';
$l['index.user.form.premmission.showRemoteRPiData.desc'] = 'User can view remote RasPi system properties';
$l['index.user.form.premmission.useAcpMenu'] = 'Admin menu';
$l['index.user.form.premmission.useAcpMenu.desc'] = 'User can use admin menu';
$l['index.user.form.premmission.acpUsers'] = 'Modify users';
$l['index.user.form.premmission.acpUsers.desc'] = 'User can create/delete users';
$l['index.user.form.premmission.acpRemote'] = 'Manage RPi\'s';
$l['index.user.form.premmission.acpRemote.desc'] = 'User can manage remote Raspberry Pis';
$l['index.user.form.premmission.acpSettings'] = 'Settings';
$l['index.user.form.premmission.acpSettings.desc'] = 'User can change settings';
$l['index.user.form.premmission.acpUpdates'] = 'Updates';
$l['index.user.form.premmission.acpUpdates.desc'] = 'User can search for updates and install them';

//Updates
$l['index.update.title'] = 'Raspberry Pi Control Center Update';
$l['index.update.tab'] = 'Install update';
$l['index.update.tab1'] = 'Chaneglog';
$l['index.update.changelogError'] = 'Chaneglog currently not available';
$l['index.update.searchError'] = 'Couldn\'t search for updates. Update server not responding.';
$l['index.update.startButton'] = 'Start update';
$l['index.update.indexButton'] = 'back to home page';


//Server
$l['index.server.ipInput'] = 'IP address ({1:s}): ';
$l['index.server.ipInvalid'] = 'Invalid IP address!';
$l['index.server.ipArlredyInvalid'] = 'Wrong IP address entered too often';
$l['index.server.port'] = 'Port ({1:s}): ';
$l['index.server.portInvalid'] = 'invalid port';
$l['index.server.portAlredyInvalid'] = 'Wrong port entered too often';
$l['index.server.saveSuccessfully'] = 'Settings successfully saved';
$l['index.server.saveError'] = 'Couldn\'t save settings';
$l['index.server.startServer'] = 'do you want to start the server now? [yes|no]: ';
$l['index.server.yesShort'] = 'y';
$l['index.server.yes'] = 'yes';
$l['index.server.noShort'] = 'n';
$l['index.server.no'] = 'no';
$l['index.server.invalidInput'] = 'Invalid input. Please type "yes" or "no"';
$l['index.server.invalidInput.exit'] = 'Invalid input. Please try again later.';
$l['index.server.updateSearch'] = 'Searching for updates';
$l['index.server.updateSearchError'] = 'Searching for updates failed. Please try again later.';
$l['index.server.updateFound'] = 'Update available';
$l['index.server.notUpdateFound'] = 'No update found. You are up to date.';
$l['index.server.installAlert'] = 'Do you want to install the update now? [yes|no]: ';
$l['index.server.invokeLater'] = 'update not installed. You can do that later.';
$l['index.server.debugActive'] = 'Debug modus activated!';
$l['index.server.start'] = 'Server started. Listening on "{1:s}:{2:s}"';
$l['index.server.shutDown'] = 'Server shut down';
?>
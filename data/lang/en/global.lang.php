<?php

/**
 * Globale Sprachvariablen
 * 
 * @author     Stefan Profanter
 * @copyright  Copyright (c) 2013, Stefan Profanter
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License
 * @since      1.0.0-0
 * @version    1.0.0-0
 */
$l = array();

//Allgemeine
$l['global.tousandsSeparator'] = ',';
$l['global.decimalSeparator'] = '.';

//Sonstige
$l['global.page.title'] = 'Raspberry Pi Control Center';
$l['global.unknown'] = 'unknown';

//Kopfzeile
$l['global.headline.acpMenu'] = 'Administration';
$l['global.headline.login'] = 'login';
$l['global.headline.logout'] = 'logout';
$l['global.headline.logedinAs'] = 'logged in as';

//Login
$l['global.login.title'] = 'Login';
$l['global.login.username'] = 'Username';
$l['global.login.password'] = 'Password';
$l['global.login.reqerror'] = 'Request failed';

//Fußzeile
$l['global.footline.generationTime'] = 'delivered by {1:s} in {2:f:0.0:4}s';

//Formular Elemente
$l['global.form.onoff.on'] = 'On';
$l['global.form.onoff.off'] = 'Off';
$l['global.form.yesno.yes'] = 'Yes';
$l['global.form.yesno.no'] = 'No';

//Allgemeine Formulare
$l['global.form.buttons.reset'] = 'Reset';

//Admin Menu
$l['global.menu.admin.users'] = 'User management';
$l['global.menu.admin.remote'] = 'Remote RPi\'s';
$l['global.menu.admin.settings'] = 'Settings';
$l['global.menu.admin.updates'] = 'Updates';

//Video Aktiviert/Deaktiviert
$l['index.data.video.box.activated'] = 'activated';
$l['index.data.video.box.deactivated'] = 'deactivated';
?>

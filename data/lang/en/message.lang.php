<?php

/**
 * Globale Sprachvariablen
 * 
 * @author     Stefan Profanter
 * @copyright  Copyright (c) 2013, Stefan Profanter
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License
 * @since      1.0.0-0
 * @version    1.0.0-0
 */
$l = array();

//Formular
$l['message.form.invalid'] = 'Invalid inputs';

//Login
$l['message.login.form.username'] = 'Username required';
$l['message.login.form.password'] = 'Password required';
$l['message.login.error'] = 'Invalid username or password';
$l['message.login.success'] = 'Successfully logged in';

//Install Daten loeschen
$l['message.install.deleteData'] = 'You should delete the following files: "install.php" and "install_pcc.zip"';
$l['message.install.deleteData.now'] = 'delete now';

//Update verfuegbar
$l['message.update.alert'] = 'New update available.';
$l['message.update.alert1'] = 'Update now';

//Logout
$l['message.logout.success'] = 'Logged out';

//Berechtigung
$l['message.accessdenied'] = 'Access denied';

//Remote Raspberry Pi
$l['message.remoterpi.notexists'] = 'Raspberry Pi not found';
$l['message.remoterpi.noconnection'] = 'connection error';

//Einstellungen
$l['message.settings.success'] = 'Settings successfully saved. Reload the page to see changes';
$l['message.settings.error'] = 'Invalid inputs in the following fields:';
$l['message.settings.inputError.language'] = 'No or invalid language selected';
$l['message.settings.inputError.autoUpdateStatus'] = 'Invalid auto update settings';
$l['message.settings.inputError.updateIntervall'] = 'No or invalid update interval';
$l['message.settings.inputError.searchUpdates'] = 'Invalid settings for auto update search';
$l['message.settings.inputError.searchIntervall'] = 'No or invalid search interval';

//Benutzer
$l['message.user.success'] = 'User successfully created';
$l['message.user.error'] = 'Invalid inputs in the following fields:';
$l['message.user.create.inputError.name'] = 'Username must be between 3 and 25 characters.';
$l['message.user.create.inputError.doubleName'] = 'Username already taken';
$l['message.user.create.inputError.password'] = 'Password must be between 5 and 20 characters';
$l['message.user.create.inputError.showState'] = 'No or invalid status page selected';
$l['message.user.create.inputError.showData'] = 'No or invalid properties page selected';
$l['message.user.create.inputError.remoteRPi'] = 'No or invalid remote page selected';
$l['message.user.create.inputError.showRemoteRPiState'] = 'No or invalid remote status selected';
$l['message.user.create.inputError.showRemoteRPiData'] = 'No or invalid remote properties selected';
$l['message.user.create.inputError.useAcpMenu'] = 'No or invalid admin menu selected';
$l['message.user.create.inputError.acpUsers'] = 'No or invalid user management selected';
$l['message.user.create.inputError.acpRemote'] = 'No or invalid RPis page selected';
$l['message.user.create.inputError.acpSettings'] = 'No or invalid settings page selected';
$l['message.user.create.inputError.acpUpdates'] = 'No or invalid update page selected';
$l['message.user.create.error'] = 'Error creating user.';

$l['message.user.edit.success'] = 'User successfully created';
$l['message.user.edit.error'] = 'Couldn\'t create user';
$l['message.user.edit.notExists'] = 'User not existing';

$l['message.user.del.success'] = 'User deleted';
$l['message.user.del.error'] = 'User couldn\'t be deleted or doesn\'t exist';

//Remote RPi
$l['message.remote.success'] = 'RasPI successfully inserted';
$l['message.remote.error'] = 'Couldn\'t insert RasPI';
$l['message.remote.create.error'] = 'Invalid inputs in the following fields:';
$l['message.remote.create.inputError.name'] = 'Name of RasPI must be given and less than 60 characters';
$l['message.remote.create.inputError.ip0'] = 'Invalid IP-address part in 1st Block';
$l['message.remote.create.inputError.ip1'] = 'Invalid IP-address part in 2nd Block';
$l['message.remote.create.inputError.ip2'] = 'Invalid IP-address part in 3rd Block';
$l['message.remote.create.inputError.ip3'] = 'Invalid IP-address part in 4th Block';
$l['message.remote.create.inputError.port'] = 'Invalid Port';

$l['message.remote.edit.success'] = 'RasPI modified';
$l['message.remote.edit.error'] = 'Couldn\'t modify RasPI';
$l['message.remote.edit.notExists'] = 'RasPI not found';

$l['message.remote.del.success'] = 'RasPI successfully deleted';
$l['message.remote.del.error'] = 'Couldn\'d delete RasPI. Not existing or not found.';

$l['message.update.backupInfo'] = 'If the PCC doen\'t work as intendet after an update you can restore the current version from the backup created before the update.';
$l['message.update.successfully'] = 'Update installed successfully';
$l['message.update.error.10'] = 'Couldn\'t create backup file';
$l['message.update.error.20'] = 'Couldn\'t open ZIP archive';
$l['message.update.error.30'] = 'Couldn\'t create backup file';
$l['message.update.error.40'] = 'Couldn\'t create update folder';
$l['message.update.error.50'] = 'Couldn\'t create update file';
$l['message.update.error.60'] = 'Couldn\'t download update file';
$l['message.update.error.70'] = 'Couldn\'t extract update file';
$l['message.update.error.80'] = $l['index.update.error.70'];
$l['message.update.error.90'] = 'Couldn\'t find update archive';
$l['message.update.error.100'] = 'Couldn\'t create folder';
$l['message.update.error.110'] = 'Couldn\'t create file';
$l['message.update.error.120'] = 'Couldn\'t write to "version.xml"';
$l['message.update.error.130'] = 'New version available';

//Install als Server oder WebApp
$l['message.pcc.webAppError'] = 'PCC has been installed as WebApp and can\'t be launched as Server';
$l['message.pcc.serverError'] = 'PCC has been installed as Server and can\'t be launched as WebApp';
?>

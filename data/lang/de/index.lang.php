<?php

/**
 * Index Seite Sprachvariablen
 * 
 * @author     Oliver Kleditzsch
 * @copyright  Copyright (c) 2013, Oliver Kleditzsch
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License
 * @since      1.0.0-0
 * @version    1.0.0-0
 */
$l = array();

//Tabs
$l['index.tabs.state'] = 'Status';
$l['index.tabs.data'] = 'Daten';
$l['index.tabs.remoteRPi'] = 'remote Raspberry Pi';

//Allgemein
$l['index.all.total'] = 'Gesamt';
$l['index.all.usage'] = 'Belegt';
$l['index.all.free'] = 'Frei';
$l['index.tooltip.edit'] = 'bearbeiten';
$l['index.tooltip.delete'] = 'löschen';

//Systemstatus
$l['index.state.time.box.title'] = 'Systemzeit';
$l['index.state.time.box.uptime'] = 'Laufzeit';
$l['index.state.time.box.lastStart'] = 'letzter Start';

$l['index.state.system.box.title'] = 'System';
$l['index.state.system.box.cpuusage'] = 'CPU Auslastung';
$l['index.state.system.box.spufreq'] = 'CPU Takt';
$l['index.state.system.box.cputemp'] = 'Core Temperatur';
$l['index.state.system.box.memoryusage'] = 'Arbeitsspeicher';
$l['index.state.system.box.swapusage'] = 'Swap';

$l['index.state.storage.box.title'] = 'Systemspeicher';
$l['index.state.storage.box.partition'] = 'Partition';
$l['index.state.storage.box.mountpoint'] = 'Mountpoint';
$l['index.state.storage.box.usage'] = 'Auslastung';

$l['index.state.network.box.title'] = 'Netzwerk';
$l['index.state.network.box.interface'] = 'Schnittstelle';
$l['index.state.network.box.rx'] = 'Empfangen';
$l['index.state.network.box.tx'] = 'Gesendet';
$l['index.state.network.box.drop'] = 'Fehler/Verloren';

$l['index.data.os.box.title'] = 'Betriebssytem';
$l['index.data.os.box.os'] = 'Betriebssytem';
$l['index.data.os.box.kernel'] = 'Kernel Version';
$l['index.data.os.box.firmware'] = 'Firmware Version';
$l['index.data.os.box.memory'] = 'Speicheraufteilung';
$l['index.data.os.box.sytem'] = 'System';
$l['index.data.os.box.video'] = 'Video';
$l['index.data.os.box.hostname'] = 'Hostname';

$l['index.data.sysdata.box.title'] = 'Systemdaten';
$l['index.data.sysdata.box.cpu'] = 'CPU';
$l['index.data.sysdata.box.cpuFeatures'] = 'CPU Eigenschaften';
$l['index.data.sysdata.box.cpuFreq'] = 'CPU Takt';
$l['index.data.sysdata.box.serial'] = 'Seriennummer';
$l['index.data.sysdata.box.rev'] = 'Revision';

$l['index.data.video.box.title'] = 'Video Lizensen';
$l['index.data.video.box.h264'] = 'h264';
$l['index.data.video.box.mpeg2'] = 'MPEG2';
$l['index.data.video.box.vc1'] = 'VC-1';

$l['index.data.usb.box.title'] = 'USB Geräte';

$l['index.data.pcc.box.title'] = 'PCC Daten';
$l['index.data.pcc.box.version'] = 'Version';
$l['index.data.pcc.box.installDate'] = 'Installations Datum';
$l['index.data.pcc.box.memory'] = 'Speicherbedarf';
$l['index.data.pcc.box.phpVersion'] = 'PHP Version';

//Remote RPi
$l['index.remote.create.button'] = 'RPi Eintragen';

$l['index.remote.table.name'] = 'Name';
$l['index.remote.table.lasteConnect'] = 'letzte Verbindung';
$l['index.remote.table.ip'] = 'IP';
$l['index.remote.table.port'] = 'Port';
$l['index.remote.table.options'] = 'Optionen';

$l['index.remote.create.desc'] = 'Hier kannst du einen neuen Raspberry Pi zur externen Überwachung eintragen';
$l['index.remote.create.name'] = 'Name';
$l['index.remote.create.name.desc'] = 'gib einen Aussagekräftigen Namen für den RPi an';
$l['index.remote.create.ip'] = 'IP Adresse';
$l['index.remote.create.ip.desc'] = 'Die IP Adresse mit der, der Raspberry Pi, im Netzwerk erreichbar ist';
$l['index.remote.create.port'] = 'Port';
$l['index.remote.create.port.desc'] = 'Der Port auf dem der Server auf Anfragen wartet';

//Einstellungen
$l['index.settings.buttons.reset'] = 'Reset';
$l['index.settings.buttons.save'] = 'Speichern';
$l['index.settings.buttons.close'] = 'Schliesen';

$l['index.settings.tabs.1'] = 'Allgemein';
$l['index.settings.tabs.2'] = 'Auto Aktualisierung';
$l['index.settings.tabs.3'] = 'Updates';

$l['index.settings.form.desc.1'] = 'Allgemeine Einstellungen des PCC';
$l['index.settings.form.desc.2'] = 'Einstellungen der RPi Status Seite';
$l['index.settings.form.desc.3'] = 'Aktualisierung des PCC';

$l['index.settings.form.lang.title'] = 'Sprache';
$l['index.settings.form.lang.desc'] = 'legt fest in welcher Sprache das PCC dargestell wird';
$l['index.settings.form.lang.values.1'] = 'Deutsch';
$l['index.settings.form.lang.values.2'] = 'Englisch';

$l['index.settings.form.autoUpdate.title'] = 'Auto Aktualisierung';
$l['index.settings.form.autoUpdate.desc'] = 'Automatische Aktualisierung der Statusdaten';

$l['index.settings.form.updateIntervall.title'] = 'Update Intervall';
$l['index.settings.form.updateIntervall.desc'] = 'Aktualisierungsintervall';
$l['index.settings.form.updateIntervall.values.1'] = '1 sek.';
$l['index.settings.form.updateIntervall.values.2'] = '2 sek.';
$l['index.settings.form.updateIntervall.values.3'] = '3 sek.';
$l['index.settings.form.updateIntervall.values.4'] = '4 sek.';
$l['index.settings.form.updateIntervall.values.5'] = '5 sek.';
$l['index.settings.form.updateIntervall.values.6'] = '10 sek.';
$l['index.settings.form.updateIntervall.values.7'] = '20 sek.';
$l['index.settings.form.updateIntervall.values.8'] = '30 sek.';
$l['index.settings.form.updateIntervall.values.9'] = '60 sek.';

$l['index.settings.form.searchUpdates.title'] = 'nach Updates suchen';
$l['index.settings.form.searchUpdates.desc'] = 'Automatisch nach Updates suchen';

$l['index.settings.form.searchIntervall.title'] = 'Suchintervall';
$l['index.settings.form.searchIntervall.desc'] = 'Intervall in welchen abständen nach Updates gesucht werden soll';
$l['index.settings.form.searchIntervall.values.1'] = 'täglich';
$l['index.settings.form.searchIntervall.values.2'] = 'wöchentlich';
$l['index.settings.form.searchIntervall.values.3'] = 'monatlich';

//Benutzer
$l['index.user.guest'] = 'Gast';
$l['index.user.table.user'] = 'Benuter';
$l['index.user.table.registerDate'] = 'Registriert seit';
$l['index.user.table.options'] = 'Optionen';

$l['index.user.buttons.createUser'] = 'Benuter erstellen';
$l['index.user.buttons.back'] = 'zurück';
$l['index.user.buttons.reset'] = 'Reset';
$l['index.user.buttons.save'] = 'Speichern';

$l['index.user.tabs.1'] = 'Benuterdaten';
$l['index.user.tabs.2'] = 'Benutzerrechte';
$l['index.user.tabs.3'] = 'Adminrechte';

$l['index.user.tabs.1.dec'] = 'Allgemeine Benutzerdaten';
$l['index.user.tabs.2.dec'] = 'hier kannst du festlegen was der Benutzer für Berechtigungen hat';
$l['index.user.tabs.3.dec'] = 'hier kannst du festlegen was der Benutzer für Administrative Berechtigungen hat';

$l['index.user.form.name'] = 'Benutzername';
$l['index.user.form.name.desc'] = 'Name des neuen Benutzers';
$l['index.user.form.pass'] = 'Passwort';
$l['index.user.form.pass.desc'] = 'Passwort des neuen Benutzers';
$l['index.user.form.pass1'] = 'Passwort wiederholung';
$l['index.user.form.pass1.desc'] = 'gib das Passwort nochmals ein um schreibfehler zu vermeiden';

$l['index.user.form.premmission.showState'] = 'Statusseite';
$l['index.user.form.premmission.showState.desc'] = 'Der Benutzer kann die Statusdaten des Raspberry Pi ansehen';
$l['index.user.form.premmission.showData'] = 'Datenseite';
$l['index.user.form.premmission.showData.desc'] = 'Der Benutzer kann die Kenndaten des Raspberry Pi ansehen';
$l['index.user.form.premmission.remoteRPi'] = 'Remote Seite';
$l['index.user.form.premmission.remoteRPi.desc'] = 'Der Benutzer kann die externen Raspberry Pi\'s ansehen';
$l['index.user.form.premmission.showRemoteRPiState'] = 'Remote Status';
$l['index.user.form.premmission.showRemoteRPiState.desc'] = 'Der Benutzer kann die externen Statusdaten der Raspberry Pi\'s ansehen';
$l['index.user.form.premmission.showRemoteRPiData'] = 'Remote Daten';
$l['index.user.form.premmission.showRemoteRPiData.desc'] = 'Der Benutzer kann die externen Kenndaten der Raspberry Pi\'s ansehen';
$l['index.user.form.premmission.useAcpMenu'] = 'Admin Menü';
$l['index.user.form.premmission.useAcpMenu.desc'] = 'Der Benutzer kann das Administrations Menü verwenden';
$l['index.user.form.premmission.acpUsers'] = 'Benutzer verwalten';
$l['index.user.form.premmission.acpUsers.desc'] = 'Der Benutzer kann Benutzer verwalten';
$l['index.user.form.premmission.acpRemote'] = 'Externe RPi\'s verwalten';
$l['index.user.form.premmission.acpRemote.desc'] = 'Der Benutzer kann die externen Raspberry Pi\'s verwalten';
$l['index.user.form.premmission.acpSettings'] = 'Einstellungen';
$l['index.user.form.premmission.acpSettings.desc'] = 'Der Benutzer kann EInstellungen verändern';
$l['index.user.form.premmission.acpUpdates'] = 'Updates';
$l['index.user.form.premmission.acpUpdates.desc'] = 'Der Benutzer kann nach Updates suchen und Updates installieren';

//Updates
$l['index.update.title'] = 'Raspberry Pi Control Center Update';
$l['index.update.tab'] = 'Update Installieren';
$l['index.update.tab1'] = 'Chaneglog';
$l['index.update.changelogError'] = 'Chaneglog zur zeit nicht verfügbar';
$l['index.update.searchError'] = 'Es konnte nicht nach Updates gesucht werden da der Update Server nicht antwortet';
$l['index.update.startButton'] = 'Update starten';
$l['index.update.indexButton'] = 'zurück zur Startseite';


//Server
$l['index.server.ipInput'] = 'IP Adresse ({1:s}): ';
$l['index.server.ipInvalid'] = 'ungültige IP Adresse!';
$l['index.server.ipArlredyInvalid'] = 'du hast zu oft eine ungültige Adresse eingegeben';
$l['index.server.port'] = 'Port ({1:s}): ';
$l['index.server.portInvalid'] = 'ungültiger Port';
$l['index.server.portAlredyInvalid'] = 'du hast zu oft einen ungültigen Port eingegeben';
$l['index.server.saveSuccessfully'] = 'die Einstellungen wurden erfolgreich gespeichert';
$l['index.server.saveError'] = 'die Einstellungen konnten nicht gespeichert werden';
$l['index.server.startServer'] = 'willst du den Server jetzt starten? [ja|nein]: ';
$l['index.server.yesShort'] = 'j';
$l['index.server.yes'] = 'ja';
$l['index.server.noShort'] = 'n';
$l['index.server.no'] = 'nein';
$l['index.server.invalidInput'] = 'ungültige Eingabe, gib ja oder nein ein';
$l['index.server.invalidInput.exit'] = 'Ungültige Eingabe, versuch es später nochmal';
$l['index.server.updateSearch'] = 'suche nach Updates';
$l['index.server.updateSearchError'] = 'Update Suche fehlgeschlagen, versuche es später noch einmal';
$l['index.server.updateFound'] = 'es wurde ein Update gefunden';
$l['index.server.notUpdateFound'] = 'es wurde kein Update gefunden';
$l['index.server.installAlert'] = 'willst du das Update jetzt installieren? [ja|nein]: ';
$l['index.server.invokeLater'] = 'das Update wurde jetzt nicht installiert, das kannst du später noch machen';
$l['index.server.debugActive'] = 'Debug modus aktiviert!';
$l['index.server.start'] = 'der Server wurde erfolgreich gestartet unter "{1:s}:{2:s}"';
$l['index.server.shutDown'] = 'der Server wurde erfolgreich beendet';
?>
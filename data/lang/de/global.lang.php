<?php

/**
 * Globale Sprachvariablen
 * 
 * @author     Oliver Kleditzsch
 * @copyright  Copyright (c) 2013, Oliver Kleditzsch
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License
 * @since      1.0.0-0
 * @version    1.0.0-0
 */
$l = array();

//Allgemeine
$l['global.tousandsSeparator'] = '.';
$l['global.decimalSeparator'] = ',';

//Sonstige
$l['global.page.title'] = 'Raspberry Pi Control Center';
$l['global.unknown'] = 'unbekannt';

//Kopfzeile
$l['global.headline.acpMenu'] = 'Administration';
$l['global.headline.login'] = 'anmelden';
$l['global.headline.logout'] = 'abmelden';
$l['global.headline.logedinAs'] = 'angemeldet als';

//Login
$l['global.login.title'] = 'Anmelden';
$l['global.login.username'] = 'Benutzername';
$l['global.login.password'] = 'Passwort';
$l['global.login.reqerror'] = 'Anfrage Fehlgeschlagen';

//Fußzeile
$l['global.footline.generationTime'] = 'erstellt mit {1:s} in {2:f:0.0:4}s';

//Formular Elemente
$l['global.form.onoff.on'] = 'An';
$l['global.form.onoff.off'] = 'Aus';
$l['global.form.yesno.yes'] = 'Ja';
$l['global.form.yesno.no'] = 'Nein';

//Allgemeine Formulare
$l['global.form.buttons.reset'] = 'Zurücksetzen';

//Admin Menu
$l['global.menu.admin.users'] = 'Benutzerverwaltung';
$l['global.menu.admin.remote'] = 'Remote RPi\'s';
$l['global.menu.admin.settings'] = 'Einstellungen';
$l['global.menu.admin.updates'] = 'Updates';

//Video Aktiviert/Deaktiviert
$l['index.data.video.box.activated'] = 'aktiviert';
$l['index.data.video.box.deactivated'] = 'deaktiviert';
?>

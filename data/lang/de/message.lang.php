<?php

/**
 * Globale Sprachvariablen
 * 
 * @author     Oliver Kleditzsch
 * @copyright  Copyright (c) 2013, Oliver Kleditzsch
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License
 * @since      1.0.0-0
 * @version    1.0.0-0
 */
$l = array();

//Formular
$l['message.form.invalid'] = 'Ungültige Eingaben';

//Login
$l['message.login.form.username'] = 'Der Benutzername muss angegeben werden und zwischen 3 und 25 Zeichen lang sein';
$l['message.login.form.password'] = 'Das Passwort muss angegeben werden und zwischen 5 und 20 Zeichen lang sein';
$l['message.login.error'] = 'Falscher Benutzername oder falsches Passwort';
$l['message.login.success'] = 'Du hast dich erfolgreich angemeldet';

//Install Daten loeschen
$l['message.install.deleteData'] = 'Du solltest die "install.php" und die "install_pcc.zip" löschen';
$l['message.install.deleteData.now'] = 'jetzt löschen';

//Update verfuegbar
$l['message.update.alert'] = 'es steht ein neues Update zum download zur verfügung';
$l['message.update.alert1'] = 'Update jetzt durchführen';

//Logout
$l['message.logout.success'] = 'Du hast dich abgemeldet';

//Berechtigung
$l['message.accessdenied'] = 'Unzurechende Berechtigung';

//Remote Raspberry Pi
$l['message.remoterpi.notexists'] = 'Der Raspberry Pi ist unbekannt';
$l['message.remoterpi.noconnection'] = 'Verbindungsfehler';

//Einstellungen
$l['message.settings.success'] = 'Die Einstellungen wurden erfolgreich gespeichert. Du musst die Seite neu Laden damit alle Einstellungen Aktiv werden.';
$l['message.settings.error'] = 'Fehlerhafte Eingaben in folgenden Feldern:';
$l['message.settings.inputError.language'] = 'keine oder nicht existierende Sprache ausgewählt';
$l['message.settings.inputError.autoUpdateStatus'] = 'Ungültige Eingabe für die Automatische Aktualisierungs Einstellung';
$l['message.settings.inputError.updateIntervall'] = 'kein oder ungültiger Updateintervall ausgewählt';
$l['message.settings.inputError.searchUpdates'] = 'Ungültige Eingabe für die Automatische Update Such Einstellung';
$l['message.settings.inputError.searchIntervall'] = 'kein oder ungültiger Suchintervall ausgewählt';

//Benutzer
$l['message.user.success'] = 'Der Benutzer wurde erfolgreich erstellt';
$l['message.user.error'] = 'Fehlerhafte Eingaben in folgenden Feldern:';
$l['message.user.create.inputError.name'] = 'Der Benutzername muss zwischen 3 und 25 Zeichen lang sein.';
$l['message.user.create.inputError.doubleName'] = 'Der Benutzername ist schon vergeben';
$l['message.user.create.inputError.password'] = 'Das Passwort muss zwischen 5 und 20 Zeichen lang sein';
$l['message.user.create.inputError.showState'] = 'kein oder ungültiger Statusseite ausgewählt';
$l['message.user.create.inputError.showData'] = 'kein oder ungültiger Datenseite ausgewählt';
$l['message.user.create.inputError.remoteRPi'] = 'kein oder ungültiger Remote Seite ausgewählt';
$l['message.user.create.inputError.showRemoteRPiState'] = 'kein oder ungültiger Remote Status ausgewählt';
$l['message.user.create.inputError.showRemoteRPiData'] = 'kein oder ungültiger Remote Daten ausgewählt';
$l['message.user.create.inputError.useAcpMenu'] = 'kein oder ungültiger Admin Menü ausgewählt';
$l['message.user.create.inputError.acpUsers'] = 'kein oder ungültiger Benutzerverwaltung ausgewählt';
$l['message.user.create.inputError.acpRemote'] = 'kein oder ungültiger Externe RPi\s verwalten ausgewählt';
$l['message.user.create.inputError.acpSettings'] = 'kein oder ungültiger Einstellungen ausgewählt';
$l['message.user.create.inputError.acpUpdates'] = 'kein oder ungültiger Updates ausgewählt';
$l['message.user.create.error'] = 'Der Benutzer konnte nicht erstellt werden';

$l['message.user.edit.success'] = 'Der Benutzer wurde erfolgreich bearbeitet';
$l['message.user.edit.error'] = 'Der Benutzer konnte nicht bearbeitet werden';
$l['message.user.edit.notExists'] = 'Der Benutzer existiert nicht';

$l['message.user.del.success'] = 'Der Benutzer wurde erfolgreich gelöscht';
$l['message.user.del.error'] = 'Der Benutzer konnte nicht gelöscht werden oder existiert nicht';

//Remote RPi
$l['message.remote.success'] = 'Der Raspberry Pi wurde erfolgreich eingetragen';
$l['message.remote.error'] = 'Der Raspberry Pi konnte nicht eingetragen werden';
$l['message.remote.create.error'] = 'Fehlerhafte Eingaben in folgenden Feldern:';
$l['message.remote.create.inputError.name'] = 'Der Name des Raspberry Pi muss angegeben werden und darf MAximal 60 Zeichen Lang sein';
$l['message.remote.create.inputError.ip0'] = 'Ungültiger Wert der IP Adresse im ersten Block';
$l['message.remote.create.inputError.ip1'] = 'Ungültiger Wert der IP Adresse im zweiten Block';
$l['message.remote.create.inputError.ip2'] = 'Ungültiger Wert der IP Adresse im dritten Block';
$l['message.remote.create.inputError.ip3'] = 'Ungültiger Wert der IP Adresse im vierten Block';
$l['message.remote.create.inputError.port'] = 'Ungültiger Port';

$l['message.remote.edit.success'] = 'Der Raspberry Pi wurde erfolgreich bearbeitet';
$l['message.remote.edit.error'] = 'Der Raspberry Pi konnte nicht bearbeitet werden';
$l['message.remote.edit.notExists'] = 'Der Raspberry Pi existiert nicht';

$l['message.remote.del.success'] = 'Der Raspberry Pi wurde erfolgreich gelöscht';
$l['message.remote.del.error'] = 'Der Raspberry Pi konnte nicht gelöscht werden oder existiert nicht';

$l['message.update.backupInfo'] = 'sollte das PCC nach dem fehlgeschlagenen Update nicht mehr zufriedenstellend funktionieren, kannst du das Backup welches vor dem Update erstellt wurde wiederherstellen';
$l['message.update.successfully'] = 'das Update wurde erfolgreich installiert';
$l['message.update.error.10'] = 'die Backupdatei konnte nicht erstellt werden';
$l['message.update.error.20'] = 'das Zip Archiv konnte nicht geöffnet werden';
$l['message.update.error.30'] = 'das erstellen der Backupdatei ist fehlgeschlagen';
$l['message.update.error.40'] = 'der Update Ordner konnte nicht erstellt werden';
$l['message.update.error.50'] = 'die Update Datei konnte nicht erstellt werden';
$l['message.update.error.60'] = 'die Update Datei konnte nicht vom Server geladen werden';
$l['message.update.error.70'] = 'das Update ARchiv konnte nicht entpackt werden';
$l['message.update.error.80'] = $l['index.update.error.70'];
$l['message.update.error.90'] = 'das Update Archiv konnte nicht gefunden werden';
$l['message.update.error.100'] = 'ein Ordner konnte nicht erstellt werden';
$l['message.update.error.110'] = 'ein Datei konnte nicht kopiert werden';
$l['message.update.error.120'] = 'die "version.xml" konnte nicht geschrieben werden';
$l['message.update.error.130'] = 'es ist keine neue Version verfügbar';

//Install als Server oder WebApp
$l['message.pcc.webAppError'] = 'Das PCC wurde als WebApp installiert und kann nicht als Server gestartet werden';
$l['message.pcc.serverError'] = 'Das PCC wurde als Server installiert und kann nicht als WebApp verwendet werden';
?>

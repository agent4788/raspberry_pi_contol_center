<?php
/**
 * Globale HTML Metadaten
 * 
 * @author     Oliver Kleditzsch
 * @copyright  Copyright (c) 2013, Oliver Kleditzsch
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License
 * @since      1.0.0-0
 * @version    1.0.0-0
 */
?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript" src="./inc/js/jquery-2.0.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="./inc/css/redmond/jquery-ui-1.10.3.custom.min.css" />
<script type="text/javascript" src="./inc/js/jquery-ui-1.10.3.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="inc/css/style.css" />
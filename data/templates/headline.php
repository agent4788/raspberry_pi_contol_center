<?php
/**
 * Kopfzeile des PCC
 * 
 * @author     Oliver Kleditzsch
 * @copyright  Copyright (c) 2013, Oliver Kleditzsch
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License
 * @since      1.0.0-0
 * @version    1.0.0-0
 */
?>
<div id="headline">
    <div>
        <?php if (PCC::getUser()->checkPremission('useAcpMenu')) { ?>
            <button id="display_acp_menu"><?php echo PCC::l()->val('global.headline.acpMenu'); ?></button>
        <?php } ?>
    </div>
    <div id="headline_left">
        <div id="headline_logo"><a href="index.php?page=index"><?php echo PCC::l()->val('global.page.title'); ?></a></div>
    </div>
    <div id="headline_right">
        <div>
            <?php
            if (PCC::getUser() instanceof Visitor) {
                echo '<a href="#" id="login_link">' . PCC::l()->val('global.headline.login') . '</a>';
            } else {
                echo '<span>' . PCC::l()->val('global.headline.logedinAs') . ' ' . String::encodeHTML(PCC::getUser()->getName()) . ' | <a href="index.php?action=logout" id="logoutLink">' . PCC::l()->val('global.headline.logout') . '</a>';
            }
            ?>
        </div>
    </div>
</div>
<?php if (PCC::getUser()->checkPremission('useAcpMenu')) { ?>
    <ul id="acp_menu">
        <?php if (PCC::getUser()->checkPremission('acpUsers')) { ?>
            <li><a href="#" id="acp_menu_users"><?php echo PCC::l()->val('global.menu.admin.users'); ?></a></li>
        <?php } ?>
        <?php if (PCC::getUser()->checkPremission('acpRemote')) { ?>
            <li><a href="#" id="acp_menu_remote"><?php echo PCC::l()->val('global.menu.admin.remote'); ?></a></li>
        <?php } ?>
        <?php if (PCC::getUser()->checkPremission('useAcpMenu')) { ?>
            <li><a href="#" id="acp_menu_settings"><?php echo PCC::l()->val('global.menu.admin.settings'); ?></a></li>
            <?php } ?>
    </ul>
    <script type="text/javascript">
        //Formulare zuruecksetzen
        function resetForm(id) {
            $(id).each(function() {
                this.reset();
            });
        }

        $('#display_acp_menu').button({
            icons: {
                secondary: "ui-icon-triangle-1-s"
            }
        }).css({
            position: "absolute",
            top: "32px",
            right: "24px"
        });
        $('#acp_menu').menu().css({'z-index': '100'});
        $('#display_acp_menu').click(function(e) {
            pos = $(this).offset();
            $('#acp_menu').css({
                top: pos.top + 29 + 'px',
                left: pos.left + 'px'
            });
            $('#acp_menu').toggleClass('visible');
        });
        $('#acp_menu').mouseleave(function() {
            $('#acp_menu').toggleClass('visible');
        });
        $(function() {
            //Menuefunktionen

            //Einstellungen
            $('#acp_menu_settings').click(function() {

                //Daten holen
                $.get('index.php?ajax=settings', function(data, textStatus, jqXHR) {
                    $('#settings_editor').html(data);
                });
                //Fenster anzeigen
                $('#settings_editor').dialog({
                    modal: true,
                    resizable: false,
                    width: 750,
                    position: {my: "center top", at: "center bottom", of: $('#headline')},
                    buttons: {
                        '<?php echo PCC::l()->val('index.settings.buttons.reset'); ?>': function() {

                            resetForm('#settings_form');
                        },
                        '<?php echo PCC::l()->val('index.settings.buttons.save'); ?>': function() {

                            //Anmeldedaten an den Server schicken
                            var $form = $('#settings_form');
                            var $inputs = $form.find("input, select, button, textarea");
                            var serializedData = $form.serialize();
                            $inputs.prop("disabled", true);

                            request = $.ajax({
                                url: "index.php?ajax=settings",
                                type: "post",
                                data: serializedData
                            });

                            request.done(function(response, textStatus, jqXHR) {
                                //Antwort vom Server
                                $('#settings_editor').html(response);
                            });

                            request.fail(function(jqXHR, textStatus, errorThrown) {
                                //Anfrage Fehlgeschlagen

                            });

                            request.always(function() {
                                $inputs.prop("disabled", false);
                            });
                        }
                    }
                });
            });

            $('#acp_menu_users').click(function() {

                //Daten holen
                $.get('index.php?ajax=userlist', function(data, textStatus, jqXHR) {
                    $('#user_editor').html(data);
                });
                //Fenster anzeigen
                $('#user_editor').dialog({
                    modal: true,
                    resizable: false,
                    width: 750,
                    position: {my: "center top", at: "center bottom", of: $('#headline')},
                    buttons: {
                        '<?php echo PCC::l()->val('index.user.buttons.createUser'); ?>': function() {

                            $.get('index.php?ajax=userlist&req=create', function(data, textStatus, jqXHR) {
                                $('#user_editor').html(data);
                            });
                        }
                    }
                });
            });

            $('#acp_menu_remote').click(function() {

                //Daten holen
                $.get('index.php?ajax=rrpiacp', function(data, textStatus, jqXHR) {
                    $('#remote_editor').html(data);
                });
                //Fenster anzeigen
                $('#remote_editor').dialog({
                    modal: true,
                    resizable: false,
                    width: 750,
                    position: {my: "center top", at: "center bottom", of: $('#headline')},
                    buttons: {
                        '<?php echo PCC::l()->val('index.remote.create.button'); ?>': function() {

                            $.get('index.php?ajax=rrpiacp&req=create', function(data, textStatus, jqXHR) {
                                $('#remote_editor').html(data);
                            });
                        }
                    }
                });
            });
        });
    </script>
<?php } ?>
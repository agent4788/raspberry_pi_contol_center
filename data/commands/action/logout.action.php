<?php

/**
 * meldet den User ab
 * 
 * @author     Oliver Kleditzsch
 * @copyright  Copyright (c) 2013, Oliver Kleditzsch
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License
 * @since      1.0.0-0
 * @version    1.0.0-0
 */
Login::userLogout();

PCC::getResponse()->setBody('');
PCC::getResponse()->addHeader('Location', 'index.php');
?>

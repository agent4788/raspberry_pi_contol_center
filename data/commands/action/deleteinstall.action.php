<?php

/**
 * loescht die Installations Dateien
 * 
 * @author     Oliver Kleditzsch
 * @copyright  Copyright (c) 2013, Oliver Kleditzsch
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License
 * @since      1.0.0-0
 * @version    1.0.0-0
 */
if (PCC::getUser()->isOriginator()) {

//Dateien loschen
    @unlink(BASE_DIR . 'install.php');
    @unlink(BASE_DIR . 'install_pcc.zip');
}

PCC::getResponse()->setBody('');
PCC::getResponse()->addHeader('Location', 'index.php');
?>

<?php
/**
 * Update Installation
 * 
 * @author     Oliver Kleditzsch
 * @copyright  Copyright (c) 2013, Oliver Kleditzsch
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License
 * @since      1.0.0-0
 * @version    1.0.0-0
 */
PCC::l()->loadModule('index');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>
        <title><?php echo PCC::l()->val('index.update.title'); ?></title>
        <?php require_once(TEMPLATES . 'header.php'); ?>
        <script type="text/javascript">
            $(function() {
                $('#content_tabs').tabs();
            });
        </script>
    </head>
    <body>
        <div id="headline">
            <div id="headline_left">
                <div id="headline_logo"><a href="index.php"><?php echo PCC::l()->val('index.update.title'); ?></a></div>
            </div>
        </div>
        <?php
        if (PCC::getUser()->checkPremission('acpUpdates')) {

            if (PCC::getRequest()->issetParam('agreement') && PCC::getRequest()->getParam('agreement', Request::GET, Request::BOOL)) {

                //Update Durchfuehren
                ?>
                <div id="content_box">
                    <div id="content_tabs">
                        <ul>
                            <li id="state"><a href="#tabs-1"><?php echo PCC::l()->val('index.update.tab'); ?></a></li>
                        </ul>
                        <div id="tabs-1">
                            <?php
                            $updateCode = Update::installUpdate();
                            if ($updateCode == 2000) {

                                //Update erfolgreich
                                ?>
                                <div>
                                    <div class="message_successfully">
                                        <div class="message_icon"></div>
                                        <div class="message"><?php echo PCC::l()->val('message.update.successfully'); ?></div>
                                    </div>
                                </div>
                                <?php
                            } else {

                                //Update Fehler
                                if ($updateCode == 130) {
                                    ?>
                                    <div>
                                        <div class="message_info">
                                            <div class="message_icon"></div>
                                            <div class="message"><?php echo PCC::l()->val('message.update.error.130'); ?></div>
                                        </div>
                                    </div>
                                    <?php
                                } else {
                                    ?>
                                    <div>
                                        <div class="message_error">
                                            <div class="message_icon"></div>
                                            <div class="message"><?php echo PCC::l()->val('message.update.error.' . $updateCode); ?></div>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="message_info">
                                            <div class="message_icon"></div>
                                            <div class="message"><?php echo PCC::l()->val('message.update.backupInfo'); ?></div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <div class="buttonField">
                            <a href="index.php"><?php echo PCC::l()->val('index.update.indexButton'); ?></a>
                        </div>
                    </div>
                </div>
                <?php
            } else {

                //Changelog Anzeigen und zustimmung einholen
                ?>
                <div id="content_box">
                    <div id="content_tabs">
                        <ul>
                            <li id="state"><a href="#tabs-1"><?php echo PCC::l()->val('index.update.tab1'); ?></a></li>
                        </ul>
                        <div id="tabs-1">
                            <div class="defaultInputField" style="width: 97%; height: 400px; overflow: scroll; padding: 5px;">
                                <?php
                                if ($data = @file_get_contents(UPDATE_CHANGELOG)) {

                                    echo String::replace("\n", "<br />\n", String::encodeHTML($data));
                                } else {

                                    echo PCC::l()->val('index.update.changelogError');
                                }
                                ?>
                            </div>
                        </div>
                        <div class="buttonField">
                            <a href="index.php?page=update&amp;agreement=true"><?php echo PCC::l()->val('index.update.startButton'); ?></a>
                        </div>
                    </div>
                </div>
                <?php
            }
        } else {

            //Keine Berechtigung
            ?>
            <div>
                <div class="message_error">
                    <div class="message_icon"></div>
                    <div class="message"><?php echo PCC::l()->val('message.accessdenied'); ?></div>
                </div>
            </div>
            <?php
        }
        ?>
    </body>
</html>


<?php
/**
 * Startseite des PCC
 * 
 * @author     Oliver Kleditzsch
 * @copyright  Copyright (c) 2013, Oliver Kleditzsch
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License
 * @since      1.0.0-0
 * @version    1.0.0-0
 */
PCC::l()->loadModule('index');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>
        <title><?php echo PCC::l()->val('global.page.title'); ?></title>
        <?php require_once(TEMPLATES . 'header.php'); ?>
        <script type="text/javascript">
            $(document).ready(function() {

                //Login
                var request;
                $('#login_link').click(function() {
                    $('#login').dialog({
                        modal: true,
                        resizable: false,
                        width: 325,
                        buttons: {
                            "<?php echo PCC::l()->val('global.headline.login'); ?>": function() {

                                //Anmeldedaten an den Server schicken
                                var $form = $('#login_form');
                                var $inputs = $form.find("input, select, button, textarea");
                                var serializedData = $form.serialize();
                                $inputs.prop("disabled", true);

                                request = $.ajax({
                                    url: "index.php?ajax=login",
                                    type: "post",
                                    data: serializedData
                                });

                                request.done(function(response, textStatus, jqXHR) {
                                    //Antwort vom Server
                                    var data = jQuery.parseJSON(response);

                                    if (data.success == true) {
                                        window.location = 'index.php?page=index';
                                    } else if (data.success == false) {
                                        $('#login_error_message').text(data.message);
                                        $('#login_error').css({display: 'block'});
                                        return;
                                    } else {
                                        $('#login_error_message').text('<?php echo PCC::l()->val('global.login.reqerror'); ?>');
                                        $('#login_error').css({display: 'block'});
                                        return;
                                    }
                                });

                                request.fail(function(jqXHR, textStatus, errorThrown) {
                                    //Anfrage Fehlgeschlagen
                                    $('#login_error_message').text('<?php echo PCC::l()->val('global.login.reqerror'); ?>');
                                    $('#login_error').css({display: 'block'});
                                    return;
                                });

                                request.always(function() {
                                    $inputs.prop("disabled", false);
                                });
                            }
                        }
                    });
                });

                //Ladebalken Anzeigen
                $('#loadPrgogressState').progressbar({
                    value: false
                });
                //Ladebalken Anzeigen
                $('#loadPrgogressData').progressbar({
                    value: false
                });

                function loadState(firstLoad) {
                    //Statusdaten per Ajax holen
                    $.getJSON(
                            "index.php?ajax=state&hash=" + Math.floor(Math.random() * 11),
                            function(data) {

                                if (typeof data.error != 'undefined') {

                                    return;
                                }

                                $('#state_uptime').text(data.uptimeSort);
                                $('#state_uptime').attr('title', data.uptime);
                                $('#state_lastBootTime').text(data.lastBootTime);

                                $('#state_sysload_0').text(data.sysload_0);
                                $('#state_sysload_1').text(data.sysload_1);
                                $('#state_sysload_2').text(data.sysload_2);
                                $('#state_cpuClock').text(data.cpuClock);
                                $('#state_coreTemp').text(data.coreTemp);

                                $('#state_memoryPercentDisplay').text(data.memoryPercentDisplay);
                                $('#state_memoryTotal').text(data.memoryTotal);
                                $('#state_memoryFree').text(data.memoryFree);
                                $('#state_memoryUsed').text(data.memoryUsed);
                                $('#memoryusage_progress').progressbar({value: data.memoryPercent});

                                $('#state_swapPercentDisplay').text(data.swapPercentDisplay);
                                $('#state_swapTotal').text(data.swapTotal);
                                $('#state_swapFree').text(data.swapFree);
                                $('#state_swapUsed').text(data.swapUsed);
                                $('#swapusage_progress').progressbar({value: data.swapPercent});

                                $('#state_sysMemory').html(data.sysMemory);
                                $('#state_sysMemoryFoot').html(data.sysMemoryFoot);

                                $('#state_network').html(data.network);

                                if (firstLoad == false) {
                                    $('#generationTime').text(data.generationTime);
                                }

                                $('#firstLoadState').css('display', 'none');
                                $('#finishedLoadState').css('display', 'block');

                                $(document).tooltip({
                                    track: true
                                });

                            });
                }

                loadState(true);

                function loadData() {
                    //Daten per Ajax holen
                    $.getJSON(
                            "index.php?ajax=data&hash=" + Math.floor(Math.random() * 11),
                            function(data) {

                                if (typeof data.error != 'undefined') {

                                    return;
                                }

                                $('#data_osname').text(data.osName);
                                $('#data_kernel').text(data.kernel);
                                $('#data_firmware').text(data.firmwareShort);
                                $('#data_firmware').attr('title', data.firmware);
                                $('#data_splitsystem').text(data.splitSystem);
                                $('#data_splitvideo').text(data.splitVideo);
                                $('#data_hostname').text(data.hostname);

                                $('#data_cputype').text(data.cpuType);
                                $('#data_cpufeatures').text(data.cpuFeatures);
                                $('#data_serial').text(data.serial);
                                $('#data_revision').text(data.revision);

                                $('#data_mpeg2').text(data.mpeg2);
                                $('#data_vc1').text(data.vc1);

                                $('#data_usb').html(data.usb);

                                $('#data_pccversion').html(data.pccVersion);
                                $('#data_installdate').html(data.installDate);
                                $('#data_pccsize').html(data.pccSize);
                                $('#data_phpversion').html(data.phpVersion);

                                $('#generationTime').text(data.generationTime);

                                $('#firstLoadData').css('display', 'none');
                                $('#finishedLoadData').css('display', 'block');

                                $(document).tooltip({
                                    track: true
                                });

                            });
                }

                var currentTab = '';
                $('#content_tabs').tabs({
                    beforeActivate: function(e, ui) {
                        if (ui.newTab.attr('id') == 'state') {
                            currentTab = 'state';
                            loadState(false);
                        } else if (ui.newTab.attr('id') == 'data') {
                            currentTab = 'data';
                            loadData();
                        }
                    }
                });

                var reloadState = function() {
                    if ($('#content_tabs').tabs("option", "active") == 0) {
                        loadState(false);
                    }
                    window.setTimeout(reloadState, <?php echo PCC::getSettings()->getValue('updateIntervall'); ?>);
                }

<?php if (PCC::getSettings()->getValue('autoUpdateStatus')) { ?>
                    window.setTimeout(reloadState, <?php echo PCC::getSettings()->getValue('updateIntervall'); ?>);
<?php } ?>

                $('#memoryusage').tooltip({
                    content: function() {
                        return $('#memoryusage_tooltip').html();
                    },
                    track: true
                });
                $('#swapusage').tooltip({
                    content: function() {
                        return $('#swapusage_tooltip').html();
                    },
                    track: true
                });

            });
        </script>
    </head>
    <body>
        <?php require_once(TEMPLATES . 'headline.php'); ?>
        <div id="login" style="display: none;" title="<?php echo PCC::l()->val('global.login.title'); ?>">
            <form action="#" id="login_form">
                <div class="message_error" style="display: none;" id="login_error">
                    <div class="message_icon"></div>
                    <div class="message" id="login_error_message"></div>
                </div>
                <div>
                    <span><?php echo PCC::l()->val('global.login.username'); ?></span>
                    <input type="text" maxlength="20" name="user" id="login_username" />
                </div>
                <div>
                    <span><?php echo PCC::l()->val('global.login.password'); ?></span>
                    <input type="password" maxlength="20" name="password" id="login_password" />
                </div>
            </form>
        </div>
        <?php if (PCC::getUser()->isOriginator() && file_exists(BASE_DIR . 'install.php')) { ?>
            <div>
                <div class="message_warning">
                    <div class="message_icon"></div>
                    <div class="message">
                        <?php echo PCC::l()->val('message.install.deleteData'); ?>
                        <a href="index.php?action=deleteinstall"><?php echo PCC::l()->val('message.install.deleteData.now'); ?></a>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if (!Update::checkForUpdatesAutomatic()) { ?>
            <div>
                <div class="message_error">
                    <div class="message_icon"></div>
                    <div class="message">
                        <?php echo PCC::l()->val('index.update.searchError'); ?>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if (PCC::getUser()->checkPremission('acpUpdates') && Update::isUpdateAvailable()) { ?>
            <div>
                <div class="message_info">
                    <div class="message_icon"></div>
                    <div class="message">
                        <?php echo PCC::l()->val('message.update.alert'); ?> <a href="index.php?page=update"><?php echo PCC::l()->val('message.update.alert1'); ?></a>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if (PCC::getUser()->checkPremission('showState') || PCC::getUser()->checkPremission('showData') || PCC::getUser()->checkPremission('remoteRPi')) { ?>
            <div id="content_box">
                <div id="content_tabs">
                    <ul>
                        <?php if (PCC::getUser()->checkPremission('showState')) { ?><li id="state"><a href="#tabs-1"><?php echo PCC::l()->val('index.tabs.state'); ?></a></li><?php } ?>
                        <?php if (PCC::getUser()->checkPremission('showData')) { ?><li id="data"><a href="#tabs-2"><?php echo PCC::l()->val('index.tabs.data'); ?></a></li><?php } ?>
                        <?php if (PCC::getUser()->checkPremission('remoteRPi')) { ?><li id="rrpi"><a href="index.php?ajax=rrpilist"><?php echo PCC::l()->val('index.tabs.remoteRPi'); ?></a></li><?php } ?>
                    </ul>
                    <?php if (PCC::getUser()->checkPremission('showState')) { ?>
                        <div id="tabs-1">
                            <div id="firstLoadState">
                                <div id="loadPrgogressState"></div>
                            </div>
                            <div id="finishedLoadState" style="display: none;">
                                <div class="content_box_collapsable ui-tabs ui-widget ui-widget-content ui-corner-all">
                                    <div class="content_box_collapsable_header ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
                                        <?php echo PCC::l()->val('index.state.time.box.title'); ?>
                                    </div>
                                    <div class="content_box_collapsable_body">
                                        <div class="content_box_collapsable_row">  
                                            <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.state.time.box.uptime'); ?></span>
                                            <span class="icon icon_time"></span>
                                            <div class="content_box_collapsable_row_content">
                                                <span id="state_uptime" title=""></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="content_box_collapsable_body">
                                        <div class="content_box_collapsable_row">
                                            <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.state.time.box.lastStart'); ?></span>
                                            <span class="icon icon_starttime"></span>
                                            <div class="content_box_collapsable_row_content">
                                                <span id="state_lastBootTime"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="content_box_collapsable ui-tabs ui-widget ui-widget-content ui-corner-all">
                                    <div class="content_box_collapsable_header ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
                                        <?php echo PCC::l()->val('index.state.system.box.title'); ?>
                                    </div>
                                    <div class="content_box_collapsable_body">
                                        <div class="content_box_collapsable_row">  
                                            <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.state.system.box.cpuusage'); ?></span>
                                            <span class="icon icon_cpuusage"></span>
                                            <div class="content_box_collapsable_row_content">
                                                <span id="state_sysload_0"></span>
                                                &gt;
                                                <span id="state_sysload_1"></span>
                                                &gt;
                                                <span id="state_sysload_2"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="content_box_collapsable_body">
                                        <div class="content_box_collapsable_row">  
                                            <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.state.system.box.spufreq'); ?></span>
                                            <span class="icon icon_cpufreq"></span>
                                            <div class="content_box_collapsable_row_content">
                                                <span id="state_cpuClock"></span> MHz
                                            </div>
                                        </div>
                                    </div>
                                    <div class="content_box_collapsable_body">
                                        <div class="content_box_collapsable_row">  
                                            <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.state.system.box.cputemp'); ?></span>
                                            <span class="icon icon_cputemp"></span>
                                            <div class="content_box_collapsable_row_content" >
                                                <span id="state_coreTemp"></span>&ordm;c
                                            </div>
                                        </div>
                                    </div>
                                    <div class="content_box_collapsable_body">
                                        <div class="content_box_collapsable_row">
                                            <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.state.system.box.memoryusage'); ?></span>
                                            <span class="icon icon_memoryusage"></span>
                                            <div class="content_box_collapsable_row_content" id="memoryusage" title="">
                                                <div id="memoryusage_progress" class="content_box_collapsable_progress"></div>
                                                <span id="state_memoryPercentDisplay"></span>%
                                            </div>
                                            <div style="display: none;" id="memoryusage_tooltip">
                                                <div class="tootlip_row">
                                                    <span class="tooltip_strong"><?php echo PCC::l()->val('index.all.total'); ?></span>: <span id="state_memoryTotal"></span>
                                                </div>
                                                <div class="tootlip_row">
                                                    <span class="tooltip_strong"><?php echo PCC::l()->val('index.all.usage'); ?></span>: <span id="state_memoryUsed"></span>
                                                </div>
                                                <div class="tootlip_row">
                                                    <span class="tooltip_strong"><?php echo PCC::l()->val('index.all.free'); ?></span>: <span id="state_memoryFree"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="content_box_collapsable_body">
                                        <div class="content_box_collapsable_row">
                                            <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.state.system.box.swapusage'); ?></span>
                                            <span class="icon icon_swapusage"></span>
                                            <div class="content_box_collapsable_row_content" id="swapusage" title="">
                                                <div id="swapusage_progress" class="content_box_collapsable_progress"></div>
                                                <span id="state_swapPercentDisplay"></span>%
                                            </div>
                                            <div style="display: none;" id="swapusage_tooltip">
                                                <div class="tootlip_row">
                                                    <span class="tooltip_strong"><?php echo PCC::l()->val('index.all.total'); ?></span>: <span id="state_swapTotal"></span>
                                                </div>
                                                <div class="tootlip_row">
                                                    <span class="tooltip_strong"><?php echo PCC::l()->val('index.all.usage'); ?></span>: <span id="state_swapUsed"></span>
                                                </div>
                                                <div class="tootlip_row">
                                                    <span class="tooltip_strong"><?php echo PCC::l()->val('index.all.free'); ?></span>: <span id="state_swapFree"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="content_box_collapsable ui-tabs ui-widget ui-widget-content ui-corner-all">
                                    <div class="content_box_collapsable_header ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
                                        <?php echo PCC::l()->val('index.state.storage.box.title'); ?>
                                    </div>
                                    <div class="content_box_collapsable_body">
                                        <table class="content_table">
                                            <thead>
                                                <tr>
                                                    <th><?php echo PCC::l()->val('index.state.storage.box.partition'); ?></th>
                                                    <th><?php echo PCC::l()->val('index.state.storage.box.mountpoint'); ?></th>
                                                    <th style="width: 204px;"><?php echo PCC::l()->val('index.state.storage.box.usage'); ?></th>
                                                    <th style="width: 35px;"></th>
                                                    <th><?php echo PCC::l()->val('index.all.total'); ?></th>
                                                    <th><?php echo PCC::l()->val('index.all.usage'); ?></th>
                                                    <th><?php echo PCC::l()->val('index.all.free'); ?></th>
                                                </tr>
                                            </thead>
                                            <tbody id="state_sysMemory">
                                            </tbody>
                                            <tfoot id="state_sysMemoryFoot">
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <div class="content_box_collapsable ui-tabs ui-widget ui-widget-content ui-corner-all">
                                    <div class="content_box_collapsable_header ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
                                        <?php echo PCC::l()->val('index.state.network.box.title'); ?>
                                    </div>
                                    <div class="content_box_collapsable_body">
                                        <table class="content_table">
                                            <thead>
                                                <tr>
                                                    <th><?php echo PCC::l()->val('index.state.network.box.interface'); ?></th>
                                                    <th><?php echo PCC::l()->val('index.state.network.box.rx'); ?></th>
                                                    <th><?php echo PCC::l()->val('index.state.network.box.tx'); ?></th>
                                                    <th><?php echo PCC::l()->val('index.state.network.box.drop'); ?></th>
                                                </tr>
                                            </thead>
                                            <tbody id="state_network">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if (PCC::getUser()->checkPremission('showData')) { ?>
                        <div id="tabs-2">
                            <div id="firstLoadData">
                                <div id="loadPrgogressData"></div>
                            </div>
                            <div id="finishedLoadData" style="display: none;">
                                <div class="content_box_collapsable ui-tabs ui-widget ui-widget-content ui-corner-all">
                                    <div class="content_box_collapsable_header ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
                                        <?php echo PCC::l()->val('index.data.os.box.title'); ?>
                                    </div>
                                    <div class="content_box_collapsable_body">
                                        <div class="content_box_collapsable_row">  
                                            <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.data.os.box.os'); ?></span>
                                            <span class="icon icon_os"></span>
                                            <div class="content_box_collapsable_row_content">
                                                <span id="data_osname"></span>
                                            </div>
                                        </div>
                                        <div class="content_box_collapsable_row">  
                                            <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.data.os.box.kernel'); ?></span>
                                            <span class="icon icon_kernel"></span>
                                            <div class="content_box_collapsable_row_content">
                                                <span id="data_kernel"></span>
                                            </div>
                                        </div>
                                        <div class="content_box_collapsable_row">  
                                            <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.data.os.box.firmware'); ?></span>
                                            <span class="icon icon_firmware"></span>
                                            <div class="content_box_collapsable_row_content">
                                                <span id="data_firmware"></span>
                                            </div>
                                        </div>
                                        <div class="content_box_collapsable_row">  
                                            <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.data.os.box.hostname'); ?></span>
                                            <span class="icon icon_hostname"></span>
                                            <div class="content_box_collapsable_row_content">
                                                <span id="data_hostname" title=""></span>
                                            </div>
                                        </div>
                                        <div class="content_box_collapsable_row">  
                                            <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.data.os.box.memory'); ?></span>
                                            <span class="icon icon_split"></span>
                                            <div class="content_box_collapsable_row_content">
                                                <?php echo PCC::l()->val('index.data.os.box.sytem'); ?> : <span id="data_splitsystem"></span>
                                                <br/>
                                                <?php echo PCC::l()->val('index.data.os.box.video'); ?> : <span id="data_splitvideo"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="content_box_collapsable ui-tabs ui-widget ui-widget-content ui-corner-all">
                                    <div class="content_box_collapsable_header ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
                                        <?php echo PCC::l()->val('index.data.sysdata.box.title'); ?>
                                    </div>
                                    <div class="content_box_collapsable_body">
                                        <div class="content_box_collapsable_row">  
                                            <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.data.sysdata.box.cpu'); ?></span>
                                            <span class="icon icon_cpu"></span>
                                            <div class="content_box_collapsable_row_content">
                                                <span id="data_cputype"></span>
                                            </div>
                                        </div>
                                        <div class="content_box_collapsable_row">  
                                            <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.data.sysdata.box.cpuFeatures'); ?></span>
                                            <span class="icon icon_features"></span>
                                            <div class="content_box_collapsable_row_content">
                                                <span id="data_cpufeatures"></span> 
                                            </div>
                                        </div>
                                        <div class="content_box_collapsable_row">  
                                            <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.data.sysdata.box.serial'); ?></span>
                                            <span class="icon icon_serial"></span>
                                            <div class="content_box_collapsable_row_content">
                                                <span id="data_serial"></span>
                                            </div>
                                        </div>
                                        <div class="content_box_collapsable_row">  
                                            <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.data.sysdata.box.rev'); ?></span>
                                            <span class="icon icon_rev"></span>
                                            <div class="content_box_collapsable_row_content">
                                                <span id="data_revision"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="content_box_collapsable ui-tabs ui-widget ui-widget-content ui-corner-all">
                                    <div class="content_box_collapsable_header ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
                                        <?php echo PCC::l()->val('index.data.video.box.title'); ?>
                                    </div>
                                    <div class="content_box_collapsable_body">
                                        <div class="content_box_collapsable_row">  
                                            <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.data.video.box.h264'); ?></span>
                                            <span class="icon icon_h264"></span>
                                            <div class="content_box_collapsable_row_content">
                                                <?php echo PCC::l()->val('index.data.video.box.activated'); ?> 
                                            </div>
                                        </div>
                                        <div class="content_box_collapsable_row">  
                                            <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.data.video.box.mpeg2'); ?></span>
                                            <span class="icon icon_mpeg2"></span>
                                            <div class="content_box_collapsable_row_content">
                                                <span id="data_mpeg2"></span>
                                            </div>
                                        </div>
                                        <div class="content_box_collapsable_row">  
                                            <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.data.video.box.vc1'); ?></span>
                                            <span class="icon icon_vc1"></span>
                                            <div class="content_box_collapsable_row_content">
                                                <span id="data_vc1"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="content_box_collapsable ui-tabs ui-widget ui-widget-content ui-corner-all">
                                    <div class="content_box_collapsable_header ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
                                        <?php echo PCC::l()->val('index.data.usb.box.title'); ?>
                                    </div>
                                    <div class="content_box_collapsable_body">
                                        <table class="content_table">
                                            <tbody id="data_usb"></tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="content_box_collapsable ui-tabs ui-widget ui-widget-content ui-corner-all">
                                    <div class="content_box_collapsable_header ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
                                        <?php echo PCC::l()->val('index.data.pcc.box.title'); ?>
                                    </div>
                                    <div class="content_box_collapsable_body">
                                        <table class="content_table">
                                            <tbody>
                                                <tr>
                                                    <td><?php echo PCC::l()->val('index.data.pcc.box.version'); ?></td>
                                                    <td><span id="data_pccversion"></span></td>
                                                </tr>
                                                <tr>
                                                    <td><?php echo PCC::l()->val('index.data.pcc.box.installDate'); ?></td>
                                                    <td><span id="data_installdate"></span></td>
                                                </tr>
                                                <tr>
                                                    <td><?php echo PCC::l()->val('index.data.pcc.box.memory'); ?></td>
                                                    <td><span id="data_pccsize"></span></td>
                                                </tr>
                                                <tr>
                                                    <td><?php echo PCC::l()->val('index.data.pcc.box.phpVersion'); ?></td>
                                                    <td><span id="data_phpversion"></span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="footline" class="ui-corner-all">
                        <span id="generationTime"><?php echo PCC::l()->get('global.footline.generationTime', PHP_VERSION, getMicrotime()); ?></span>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if (PCC::getUser()->checkPremission('acpUsers')) { ?>
            <div id="user_editor" title="<?php echo PCC::l()->val('global.menu.admin.users'); ?>" style="display: none;"></div>
        <?php } ?>
        <?php if (PCC::getUser()->checkPremission('acpRemote')) { ?>
            <div id="remote_editor" title="<?php echo PCC::l()->val('global.menu.admin.remote'); ?>" style="display: none;"></div>
        <?php } ?>
        <?php if (PCC::getUser()->checkPremission('acpSettings')) { ?>
            <div id="settings_editor" title="<?php echo PCC::l()->val('global.menu.admin.settings'); ?>" style="display: none;"></div>
        <?php } ?>
    </body>
</html>
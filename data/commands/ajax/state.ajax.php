<?php

/**
 * liefert die Statusdaten der RPi als JSON zurueck
 * 
 * @author     Oliver Kleditzsch
 * @copyright  Copyright (c) 2013, Oliver Kleditzsch
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License
 * @since      1.0.0-0
 * @version    1.0.0-0
 */
if (PCC::getUser()->checkPremission('showState')) {

    $data = array();
    $rpi = new RPi();

    //Sprachdetn Laden
    PCC::l()->loadModule('index');

    //Systemzeit
    $uptime = TimeUtil::formatTimefromSeconds($rpi->getUptime());
    if (String::length($uptime) > 50) {

        $data['uptimeSort'] = String::subString($uptime, 0, 45) . ' ...';
        $data['uptime'] = $uptime;
    } else {
        $data['uptimeSort'] = $uptime;
        $data['uptime'] = $uptime;
    }

    $date = new DateTimeExtendet();
    $date->setTimestamp($rpi->getLastBootTime());
    $data['lastBootTime'] = $date->format('d.m.Y H:i:s');

    //CPU
    $sysload = $rpi->getSysLoad();
    $data['sysload_0'] = $sysload[0];
    $data['sysload_1'] = $sysload[1];
    $data['sysload_2'] = $sysload[2];

    $data['cpuClock'] = String::formatFloat($rpi->getCpuClock());

    $data['coreTemp'] = String::formatFloat($rpi->getCoreTemprature());

    //Speicher
    $memory = $rpi->getMemoryUsage();
    $data['memoryPercent'] = $memory['percent'];
    $data['memoryPercentDisplay'] = String::formatFloat($memory['percent'], 0);
    $data['memoryTotal'] = FileUtil::formatBytesBinary($memory['total']);
    $data['memoryFree'] = FileUtil::formatBytesBinary($memory['free']);
    $data['memoryUsed'] = FileUtil::formatBytesBinary($memory['used']);

    //Swap
    $swap = $rpi->getSwapUsage();
    $data['swapPercent'] = $swap['percent'];
    $data['swapPercentDisplay'] = String::formatFloat($swap['percent'], 0);
    $data['swapTotal'] = FileUtil::formatBytesBinary($swap['total']);
    $data['swapFree'] = FileUtil::formatBytesBinary($swap['free']);
    $data['swapUsed'] = FileUtil::formatBytesBinary($swap['used']);

    //Systemspeicher
    $sysMemory = $rpi->getMemoryInfo();
    $data['sysMemory'] = '';
    foreach ($sysMemory as $index => $mem) {

        if ($index != (count($sysMemory) - 1)) {

            $data['sysMemory'] .= '
            <tr>
                <td>' . String::encodeHTML($mem['device']) . '</td>
                <td>' . String::encodeHTML($mem['mountpoint']) . '</td>
                <td>
                    <script type="text/javascript">
                        $(function() {
                            $(\'#sysMem_progress_' . $index . '\').progressbar({value: ' . $mem['percent'] . '});
                        });
                    </script>
                    <div class="storage_progress" id="sysMem_progress_' . $index . '"></div>
                </td>
                <td>' . String::encodeHTML($mem['percent']) . '%</td>
                <td>' . FileUtil::formatBytesBinary($mem['total']) . '</td>
                <td>' . FileUtil::formatBytesBinary($mem['used']) . '</td>
                <td>' . FileUtil::formatBytesBinary($mem['free']) . '</td>
            </tr>';
        } else {

            $data['sysMemoryFoot'] = '
            <tr>
                <td>' . PCC::l()->val('index.all.total') . '</td>
                <td></td>
                <td>
                    <script type="text/javascript">
                        $(function() {
                            $(\'#sysMem_progress_' . $index . '\').progressbar({value: ' . $mem['percent'] . '});
                        });
                    </script>
                    <div class="storage_progress" id="sysMem_progress_' . $index . '"></div>
                </td>
                <td>' . String::encodeHTML($mem['percent']) . '%</td>
                <td>' . FileUtil::formatBytesBinary($mem['total']) . '</td>
                <td>' . FileUtil::formatBytesBinary($mem['used']) . '</td>
                <td>' . FileUtil::formatBytesBinary($mem['free']) . '</td>
            </tr>';
        }
    }

    //Netzwerk
    $network = $rpi->getNetworkDevices();
    $data['network'] = '';
    foreach ($network as $index => $net) {

        $data['network'] .= '
            <tr>
                <td>' . String::encodeHTML($net['name']) . '</td>
                <td>' . FileUtil::formatBytesBinary($net['in']) . '</td>
                <td>' . FileUtil::formatBytesBinary($net['out']) . '</td>
                <td>' . String::formatFloat($net['errors'], 0) . '/' . String::formatFloat($net['drops'], 0) . '</td>
            </tr>';
    }

    //Zeitmessung
    $data['generationTime'] = PCC::l()->get('global.footline.generationTime', PHP_VERSION, getMicrotime());
} else {

    $data['error'] = PCC::l()->val('message.accessdenied');
}

//Daten senden
echo json_encode($data);
?>

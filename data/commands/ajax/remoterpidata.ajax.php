<?php
/**
 * Verbindet sich mit einem Remote Server und liest die Systemdaten aus
 * 
 * @author     Oliver Kleditzsch
 * @copyright  Copyright (c) 2013, Oliver Kleditzsch
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License
 * @since      1.0.0-0
 * @version    1.0.0-0
 */
try {
    if (PCC::getUser()->checkPremission('showRemoteRPiState')) {

        $data = array();
        $id = PCC::getRequest()->getParam('rrpiid', Request::GET, Request::INT);
        try {

            $rrpi = new RemoteRPi($id);

            //Mit dem entfernten RPi verbinden
            $rrpis = new RemoteRPiSocket($rrpi);
            $rrpis->open();
            $data = $rrpis->getData();
            $rrpis->close();

            $rrpi->updateLastConnection();

            //Daten zum versenden aufbereiten
        } catch (Exception $e) {

            if ($e->getCode() == 10) {

                //Raspberry Existiert nicht
                ?>
                <div>
                    <div class="message_error" id="login_error">
                        <div class="message_icon"></div>
                        <div class="message" id="login_error_message"><?php echo PCC::l()->val('message.remoterpi.notexists') ?></div>
                    </div>
                </div>
                <?php
            } else {

                throw $e;
            }
        }

        //HTML Statusdaten
        PCC::l()->loadModule('index');
        ?>
        <div class="content_box_collapsable ui-tabs ui-widget ui-widget-content ui-corner-all">
            <div class="content_box_collapsable_header ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
                <?php echo PCC::l()->val('index.data.os.box.title'); ?>
            </div>
            <div class="content_box_collapsable_body">
                <div class="content_box_collapsable_row">  
                    <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.data.os.box.os'); ?></span>
                    <span class="icon icon_os"></span>
                    <div class="content_box_collapsable_row_content">
                        <?php echo String::encodeHTML($data['osName']) ?>
                    </div>
                </div>
                <div class="content_box_collapsable_row">  
                    <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.data.os.box.kernel'); ?></span>
                    <span class="icon icon_kernel"></span>
                    <div class="content_box_collapsable_row_content">
                        <?php echo String::encodeHTML($data['kernel']) ?>
                    </div>
                </div>
                <div class="content_box_collapsable_row">  
                    <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.data.os.box.firmware'); ?></span>
                    <span class="icon icon_firmware"></span>
                    <div class="content_box_collapsable_row_content">
                        <span title="<?php echo String::encodeHTML($data['firmware']) ?>"><?php echo String::subString(String::encodeHTML($data['firmware']), 0, 30) . ' ...'; ?></span>
                    </div>
                    <script type="text/javascript">
                        $(document).tooltip({
                            track: true
                        });
                    </script>
                </div>
                <div class="content_box_collapsable_row">  
                    <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.data.os.box.hostname'); ?></span>
                    <span class="icon icon_hostname"></span>
                    <div class="content_box_collapsable_row_content">
                        <?php echo String::encodeHTML($data['hostname']) ?>
                    </div>
                </div>
                <div class="content_box_collapsable_row">  
                    <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.data.os.box.memory'); ?></span>
                    <span class="icon icon_split"></span>
                    <div class="content_box_collapsable_row_content">
                        <?php echo PCC::l()->val('index.data.os.box.sytem'); ?> : <?php echo String::encodeHTML($data['splitSystem']) ?>
                        <br/>
                        <?php echo PCC::l()->val('index.data.os.box.video'); ?> : <?php echo String::encodeHTML($data['splitVideo']) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="content_box_collapsable ui-tabs ui-widget ui-widget-content ui-corner-all">
            <div class="content_box_collapsable_header ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
                <?php echo PCC::l()->val('index.data.sysdata.box.title'); ?>
            </div>
            <div class="content_box_collapsable_body">
                <div class="content_box_collapsable_row">  
                    <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.data.sysdata.box.cpu'); ?></span>
                    <span class="icon icon_cpu"></span>
                    <div class="content_box_collapsable_row_content">
                        <?php echo String::encodeHTML($data['cpuType']); ?>
                    </div>
                </div>
                <div class="content_box_collapsable_row">  
                    <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.data.sysdata.box.cpuFeatures'); ?></span>
                    <span class="icon icon_features"></span>
                    <div class="content_box_collapsable_row_content">
                        <?php echo String::encodeHTML($data['cpuFeatures']); ?>
                    </div>
                </div>
                <div class="content_box_collapsable_row">  
                    <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.data.sysdata.box.serial'); ?></span>
                    <span class="icon icon_serial"></span>
                    <div class="content_box_collapsable_row_content">
                        <?php echo String::encodeHTML($data['serial']); ?>
                    </div>
                </div>
                <div class="content_box_collapsable_row">  
                    <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.data.sysdata.box.rev'); ?></span>
                    <span class="icon icon_rev"></span>
                    <div class="content_box_collapsable_row_content">
                        <?php
                        if ($data['revision'] == 2) {
                            echo 'Model B Revision 1.0';
                        } elseif ($data['revision'] == 3) {
                            echo 'Model B Revision 1.0 + Fuses mod and D14 removed';
                        } elseif ($data['revision'] == 4) {
                            echo 'Model B Revision 2.0 256MB (Sony)';
                        } elseif ($data['revision'] == 5) {
                            echo 'Model B Revision 2.0 256MB (Qisda)';
                        } elseif ($data['revision'] == 6) {
                            echo 'Model B Revision 2.0 256MB (Egoman)';
                        } elseif ($data['revision'] == 7) {
                            echo 'Model A Revision 2.0 256MB (Egoman)';
                        } elseif ($data['revision'] == 8) {
                            echo 'Model A Revision 2.0 256MB (Sony)';
                        } elseif ($data['revision'] == 9) {
                            echo 'Model A Revision 2.0 256MB (Qisda)';
                        } elseif ($data['revision'] == 13) {
                            echo 'Model B Revision 2.0 512MB (Egoman)';
                        } elseif ($data['revision'] == 14) {
                            echo 'Model B Revision 2.0 512MB (Sony)';
                        } elseif ($data['revision'] == 15) {
                            echo 'Model B Revision 2.0 512MB (Qisda)';
                        } else {
                            echo PCC::l()->val('global.unknown');
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="content_box_collapsable ui-tabs ui-widget ui-widget-content ui-corner-all">
            <div class="content_box_collapsable_header ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
                <?php echo PCC::l()->val('index.data.video.box.title'); ?>
            </div>
            <div class="content_box_collapsable_body">
                <div class="content_box_collapsable_row">  
                    <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.data.video.box.h264'); ?></span>
                    <span class="icon icon_h264"></span>
                    <div class="content_box_collapsable_row_content">
                        <?php echo PCC::l()->val('index.data.video.box.activated'); ?> 
                    </div>
                </div>
                <div class="content_box_collapsable_row">  
                    <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.data.video.box.mpeg2'); ?></span>
                    <span class="icon icon_mpeg2"></span>
                    <div class="content_box_collapsable_row_content">
                        <?php
                        if ($data['mpeg2']) {
                            echo PCC::l()->val('index.data.video.box.activated');
                        } else {
                            echo PCC::l()->val('index.data.video.box.deactivated');
                        }
                        ?> 
                    </div>
                </div>
                <div class="content_box_collapsable_row">  
                    <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.data.video.box.vc1'); ?></span>
                    <span class="icon icon_vc1"></span>
                    <div class="content_box_collapsable_row_content">
                        <?php
                        if ($data['vc1']) {
                            echo PCC::l()->val('index.data.video.box.activated');
                        } else {
                            echo PCC::l()->val('index.data.video.box.deactivated');
                        }
                        ?> 
                    </div>
                </div>
            </div>
        </div>
        <div class="content_box_collapsable ui-tabs ui-widget ui-widget-content ui-corner-all">
            <div class="content_box_collapsable_header ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
                <?php echo PCC::l()->val('index.data.usb.box.title'); ?>
            </div>
            <div class="content_box_collapsable_body">
                <table class="content_table">
                    <tbody>
                        <?php foreach ($data['usb'] as $usb) { ?>
                            <tr>
                                <td><?php echo String::encodeHTML($usb); ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="content_box_collapsable ui-tabs ui-widget ui-widget-content ui-corner-all">
            <div class="content_box_collapsable_header ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
                <?php echo PCC::l()->val('index.data.pcc.box.title'); ?>
            </div>
            <div class="content_box_collapsable_body">
                <table class="content_table">
                    <tbody>
                        <tr>
                            <td><?php echo PCC::l()->val('index.data.pcc.box.version'); ?></td>
                            <td><?php echo String::encodeHTML($data['pccVersion'][0]); ?></td>
                        </tr>
                        <tr>
                            <td><?php echo PCC::l()->val('index.data.pcc.box.installDate'); ?></td>
                            <td><?php echo String::encodeHTML($data['installDate'][0]); ?></td>
                        </tr>
                        <tr>
                            <td><?php echo PCC::l()->val('index.data.pcc.box.memory'); ?></td>
                            <td><?php echo FileUtil::formatBytesBinary($data['pccSize']); ?></td>
                        </tr>
                        <tr>
                            <td><?php echo PCC::l()->val('index.data.pcc.box.phpVersion'); ?></td>
                            <td><?php echo String::encodeHTML($data['phpVersion']); ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="footline" class="ui-corner-all">
            <span id="generationTime"><?php echo PCC::l()->get('global.footline.generationTime', PHP_VERSION, getMicrotime()); ?></span>
        </div>
        <?php
    } else {

        //HTML Fehler
        ?>
        <div>
            <div class="message_error" id="login_error">
                <div class="message_icon"></div>
                <div class="message" id="login_error_message"><?php echo PCC::l()->val('message.accessdenied') ?></div>
            </div>
        </div>
        <?php
    }
} catch (Exception $e) {
    ?>
    <div>
        <div class="message_error" id="login_error">
            <div class="message_icon"></div>
            <div class="message" id="login_error_message"><?php echo PCC::l()->val('message.remoterpi.noconnection') ?></div>
        </div>
    </div>
    <?php
}
?>

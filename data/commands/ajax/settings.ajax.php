<?php
/**
 * Einstellungen Verwalten
 * 
 * @author     Oliver Kleditzsch
 * @copyright  Copyright (c) 2013, Oliver Kleditzsch
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License
 * @since      1.0.0-0
 * @version    1.0.0-0
 */
PCC::l()->loadModule('index');
if (PCC::getUser()->checkPremission('acpSettings')) {
    if (!count($_POST)) {
        $settings = PCC::getSettings();

        //Einstellungen
        $language = $settings->getValue('language');
        $autoUpdateStatus = $settings->getValue('autoUpdateStatus');
        $updateIntervall = $settings->getValue('updateIntervall');
        $searchUpdates = $settings->getValue('searchUpdates');
        $searchIntervall = $settings->getValue('searchIntervall');
        ?>
        <div>
            <div id="settings_tabs">
                <ul>
                    <li><a href="#settings_tab_1"><?php echo PCC::l()->val('index.settings.tabs.1'); ?></a></li>
                    <li><a href="#settings_tab_2"><?php echo PCC::l()->val('index.settings.tabs.2'); ?></a></li>
                    <li><a href="#settings_tab_3"><?php echo PCC::l()->val('index.settings.tabs.3'); ?></a></li>
                </ul>
                <form id="settings_form">
                    <div id="settings_tab_1">
                        <div class="description">
                            <div class="descriptionText">
                                <?php echo PCC::l()->val('index.settings.form.desc.1'); ?>
                            </div>
                        </div>
                        <div class="form_content">
                            <div class="form_title"><?php echo PCC::l()->val('index.settings.form.lang.title'); ?></div>
                            <div class="form_element">
                                <select name="language" class="defaultInputField">
                                    <option value="de" <?php echo ($language == 'de' ? 'selected="selected"' : ''); ?>><?php echo PCC::l()->val('index.settings.form.lang.values.1'); ?></option>
                                    <option value="en" <?php echo ($language == 'en' ? 'selected="selected"' : ''); ?>><?php echo PCC::l()->val('index.settings.form.lang.values.2'); ?></option>
                                </select>
                            </div>
                            <div class="form_description"><?php echo PCC::l()->val('index.settings.form.lang.desc'); ?></div>
                        </div>										
                    </div>
                    <div id="settings_tab_2">
                        <div class="description">
                            <div class="descriptionText">
                                <?php echo PCC::l()->val('index.settings.form.desc.2'); ?>  
                            </div>
                        </div>
                        <div class="form_content">
                            <div class="form_title"><?php echo PCC::l()->val('index.settings.form.autoUpdate.title'); ?></div>
                            <div class="form_element">
                                <div style="margin: 5px;"  name="autoUpdateStatus">
                                    <label for="autoUpdateStatus_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                        <input type="radio" style="margin: 0 5px 0 0;" name="autoUpdateStatus" id="autoUpdateStatus_on" value="1" <?php echo ($autoUpdateStatus === true ? 'checked="checked"' : ''); ?>/>
                                        <?php echo PCC::l()->val('global.form.onoff.on'); ?>
                                    </label>
                                    <label for="autoUpdateStatus_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                        <input type="radio" style="margin: 0 5px 0 0;" name="autoUpdateStatus" id="autoUpdateStatus_off" value="0" <?php echo ($autoUpdateStatus === false ? 'checked="checked"' : ''); ?>/>
                                        <?php echo PCC::l()->val('global.form.onoff.off'); ?>
                                    </label>
                                </div>
                            </div>
                            <div class="form_description"><?php echo PCC::l()->val('index.settings.form.autoUpdate.desc'); ?></div>
                        </div>
                        <div class="form_content">
                            <div class="form_title"><?php echo PCC::l()->val('index.settings.form.updateIntervall.title'); ?></div>
                            <div class="form_element">
                                <select name="updateIntervall" class=" defaultInputField">
                                    <option value="1000" <?php echo ($updateIntervall === 1000 ? 'selected="selected"' : ''); ?>><?php echo PCC::l()->val('index.settings.form.updateIntervall.values.1'); ?></option>
                                    <option value="2000" <?php echo ($updateIntervall === 2000 ? 'selected="selected"' : ''); ?>><?php echo PCC::l()->val('index.settings.form.updateIntervall.values.2'); ?></option>
                                    <option value="3000" <?php echo ($updateIntervall === 3000 ? 'selected="selected"' : ''); ?>><?php echo PCC::l()->val('index.settings.form.updateIntervall.values.3'); ?></option>
                                    <option value="4000" <?php echo ($updateIntervall === 4000 ? 'selected="selected"' : ''); ?>><?php echo PCC::l()->val('index.settings.form.updateIntervall.values.4'); ?></option>
                                    <option value="5000" <?php echo ($updateIntervall === 5000 ? 'selected="selected"' : ''); ?>><?php echo PCC::l()->val('index.settings.form.updateIntervall.values.5'); ?></option>
                                    <option value="10000" <?php echo ($updateIntervall === 10000 ? 'selected="selected"' : ''); ?>><?php echo PCC::l()->val('index.settings.form.updateIntervall.values.6'); ?></option>
                                    <option value="20000" <?php echo ($updateIntervall === 20000 ? 'selected="selected"' : ''); ?>><?php echo PCC::l()->val('index.settings.form.updateIntervall.values.7'); ?></option>
                                    <option value="30000" <?php echo ($updateIntervall === 30000 ? 'selected="selected"' : ''); ?>><?php echo PCC::l()->val('index.settings.form.updateIntervall.values.8'); ?></option>
                                    <option value="60000" <?php echo ($updateIntervall === 60000 ? 'selected="selected"' : ''); ?>><?php echo PCC::l()->val('index.settings.form.updateIntervall.values.9'); ?></option>
                                </select>
                            </div>
                            <div class="form_description"><?php echo PCC::l()->val('index.settings.form.updateIntervall.desc'); ?></div>
                        </div>
                    </div>
                    <div id="settings_tab_3">
                        <div class="description">
                            <div class="descriptionText">
                                <?php echo PCC::l()->val('index.settings.form.desc.3'); ?>
                            </div>
                        </div>
                        <div class="form_content">
                            <div class="form_title"><?php echo PCC::l()->val('index.settings.form.searchUpdates.title'); ?></div>
                            <div class="form_element">
                                <div style="margin: 5px;"  name="searchUpdates">
                                    <label for="searchUpdates_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                        <input type="radio" style="margin: 0 5px 0 0;" name="searchUpdates" id="searchUpdates_on" value="1" <?php echo ($searchUpdates === true ? 'checked="checked"' : ''); ?>/>
                                        <?php echo PCC::l()->val('global.form.onoff.on'); ?>
                                    </label>
                                    <label for="searchUpdates_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                        <input type="radio" style="margin: 0 5px 0 0;" name="searchUpdates" id="searchUpdates_off" value="0" <?php echo ($searchUpdates === false ? 'checked="checked"' : ''); ?>/>
                                        <?php echo PCC::l()->val('global.form.onoff.off'); ?>
                                    </label>
                                </div>
                            </div>
                            <div class="form_description"><?php echo PCC::l()->val('index.settings.form.searchUpdates.desc'); ?></div>
                        </div>
                        <div class="form_content">
                            <div class="form_title"><?php echo PCC::l()->val('index.settings.form.searchIntervall.title'); ?></div>
                            <div class="form_element">
                                <select name="searchIntervall" class=" defaultInputField">
                                    <option value="1" <?php echo ($searchIntervall === 1 ? 'selected="selected"' : ''); ?>><?php echo PCC::l()->val('index.settings.form.searchIntervall.values.1'); ?></option>
                                    <option value="7" <?php echo ($searchIntervall === 7 ? 'selected="selected"' : ''); ?>><?php echo PCC::l()->val('index.settings.form.searchIntervall.values.2'); ?></option>
                                    <option value="30" <?php echo ($searchIntervall === 30 ? 'selected="selected"' : ''); ?>><?php echo PCC::l()->val('index.settings.form.searchIntervall.values.3'); ?></option>
                                </select>
                            </div>
                            <div class="form_description"><?php echo PCC::l()->val('index.settings.form.searchIntervall.desc'); ?></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript">
            $(function() {

                $('#settings_tabs').tabs();
            });
        </script>
        <?php
    } else {

        //Daten Speichern
        $settings = PCC::getSettings();
        $invalid = false;
        $invalidFields = array();
        $request = PCC::getRequest();
        //Sprache
        $language = $request->getParam('language', Request::POST, Request::STRING);
        if (in_array($language, array('de', 'en')) && $settings->updateSetting('language', $language)) {
            
        } else {
            $invalidFields[] = 'language';
            $invalid = true;
        }

        //Auto Update
        $autoUpdate = $request->getParam('autoUpdateStatus', Request::POST, Request::BOOL);
        if (in_array($autoUpdate, array('1', '0')) && $settings->updateSetting('autoUpdateStatus', $autoUpdate)) {
            
        } else {
            $invalidFields[] = 'autoUpdateStatus';
            $invalid = true;
        }

        //Update Intervall
        $updateIntervall = $request->getParam('updateIntervall', Request::POST, Request::INT);
        if (in_array($updateIntervall, array('1000', '2000', '3000', '4000', '5000', '10000', '20000', '30000', '60000')) && $settings->updateSetting('updateIntervall', $updateIntervall)) {
            
        } else {
            $invalidFields[] = 'updateIntervall';
            $invalid = true;
        }

        //Update Suche
        $searchUpdates = $request->getParam('searchUpdates', Request::POST, Request::BOOL);
        if (in_array($searchUpdates, array('1', '0')) && $settings->updateSetting('searchUpdates', $searchUpdates)) {
            
        } else {
            $invalidFields[] = 'searchUpdates';
            $invalid = true;
        }

        //Such Intervall
        $searceIntervall = $request->getParam('searchIntervall', Request::POST, Request::INT);
        if (in_array($searceIntervall, array('1', '7', '30')) && $settings->updateSetting('searchIntervall', $searceIntervall)) {
            
        } else {
            $invalidFields[] = 'searchIntervall';
            $invalid = true;
        }

        if ($invalid === true) {

            //Formular erneut anzeigen
            ?>
            <div>
                <div class="message_error">
                    <div class="message_icon"></div>
                    <div class="message"><?php echo PCC::l()->val('message.settings.error'); ?></div>
                    <ul>
                        <?php foreach ($invalidFields as $field) { ?>
                            <li><?php echo PCC::l()->val('message.settings.inputError.' . $field); ?></li>
                        <?php } ?> 
                    </ul>
                </div>
            </div>
            <div>
                <div id="settings_tabs">
                    <ul>
                        <li><a href="#settings_tab_1"><?php echo PCC::l()->val('index.settings.tabs.1'); ?></a></li>
                        <li><a href="#settings_tab_2"><?php echo PCC::l()->val('index.settings.tabs.2'); ?></a></li>
                        <li><a href="#settings_tab_3"><?php echo PCC::l()->val('index.settings.tabs.3'); ?></a></li>
                    </ul>
                    <form id="settings_form">
                        <div id="settings_tab_1">
                            <div class="description">
                                <div class="descriptionText">
                                    <?php echo PCC::l()->val('index.settings.form.desc.1'); ?>
                                </div>
                            </div>
                            <div class="form_content">
                                <div class="form_title"><?php echo PCC::l()->val('index.settings.form.lang.title'); ?></div>
                                <div class="form_element">
                                    <select name="language" class="defaultInputField<?php echo (in_array('language', $invalidFields) ? ' invalidFormField' : ''); ?>">
                                        <option value="de" <?php echo ($language == 'de' ? 'selected="selected"' : ''); ?>><?php echo PCC::l()->val('index.settings.form.lang.values.1'); ?></option>
                                        <option value="en" <?php echo ($language == 'en' ? 'selected="selected"' : ''); ?>><?php echo PCC::l()->val('index.settings.form.lang.values.2'); ?></option>
                                    </select>
                                </div>
                                <div class="form_description"><?php echo PCC::l()->val('index.settings.form.lang.desc'); ?></div>
                            </div>										
                        </div>
                        <div id="settings_tab_2">
                            <div class="description">
                                <div class="descriptionText">
                                    <?php echo PCC::l()->val('index.settings.form.desc.2'); ?>  
                                </div>
                            </div>
                            <div class="form_content">
                                <div class="form_title"><?php echo PCC::l()->val('index.settings.form.autoUpdate.title'); ?></div>
                                <div class="form_element">
                                    <div style="margin: 5px;"  name="autoUpdateStatus">
                                        <label for="autoUpdateStatus_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                            <input type="radio" style="margin: 0 5px 0 0;" name="autoUpdateStatus" id="autoUpdateStatus_on" value="1" <?php echo ($autoUpdate === true ? 'checked="checked"' : ''); ?>/>
                                            <?php echo PCC::l()->val('global.form.onoff.on'); ?>
                                        </label>
                                        <label for="autoUpdateStatus_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                            <input type="radio" style="margin: 0 5px 0 0;" name="autoUpdateStatus" id="autoUpdateStatus_off" value="0" <?php echo ($autoUpdate === false ? 'checked="checked"' : ''); ?>/>
                                            <?php echo PCC::l()->val('global.form.onoff.off'); ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="form_description"><?php echo PCC::l()->val('index.settings.form.autoUpdate.desc'); ?></div>
                            </div>
                            <div class="form_content">
                                <div class="form_title"><?php echo PCC::l()->val('index.settings.form.updateIntervall.title'); ?></div>
                                <div class="form_element">
                                    <select name="updateIntervall" class=" defaultInputField<?php echo (in_array('updateIntervall', $invalidFields) ? ' invalidFormField' : ''); ?>">
                                        <option value="1000" <?php echo ($updateIntervall === 1000 ? 'selected="selected"' : ''); ?>><?php echo PCC::l()->val('index.settings.form.updateIntervall.values.1'); ?></option>
                                        <option value="2000" <?php echo ($updateIntervall === 2000 ? 'selected="selected"' : ''); ?>><?php echo PCC::l()->val('index.settings.form.updateIntervall.values.2'); ?></option>
                                        <option value="3000" <?php echo ($updateIntervall === 3000 ? 'selected="selected"' : ''); ?>><?php echo PCC::l()->val('index.settings.form.updateIntervall.values.3'); ?></option>
                                        <option value="4000" <?php echo ($updateIntervall === 4000 ? 'selected="selected"' : ''); ?>><?php echo PCC::l()->val('index.settings.form.updateIntervall.values.4'); ?></option>
                                        <option value="5000" <?php echo ($updateIntervall === 5000 ? 'selected="selected"' : ''); ?>><?php echo PCC::l()->val('index.settings.form.updateIntervall.values.5'); ?></option>
                                        <option value="10000" <?php echo ($updateIntervall === 10000 ? 'selected="selected"' : ''); ?>><?php echo PCC::l()->val('index.settings.form.updateIntervall.values.6'); ?></option>
                                        <option value="20000" <?php echo ($updateIntervall === 20000 ? 'selected="selected"' : ''); ?>><?php echo PCC::l()->val('index.settings.form.updateIntervall.values.7'); ?></option>
                                        <option value="30000" <?php echo ($updateIntervall === 30000 ? 'selected="selected"' : ''); ?>><?php echo PCC::l()->val('index.settings.form.updateIntervall.values.8'); ?></option>
                                        <option value="60000" <?php echo ($updateIntervall === 60000 ? 'selected="selected"' : ''); ?>><?php echo PCC::l()->val('index.settings.form.updateIntervall.values.9'); ?></option>
                                    </select>
                                </div>
                                <div class="form_description"><?php echo PCC::l()->val('index.settings.form.updateIntervall.desc'); ?></div>
                            </div>
                        </div>
                        <div id="settings_tab_3">
                            <div class="description">
                                <div class="descriptionText">
                                    <?php echo PCC::l()->val('index.settings.form.desc.3'); ?>
                                </div>
                            </div>
                            <div class="form_content">
                                <div class="form_title"><?php echo PCC::l()->val('index.settings.form.searchUpdates.title'); ?></div>
                                <div class="form_element">
                                    <div style="margin: 5px;"  name="searchUpdates">
                                        <label for="searchUpdates_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                            <input type="radio" style="margin: 0 5px 0 0;" name="searchUpdates" id="searchUpdates_on" value="1" <?php echo ($searchUpdates === true ? 'checked="checked"' : ''); ?>/>
                                            <?php echo PCC::l()->val('global.form.onoff.on'); ?>
                                        </label>
                                        <label for="searchUpdates_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                            <input type="radio" style="margin: 0 5px 0 0;" name="searchUpdates" id="searchUpdates_off" value="0" <?php echo ($searchUpdates === false ? 'checked="checked"' : ''); ?>/>
                                            <?php echo PCC::l()->val('global.form.onoff.off'); ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="form_description"><?php echo PCC::l()->val('index.settings.form.searchUpdates.desc'); ?></div>
                            </div>
                            <div class="form_content">
                                <div class="form_title"><?php echo PCC::l()->val('index.settings.form.searchIntervall.title'); ?></div>
                                <div class="form_element">
                                    <select name="searchIntervall" class=" defaultInputField<?php echo (in_array('searchIntervall', $invalidFields) ? ' invalidFormField' : ''); ?>">
                                        <option value="1" <?php echo ($searceIntervall === 1 ? 'selected="selected"' : ''); ?>><?php echo PCC::l()->val('index.settings.form.searchIntervall.values.1'); ?></option>
                                        <option value="7" <?php echo ($searceIntervall === 7 ? 'selected="selected"' : ''); ?>><?php echo PCC::l()->val('index.settings.form.searchIntervall.values.2'); ?></option>
                                        <option value="30" <?php echo ($searceIntervall === 30 ? 'selected="selected"' : ''); ?>><?php echo PCC::l()->val('index.settings.form.searchIntervall.values.3'); ?></option>
                                    </select>
                                </div>
                                <div class="form_description"><?php echo PCC::l()->val('index.settings.form.searchIntervall.desc'); ?></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <script type="text/javascript">
                $(function() {

                    $('#settings_tabs').tabs();
                });
            </script>
            <?php
        } else {

            //Erfolgsmeldung
            ?>
            <div>
                <div class="message_successfully">
                    <div class="message_icon"></div>
                    <div class="message"><?php echo PCC::l()->val('message.settings.success'); ?></div>
                </div>
            </div>
            <script type="text/javascript">
                $('#settings_editor').dialog({
                    buttons: {
                        '<?php echo PCC::l()->val('index.settings.buttons.close'); ?>': function() {
                            $('#settings_editor').dialog('close');
                        }
                    }
                });
            </script>
            <?php
        }
    }
} else {
    //Keine Berechtigung
    ?>
    <div>
        <div class="message_error">
            <div class="message_icon"></div>
            <div class="message"><?php echo PCC::l()->val('message.accessdenied'); ?></div>
        </div>
    </div>
    <?php
}
?>

<?php
/**
 * verbindet sich mit einem Remote Server und liest die Statusdaten aus
 * 
 * @author     Oliver Kleditzsch
 * @copyright  Copyright (c) 2013, Oliver Kleditzsch
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License
 * @since      1.0.0-0
 * @version    1.0.0-0
 */
try {
    if (PCC::getUser()->checkPremission('showRemoteRPiState')) {

        $data = array();
        $id = PCC::getRequest()->getParam('rrpiid', Request::GET, Request::INT);
        try {

            $rrpi = new RemoteRPi($id);

            //Mit dem entfernten RPi verbinden
            $rrpis = new RemoteRPiSocket($rrpi);
            $rrpis->open();
            $state = $rrpis->getState();
            $rrpis->close();

            $rrpi->updateLastConnection();

            //Daten zum versenden aufbereiten
        } catch (Exception $e) {

            if ($e->getCode() == 10) {

                //Raspberry Existiert nicht
                ?>
                <div>
                    <div class="message_error" id="login_error">
                        <div class="message_icon"></div>
                        <div class="message" id="login_error_message"><?php echo PCC::l()->val('message.remoterpi.notexists') ?></div>
                    </div>
                </div>
                <?php
            } else {

                throw $e;
            }
        }

        //HTML Statusdaten
        PCC::l()->loadModule('index');
        ?>
        <div class="content_box_collapsable ui-tabs ui-widget ui-widget-content ui-corner-all">
            <div class="content_box_collapsable_header ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
                <?php echo PCC::l()->val('index.state.time.box.title'); ?>
            </div>
            <div class="content_box_collapsable_body">
                <div class="content_box_collapsable_row">  
                    <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.state.time.box.uptime'); ?></span>
                    <span class="icon icon_time"></span>
                    <div class="content_box_collapsable_row_content">
                        <?php
                        $uptime = TimeUtil::formatTimefromSeconds($state['uptime']);
                        if (String::length($uptime) > 40) {
                            echo '<span title="' . $uptime . '">' . String::subString($uptime, 0, 35) . ' ...</span>';
                        } else {
                            echo '<span title="' . $uptime . '">' . $uptime . '</span>';
                        }
                        ?>
                    </div>
                </div>
                <script type="text/javascript">
                    $(document).tooltip({
                        track: true
                    });
                </script>
            </div>
            <div class="content_box_collapsable_body">
                <div class="content_box_collapsable_row">
                    <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.state.time.box.lastStart'); ?></span>
                    <span class="icon icon_starttime"></span>
                    <div class="content_box_collapsable_row_content">
                        <?php
                        $date = new DateTimeExtendet();
                        $date->setTimestamp($state['lastBootTime']);
                        echo $date->format('d.m.Y H:i:s');
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="content_box_collapsable ui-tabs ui-widget ui-widget-content ui-corner-all">
            <div class="content_box_collapsable_header ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
                <?php echo PCC::l()->val('index.state.system.box.title'); ?>
            </div>
            <div class="content_box_collapsable_body">
                <div class="content_box_collapsable_row">  
                    <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.state.system.box.cpuusage'); ?></span>
                    <span class="icon icon_cpuusage"></span>
                    <div class="content_box_collapsable_row_content">
                        <?php echo $state['sysload_0']; ?>
                        &gt;
                        <?php echo $state['sysload_1']; ?>
                        &gt;
                        <?php echo $state['sysload_2']; ?>
                    </div>
                </div>
            </div>
            <div class="content_box_collapsable_body">
                <div class="content_box_collapsable_row">  
                    <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.state.system.box.spufreq'); ?></span>
                    <span class="icon icon_cpufreq"></span>
                    <div class="content_box_collapsable_row_content">
                        <?php echo String::formatFloat($state['cpuClock']); ?> MHz
                    </div>
                </div>
            </div>
            <div class="content_box_collapsable_body">
                <div class="content_box_collapsable_row">  
                    <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.state.system.box.cputemp'); ?></span>
                    <span class="icon icon_cputemp"></span>
                    <div class="content_box_collapsable_row_content" >
                        <?php echo String::formatFloat($state['coreTemp']); ?>&ordm;c
                    </div>
                </div>
            </div>
            <?php $dynId = String::randomStr(32); ?>
            <div class="content_box_collapsable_body">
                <div class="content_box_collapsable_row">
                    <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.state.system.box.memoryusage'); ?></span>
                    <span class="icon icon_memoryusage"></span>
                    <div class="content_box_collapsable_row_content" id="memoryusage_rrpi_<?php echo $dynId; ?>" title="">
                        <div id="memoryusage_progress_rrpi_<?php echo $dynId; ?>" class="content_box_collapsable_progress" style="width: 200px"></div>
                        <?php echo String::formatFloat($state['memoryPercent'], 0); ?>%
                    </div>
                    <div style="display: none;" id="memoryusage_rrpi_<?php echo $id; ?>_tooltip">
                        <div class="tootlip_row">
                            <span class="tooltip_strong"><?php echo PCC::l()->val('index.all.total'); ?></span>: <?php echo FileUtil::formatBytesBinary($state['memoryTotal']); ?>
                        </div>
                        <div class="tootlip_row">
                            <span class="tooltip_strong"><?php echo PCC::l()->val('index.all.usage'); ?></span>: <?php echo FileUtil::formatBytesBinary($state['memoryUsed']); ?>
                        </div>
                        <div class="tootlip_row">
                            <span class="tooltip_strong"><?php echo PCC::l()->val('index.all.free'); ?></span>: <?php echo FileUtil::formatBytesBinary($state['memoryFree']); ?>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    $('#memoryusage_progress_rrpi_<?php echo $dynId; ?>').progressbar({value: <?php echo $state['memoryPercent']; ?>});
                    $('#memoryusage_rrpi_<?php echo $dynId; ?>').tooltip({
                        content: function() {
                            return $('#memoryusage_rrpi_<?php echo $id; ?>_tooltip').html();
                        },
                        track: true
                    });
                </script>
            </div>
            <?php $dynId = String::randomStr(32); ?>
            <div class="content_box_collapsable_body">
                <div class="content_box_collapsable_row">
                    <span class="content_box_collapsable_row_title"><?php echo PCC::l()->val('index.state.system.box.swapusage'); ?></span>
                    <span class="icon icon_swapusage"></span>
                    <div class="content_box_collapsable_row_content" id="swapusage_rrpi_<?php echo $dynId; ?>" title="">
                        <div id="swapusage_progress_rrpi_<?php echo $dynId; ?>" class="content_box_collapsable_progress" style="width: 200px"></div>
                        <?php echo String::formatFloat($state['swapPercent'], 0); ?>%
                    </div>
                    <div style="display: none;" id="swapusage_rrpi_<?php echo $id; ?>_tooltip">
                        <div class="tootlip_row">
                            <span class="tooltip_strong"><?php echo PCC::l()->val('index.all.total'); ?></span>: <?php echo FileUtil::formatBytesBinary($state['swapTotal']); ?>
                        </div>
                        <div class="tootlip_row">
                            <span class="tooltip_strong"><?php echo PCC::l()->val('index.all.usage'); ?></span>: <?php echo FileUtil::formatBytesBinary($state['swapUsed']); ?>
                        </div>
                        <div class="tootlip_row">
                            <span class="tooltip_strong"><?php echo PCC::l()->val('index.all.free'); ?></span>: <?php echo FileUtil::formatBytesBinary($state['swapFree']); ?>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    $('#swapusage_progress_rrpi_<?php echo $dynId; ?>').progressbar({value: <?php echo $state['swapPercent']; ?>});
                    $('#swapusage_rrpi_<?php echo $dynId; ?>').tooltip({
                        content: function() {
                            return $('#swapusage_rrpi_<?php echo $id; ?>_tooltip').html();
                        },
                        track: true
                    });
                </script>
            </div>
        </div>
        <div class="content_box_collapsable ui-tabs ui-widget ui-widget-content ui-corner-all">
            <div class="content_box_collapsable_header ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
                <?php echo PCC::l()->val('index.state.storage.box.title'); ?>
            </div>
            <div class="content_box_collapsable_body">
                <table class="content_table">
                    <thead>
                        <tr>
                            <th><?php echo PCC::l()->val('index.state.storage.box.partition'); ?></th>
                            <th><?php echo PCC::l()->val('index.state.storage.box.mountpoint'); ?></th>
                            <th style="width: 35px;"></th>
                            <th><?php echo PCC::l()->val('index.all.total'); ?></th>
                            <th><?php echo PCC::l()->val('index.all.usage'); ?></th>
                            <th><?php echo PCC::l()->val('index.all.free'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($state['sysMemory'] as $device) { ?>
                            <tr>
                                <td><?php echo String::encodeHTML($device['device']) ?></td>
                                <td><?php echo String::encodeHTML($device['mountpoint']) ?></td>
                                <td><?php echo String::formatFloat($device['percent'], 0) ?>%</td>
                                <td><?php echo FileUtil::formatBytesBinary($device['total']) ?></td>
                                <td><?php echo FileUtil::formatBytesBinary($device['used']) ?></td>
                                <td><?php echo FileUtil::formatBytesBinary($device['free']) ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td><?php echo PCC::l()->val('index.all.total') ?></td>
                            <td></td>
                            <td><?php echo String::formatFloat($state['sysMemoryTotal']['percent'], 0) ?>%</td>
                            <td><?php echo FileUtil::formatBytesBinary($state['sysMemoryTotal']['total']) ?></td>
                            <td><?php echo FileUtil::formatBytesBinary($state['sysMemoryTotal']['used']) ?></td>
                            <td><?php echo FileUtil::formatBytesBinary($state['sysMemoryTotal']['free']) ?></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <div class="content_box_collapsable ui-tabs ui-widget ui-widget-content ui-corner-all">
            <div class="content_box_collapsable_header ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
                <?php echo PCC::l()->val('index.state.network.box.title'); ?>
            </div>
            <div class="content_box_collapsable_body">
                <table class="content_table">
                    <thead>
                        <tr>
                            <th><?php echo PCC::l()->val('index.state.network.box.interface'); ?></th>
                            <th><?php echo PCC::l()->val('index.state.network.box.rx'); ?></th>
                            <th><?php echo PCC::l()->val('index.state.network.box.tx'); ?></th>
                            <th><?php echo PCC::l()->val('index.state.network.box.drop'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($state['network'] as $device) { ?>
                            <tr>
                                <td><?php echo String::encodeHTML($device['name']) ?></td>
                                <td><?php echo FileUtil::formatBytesBinary($device['in']) ?></td>
                                <td><?php echo FileUtil::formatBytesBinary($device['out']) ?></td>
                                <td><?php echo String::formatFloat($device['errors'], 0) ?>/<?php echo String::formatFloat($device['drops'], 0) ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="footline" class="ui-corner-all">
            <span id="generationTime"><?php echo PCC::l()->get('global.footline.generationTime', PHP_VERSION, getMicrotime()); ?></span>
        </div>
        <?php
    } else {

        //HTML Fehler
        ?>
        <div>
            <div class="message_error" id="login_error">
                <div class="message_icon"></div>
                <div class="message" id="login_error_message"><?php echo PCC::l()->val('message.accessdenied') ?></div>
            </div>
        </div>
        <?php
    }
} catch (Exception $e) {
    ?>
    <div>
        <div class="message_error" id="login_error">
            <div class="message_icon"></div>
            <div class="message" id="login_error_message"><?php echo PCC::l()->val('message.remoterpi.noconnection') ?></div>
        </div>
    </div>
    <?php
}
?>

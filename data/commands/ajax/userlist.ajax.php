<?php
/**
 * Benutzerverwaltung
 * 
 * @author     Oliver Kleditzsch
 * @copyright  Copyright (c) 2013, Oliver Kleditzsch
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License
 * @since      1.0.0-0
 * @version    1.0.0-0
 */
PCC::l()->loadModule('index');
if (PCC::getUser()->checkPremission('acpUsers')) {

    $request = PCC::getRequest();
    if ($request->issetParam('req', Request::GET) && $request->getParam('req', Request::GET, Request::STRING) == 'create') {

        if (!count($_POST)) {
            //Benuter erstellen (Formular anzeigen)
            ?>
            <div>
                <div id="user_tabs">
                    <ul>
                        <li><a href="#user_tab_1"><?php echo PCC::l()->val('index.user.tabs.1'); ?></a></li>
                        <li><a href="#user_tab_2"><?php echo PCC::l()->val('index.user.tabs.2'); ?></a></li>
                        <li><a href="#user_tab_3"><?php echo PCC::l()->val('index.user.tabs.3'); ?></a></li>
                    </ul>
                    <form id="user_create">
                        <div id="user_tab_1">
                            <div class="description">
                                <div class="descriptionText">
                                    <?php echo PCC::l()->val('index.user.tabs.1.dec'); ?>
                                </div>
                            </div>
                            <div class="form_content">
                                <div class="form_title"><?php echo PCC::l()->val('index.user.form.name'); ?> <span class="red">*</span></div>
                                <div class="form_element"><input type="text" name="name" class=" defaultInputField" value="" maxlength="25" /></div>
                                <div class="form_description"><?php echo PCC::l()->val('index.user.form.name.desc'); ?></div>
                            </div>
                            <div class="form_content">
                                <div class="form_title"><?php echo PCC::l()->val('index.user.form.pass'); ?> <span class="red">*</span></div>
                                <div class="form_element"><input type="password" name="password" class=" defaultInputField" maxlength="20" /></div>
                                <div class="form_description"><?php echo PCC::l()->val('index.user.form.pass.desc'); ?></div>
                            </div>
                            <div class="form_content">
                                <div class="form_title"><?php echo PCC::l()->val('index.user.form.pass1'); ?> <span class="red">*</span></div>
                                <div class="form_element"><input type="password" name="password1" class=" defaultInputField" maxlength="20" /></div>
                                <div class="form_description"><?php echo PCC::l()->val('index.user.form.pass1.desc'); ?></div>
                            </div>
                        </div>
                        <div id="user_tab_2">
                            <div class="description">
                                <div class="descriptionText">
                                    <?php echo PCC::l()->val('index.user.tabs.2.dec'); ?>
                                </div>
                            </div>
                            <div class="form_content">
                                <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.showState'); ?></div>
                                <div class="form_element">
                                    <div style="margin: 5px;"  name="showState">
                                        <label for="showState_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                            <input type="radio" style="margin: 0 5px 0 0;" name="showState" id="showState_on" value="1" checked="checked"/>
                                            <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                        </label>
                                        <label for="showState_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                            <input type="radio" style="margin: 0 5px 0 0;" name="showState" id="showState_off" value="0" />
                                            <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.showState.desc'); ?></div>
                            </div>
                            <div class="form_content">
                                <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.showData'); ?></div>
                                <div class="form_element">
                                    <div style="margin: 5px;"  name="showData">
                                        <label for="showData_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                            <input type="radio" style="margin: 0 5px 0 0;" name="showData" id="showData_on" value="1" checked="checked"/>
                                            <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                        </label>
                                        <label for="showData_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                            <input type="radio" style="margin: 0 5px 0 0;" name="showData" id="showData_off" value="0" />
                                            <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.showData.desc'); ?></div>
                            </div>
                            <div class="form_content">
                                <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.remoteRPi'); ?></div>
                                <div class="form_element">
                                    <div style="margin: 5px;"  name="remoteRPi">
                                        <label for="remoteRPi_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                            <input type="radio" style="margin: 0 5px 0 0;" name="remoteRPi" id="remoteRPi_on" value="1"/>
                                            <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                        </label>
                                        <label for="remoteRPi_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                            <input type="radio" style="margin: 0 5px 0 0;" name="remoteRPi" id="remoteRPi_off" value="0" checked="checked" />
                                            <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.remoteRPi.desc'); ?></div>
                            </div>
                            <div class="form_content">
                                <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.showRemoteRPiState'); ?></div>
                                <div class="form_element">
                                    <div style="margin: 5px;"  name="showRemoteRPiState">
                                        <label for="showRemoteRPiState_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                            <input type="radio" style="margin: 0 5px 0 0;" name="showRemoteRPiState" id="showRemoteRPiState_on" value="1" />
                                            <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                        </label>
                                        <label for="showRemoteRPiState_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                            <input type="radio" style="margin: 0 5px 0 0;" name="showRemoteRPiState" id="showRemoteRPiState_off" value="0"  checked="checked"/>
                                            <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.showRemoteRPiState.desc'); ?></div>
                            </div>
                            <div class="form_content">
                                <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.showRemoteRPiData'); ?></div>
                                <div class="form_element">
                                    <div style="margin: 5px;"  name="showRemoteRPiData">
                                        <label for="showRemoteRPiData_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                            <input type="radio" style="margin: 0 5px 0 0;" name="showRemoteRPiData" id="showRemoteRPiData_on" value="1" />
                                            <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                        </label>
                                        <label for="showRemoteRPiData_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                            <input type="radio" style="margin: 0 5px 0 0;" name="showRemoteRPiData" id="showRemoteRPiData_off" value="0"  checked="checked"/>
                                            <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.showRemoteRPiData.desc'); ?></div>
                            </div>
                        </div>
                        <div id="user_tab_3">
                            <div class="description">
                                <div class="descriptionText">
                                    <?php echo PCC::l()->val('index.user.tabs.3.dec'); ?>
                                </div>
                            </div>
                            <div class="form_content">
                                <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.useAcpMenu'); ?></div>
                                <div class="form_element">
                                    <div style="margin: 5px;"  name="useAcpMenu">
                                        <label for="useAcpMenu_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                            <input type="radio" style="margin: 0 5px 0 0;" name="useAcpMenu" id="useAcpMenu_on" value="1" />
                                            <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                        </label>
                                        <label for="useAcpMenu_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                            <input type="radio" style="margin: 0 5px 0 0;" name="useAcpMenu" id="useAcpMenu_off" value="0"  checked="checked"/>
                                            <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.useAcpMenu.desc'); ?></div>
                            </div>
                            <div class="form_content">
                                <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.acpUsers'); ?></div>
                                <div class="form_element">
                                    <div style="margin: 5px;"  name="acpUsers">
                                        <label for="acpUsers_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                            <input type="radio" style="margin: 0 5px 0 0;" name="acpUsers" id="acpUsers_on" value="1" />
                                            <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                        </label>
                                        <label for="acpUsers_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                            <input type="radio" style="margin: 0 5px 0 0;" name="acpUsers" id="acpUsers_off" value="0"  checked="checked"/>
                                            <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.acpUsers.desc'); ?></div>
                            </div>
                            <div class="form_content">
                                <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.acpRemote'); ?></div>
                                <div class="form_element">
                                    <div style="margin: 5px;"  name="acpRemote">
                                        <label for="acpRemote_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                            <input type="radio" style="margin: 0 5px 0 0;" name="acpRemote" id="acpRemote_on" value="1" />
                                            <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                        </label>
                                        <label for="acpRemote_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                            <input type="radio" style="margin: 0 5px 0 0;" name="acpRemote" id="acpRemote_off" value="0"  checked="checked"/>
                                            <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.acpRemote.desc'); ?></div>
                            </div>
                            <div class="form_content">
                                <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.acpSettings'); ?></div>
                                <div class="form_element">
                                    <div style="margin: 5px;"  name="acpSettings">
                                        <label for="acpSettings_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                            <input type="radio" style="margin: 0 5px 0 0;" name="acpSettings" id="acpSettings_on" value="1" />
                                            <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                        </label>
                                        <label for="acpSettings_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                            <input type="radio" style="margin: 0 5px 0 0;" name="acpSettings" id="acpSettings_off" value="0"  checked="checked"/>
                                            <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.acpSettings.desc'); ?></div>
                            </div>
                            <div class="form_content">
                                <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.acpUpdates'); ?></div>
                                <div class="form_element">
                                    <div style="margin: 5px;"  name="acpUpdates">
                                        <label for="acpUpdates_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                            <input type="radio" style="margin: 0 5px 0 0;" name="acpUpdates" id="acpUpdates_on" value="1" />
                                            <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                        </label>
                                        <label for="acpUpdates_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                            <input type="radio" style="margin: 0 5px 0 0;" name="acpUpdates" id="acpUpdates_off" value="0"  checked="checked"/>
                                            <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.acpUpdates.desc'); ?></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <script type="text/javascript">
                $(function() {

                    $('#user_tabs').tabs();
                    $('#user_editor').dialog({
                        buttons: {
                            '<?php echo utf8_decode(PCC::l()->val('index.user.buttons.back', null, true, false)); ?>': function() {
                                $.get('index.php?ajax=userlist', function(data, textStatus, jqXHR) {
                                    $('#user_editor').html(data);
                                });

                                $('#user_editor').dialog({
                                    buttons: {
                                        '<?php echo PCC::l()->val('index.user.buttons.createUser'); ?>': function() {

                                            $.get('index.php?ajax=userlist&req=create', function(data, textStatus, jqXHR) {
                                                $('#user_editor').html(data);
                                            });
                                        }
                                    }
                                });
                            },
                            '<?php echo PCC::l()->val('index.user.buttons.reset'); ?>': function() {

                                resetForm('#user_create');
                            },
                                    '<?php echo PCC::l()->val('index.user.buttons.save'); ?>': function() {

                                //Anmeldedaten an den Server schicken
                                var $form = $('#user_create');
                                var $inputs = $form.find("input, select, button, textarea");
                                var serializedData = $form.serialize();
                                $inputs.prop("disabled", true);

                                request = $.ajax({
                                    url: "index.php?ajax=userlist&req=create",
                                    type: "post",
                                    data: serializedData
                                });

                                request.done(function(response, textStatus, jqXHR) {
                                    //Antwort vom Server
                                    $('#user_editor').html(response);
                                });

                                request.fail(function(jqXHR, textStatus, errorThrown) {
                                    //Anfrage Fehlgeschlagen

                                });

                                request.always(function() {
                                    $inputs.prop("disabled", false);
                                });
                            }
                        }
                    });
                });
            </script>
            <?php
        } else {

            //Benutzer erstellen
            $invalid = false;
            $invalidFields = array();

            //Benutzername
            $name = $request->getParam('name', Request::POST, Request::STRING);
            if ($name == '' || !preg_match('#[a-z0-9\#\_\!\-\.\,\;\+\*\?]{3,25}#i', $name)) {

                $invalid = true;
                $invalidFields[] = 'name';
            }

            //Pruefen ob der Benutzername frei ist
            if (!UserFactory::getFactory()->checkUserName($name)) {

                $invalid = true;
                $invalidFields[] = 'doubleName';
            }

            //Passwort
            $pass1 = $request->getParam('password', Request::POST, Request::STRING);
            $pass2 = $request->getParam('password1', Request::POST, Request::STRING);
            if ($pass1 === null || $pass2 === null || $pass1 != $pass2 || !String::checkLength($pass1, 5, 20)) {

                $invalid = true;
                $invalidFields[] = 'password';
                $invalidFields[] = 'password1';
            }

            //Benutzerechte
            $premissions = array();

            //Statusseite
            $showState = $request->getParam('showState', Request::POST, Request::BOOL);
            if ($showState === null) {

                $invalid = true;
                $invalidFields[] = 'showState';
            } else {
                $premissions['showState'] = $showState;
            }

            //Daten Seite
            $showData = $request->getParam('showData', Request::POST, Request::BOOL);
            if ($showData === null) {

                $invalid = true;
                $invalidFields[] = 'showData';
            } else {
                $premissions['showData'] = $showData;
            }

            //Externe Raspberrys
            $remoteRPi = $request->getParam('remoteRPi', Request::POST, Request::BOOL);
            if ($remoteRPi === null) {

                $invalid = true;
                $invalidFields[] = 'remoteRPi';
            } else {
                $premissions['remoteRPi'] = $remoteRPi;
            }

            //Statusdaten von externen Raspberrys
            $showRemoteRPiState = $request->getParam('showRemoteRPiState', Request::POST, Request::BOOL);
            if ($showRemoteRPiState === null) {

                $invalid = true;
                $invalidFields[] = 'showRemoteRPiState';
            } else {
                $premissions['showRemoteRPiState'] = $showRemoteRPiState;
            }

            //Kenndaten von externen Raspberrys
            $showRemoteRPiData = $request->getParam('showRemoteRPiData', Request::POST, Request::BOOL);
            if ($showRemoteRPiData === null) {

                $invalid = true;
                $invalidFields[] = 'showRemoteRPiData';
            } else {
                $premissions['showRemoteRPiData'] = $showRemoteRPiData;
            }

            //Admin Menue
            $useAcpMenu = $request->getParam('useAcpMenu', Request::POST, Request::BOOL);
            if ($useAcpMenu === null) {

                $invalid = true;
                $invalidFields[] = 'useAcpMenu';
            } else {
                $premissions['useAcpMenu'] = $useAcpMenu;
            }

            //Benutzerverwaltung
            $acpUsers = $request->getParam('acpUsers', Request::POST, Request::BOOL);
            if ($acpUsers === null) {

                $invalid = true;
                $invalidFields[] = 'acpUsers';
            } else {
                $premissions['acpUsers'] = $acpUsers;
            }

            //externen Raspberrys verwalten
            $acpRemote = $request->getParam('acpRemote', Request::POST, Request::BOOL);
            if ($acpRemote === null) {

                $invalid = true;
                $invalidFields[] = 'acpRemote';
            } else {
                $premissions['acpRemote'] = $acpRemote;
            }

            //Einstellungen
            $acpSettings = $request->getParam('acpSettings', Request::POST, Request::BOOL);
            if ($acpSettings === null) {

                $invalid = true;
                $invalidFields[] = 'acpSettings';
            } else {
                $premissions['acpSettings'] = $acpSettings;
            }

            //Aktualisierungen
            $acpUpdates = $request->getParam('acpUpdates', Request::POST, Request::BOOL);
            if ($acpUpdates === null) {

                $invalid = true;
                $invalidFields[] = 'acpUpdates';
            } else {
                $premissions['acpUpdates'] = $acpUpdates;
            }

            //Daten Speichern
            if ($invalid === true) {

                //Felherhafte Eingaben
                ?>
                <div>
                    <div class="message_error">
                        <div class="message_icon"></div>
                        <div class="message"><?php echo PCC::l()->val('message.user.error'); ?></div>
                        <ul>
                            <?php
                            foreach ($invalidFields as $field) {
                                if ($field == 'password1') {
                                    continue;
                                }
                                ?>
                                <li><?php echo PCC::l()->val('message.user.create.inputError.' . $field); ?></li>
                            <?php } ?> 
                        </ul>
                    </div>
                </div>
                <div>
                    <div id="user_tabs">
                        <ul>
                            <li><a href="#user_tab_1"><?php echo PCC::l()->val('index.user.tabs.1'); ?></a></li>
                            <li><a href="#user_tab_2"><?php echo PCC::l()->val('index.user.tabs.2'); ?></a></li>
                            <li><a href="#user_tab_3"><?php echo PCC::l()->val('index.user.tabs.3'); ?></a></li>
                        </ul>
                        <form id="user_create">
                            <div id="user_tab_1">
                                <div class="description">
                                    <div class="descriptionText">
                                        <?php echo PCC::l()->val('index.user.tabs.1.dec'); ?>
                                    </div>
                                </div>
                                <div class="form_content">
                                    <div class="form_title"><?php echo PCC::l()->val('index.user.form.name'); ?> <span class="red">*</span></div>
                                    <div class="form_element"><input type="text" name="name" class="defaultInputField<?php echo (in_array('name', $invalidFields) || in_array('doubleName', $invalidFields) ? ' invalidFormField' : ''); ?>" maxlength="25" value="<?php echo String::encodeHTML($name); ?>" /></div>
                                    <div class="form_description"><?php echo PCC::l()->val('index.user.form.name.desc'); ?></div>
                                </div>
                                <div class="form_content">
                                    <div class="form_title"><?php echo PCC::l()->val('index.user.form.pass'); ?> <span class="red">*</span></div>
                                    <div class="form_element"><input type="password" name="password" class="defaultInputField<?php echo (in_array('password', $invalidFields) ? ' invalidFormField' : ''); ?>" maxlength="20" /></div>
                                    <div class="form_description"><?php echo PCC::l()->val('index.user.form.pass.desc'); ?></div>
                                </div>
                                <div class="form_content">
                                    <div class="form_title"><?php echo PCC::l()->val('index.user.form.pass1'); ?> <span class="red">*</span></div>
                                    <div class="form_element"><input type="password" name="password1" class="defaultInputField<?php echo (in_array('password1', $invalidFields) ? ' invalidFormField' : ''); ?>" maxlength="20" /></div>
                                    <div class="form_description"><?php echo PCC::l()->val('index.user.form.pass1.desc'); ?></div>
                                </div>
                            </div>
                            <div id="user_tab_2">
                                <div class="description">
                                    <div class="descriptionText">
                                        <?php echo PCC::l()->val('index.user.tabs.2.dec'); ?>
                                    </div>
                                </div>
                                <div class="form_content">
                                    <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.showState'); ?></div>
                                    <div class="form_element">
                                        <div style="margin: 5px;"  name="showState">
                                            <label for="showState_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="showState" id="showState_on" value="1" <?php echo ($showState === true ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                            </label>
                                            <label for="showState_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="showState" id="showState_off" value="0" <?php echo ($showState == false ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.showState.desc'); ?></div>
                                </div>
                                <div class="form_content">
                                    <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.showData'); ?></div>
                                    <div class="form_element">
                                        <div style="margin: 5px;"  name="showData">
                                            <label for="showData_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="showData" id="showData_on" value="1" <?php echo ($showData === true ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                            </label>
                                            <label for="showData_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="showData" id="showData_off" value="0" <?php echo ($showData == false ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.showData.desc'); ?></div>
                                </div>
                                <div class="form_content">
                                    <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.remoteRPi'); ?></div>
                                    <div class="form_element">
                                        <div style="margin: 5px;"  name="remoteRPi">
                                            <label for="remoteRPi_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="remoteRPi" id="remoteRPi_on" value="1" <?php echo ($remoteRPi === true ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                            </label>
                                            <label for="remoteRPi_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="remoteRPi" id="remoteRPi_off" value="0" <?php echo ($remoteRPi == false ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.remoteRPi.desc'); ?></div>
                                </div>
                                <div class="form_content">
                                    <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.showRemoteRPiState'); ?></div>
                                    <div class="form_element">
                                        <div style="margin: 5px;"  name="showRemoteRPiState">
                                            <label for="showRemoteRPiState_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="showRemoteRPiState" id="showRemoteRPiState_on" value="1" <?php echo ($showRemoteRPiState === true ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                            </label>
                                            <label for="showRemoteRPiState_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="showRemoteRPiState" id="showRemoteRPiState_off" value="0" <?php echo ($showRemoteRPiState == false ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.showRemoteRPiState.desc'); ?></div>
                                </div>
                                <div class="form_content">
                                    <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.showRemoteRPiData'); ?></div>
                                    <div class="form_element">
                                        <div style="margin: 5px;"  name="showRemoteRPiData">
                                            <label for="showRemoteRPiData_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="showRemoteRPiData" id="showRemoteRPiData_on" value="1" <?php echo ($showRemoteRPiData === true ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                            </label>
                                            <label for="showRemoteRPiData_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="showRemoteRPiData" id="showRemoteRPiData_off" value="0" <?php echo ($showRemoteRPiData == false ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.showRemoteRPiData.desc'); ?></div>
                                </div>
                            </div>
                            <div id="user_tab_3">
                                <div class="description">
                                    <div class="descriptionText">
                                        <?php echo PCC::l()->val('index.user.tabs.3.dec'); ?>
                                    </div>
                                </div>
                                <div class="form_content">
                                    <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.useAcpMenu'); ?></div>
                                    <div class="form_element">
                                        <div style="margin: 5px;"  name="useAcpMenu">
                                            <label for="useAcpMenu_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="useAcpMenu" id="useAcpMenu_on" value="1" <?php echo ($useAcpMenu === true ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                            </label>
                                            <label for="useAcpMenu_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="useAcpMenu" id="useAcpMenu_off" value="0" <?php echo ($useAcpMenu == false ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.useAcpMenu.desc'); ?></div>
                                </div>
                                <div class="form_content">
                                    <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.acpUsers'); ?></div>
                                    <div class="form_element">
                                        <div style="margin: 5px;"  name="acpUsers">
                                            <label for="acpUsers_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="acpUsers" id="acpUsers_on" value="1" <?php echo ($acpUsers === true ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                            </label>
                                            <label for="acpUsers_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="acpUsers" id="acpUsers_off" value="0" <?php echo ($acpUsers == false ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.acpUsers.desc'); ?></div>
                                </div>
                                <div class="form_content">
                                    <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.acpRemote'); ?></div>
                                    <div class="form_element">
                                        <div style="margin: 5px;"  name="acpRemote">
                                            <label for="acpRemote_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="acpRemote" id="acpRemote_on" value="1" <?php echo ($acpRemote === true ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                            </label>
                                            <label for="acpRemote_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="acpRemote" id="acpRemote_off" value="0" <?php echo ($acpRemote == false ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.acpRemote.desc'); ?></div>
                                </div>
                                <div class="form_content">
                                    <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.acpSettings'); ?></div>
                                    <div class="form_element">
                                        <div style="margin: 5px;"  name="acpSettings">
                                            <label for="acpSettings_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="acpSettings" id="acpSettings_on" value="1" <?php echo ($acpSettings === true ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                            </label>
                                            <label for="acpSettings_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="acpSettings" id="acpSettings_off" value="0" <?php echo ($acpSettings == false ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.acpSettings.desc'); ?></div>
                                </div>
                                <div class="form_content">
                                    <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.acpUpdates'); ?></div>
                                    <div class="form_element">
                                        <div style="margin: 5px;"  name="acpUpdates">
                                            <label for="acpUpdates_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="acpUpdates" id="acpUpdates_on" value="1" <?php echo ($acpUpdates === true ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                            </label>
                                            <label for="acpUpdates_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="acpUpdates" id="acpUpdates_off" value="0" <?php echo ($acpUpdates == false ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.acpUpdates.desc'); ?></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <script type="text/javascript">
                    $(function() {

                        $('#user_tabs').tabs();
                        $('#user_editor').dialog({
                            buttons: {
                                '<?php echo utf8_decode(PCC::l()->val('index.user.buttons.back')); ?>': function() {
                                    $.get('index.php?ajax=userlist', function(data, textStatus, jqXHR) {
                                        $('#user_editor').html(data);
                                    });

                                    $('#user_editor').dialog({
                                        buttons: {
                                            '<?php echo PCC::l()->val('index.user.buttons.createUser'); ?>': function() {

                                                $.get('index.php?ajax=userlist&req=create', function(data, textStatus, jqXHR) {
                                                    $('#user_editor').html(data);
                                                });
                                            }
                                        }
                                    });
                                },
                                '<?php echo PCC::l()->val('index.user.buttons.reset'); ?>': function() {

                                    resetForm('#user_create');
                                },
                                        '<?php echo PCC::l()->val('index.user.buttons.save'); ?>': function() {

                                    //Anmeldedaten an den Server schicken
                                    var $form = $('#user_create');
                                    var $inputs = $form.find("input, select, button, textarea");
                                    var serializedData = $form.serialize();
                                    $inputs.prop("disabled", true);

                                    request = $.ajax({
                                        url: "index.php?ajax=userlist&req=create",
                                        type: "post",
                                        data: serializedData
                                    });

                                    request.done(function(response, textStatus, jqXHR) {
                                        //Antwort vom Server
                                        $('#user_editor').html(response);
                                    });

                                    request.fail(function(jqXHR, textStatus, errorThrown) {
                                        //Anfrage Fehlgeschlagen

                                    });

                                    request.always(function() {
                                        $inputs.prop("disabled", false);
                                    });
                                }
                            }
                        });
                    });
                </script>
                <?php
            } elseif ($invalid === false && UserFactory::getFactory()->createUser($name, $pass1, $premissions) > 0) {

                //Erfolgsmeldung
                ?>
                <div>
                    <div class="message_successfully">
                        <div class="message_icon"></div>
                        <div class="message"><?php echo PCC::l()->val('message.user.success'); ?></div>
                    </div>
                </div>
                <script type="text/javascript">
                    $('#user_editor').dialog({
                        buttons: {
                            '<?php echo utf8_decode(PCC::l()->val('index.user.buttons.back')); ?>': function() {
                                $.get('index.php?ajax=userlist', function(data, textStatus, jqXHR) {
                                    $('#user_editor').html(data);
                                });

                                $('#user_editor').dialog({
                                    buttons: {
                                        '<?php echo PCC::l()->val('index.user.buttons.createUser'); ?>': function() {

                                            $.get('index.php?ajax=userlist&req=create', function(data, textStatus, jqXHR) {
                                                $('#user_editor').html(data);
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    });
                </script>
                <?php
            } else {
                ?>
                <div>
                    <div class="message_error">
                        <div class="message_icon"></div>
                        <div class="message"><?php echo PCC::l()->val('message.user.create.error'); ?></div>
                    </div>
                </div>
                <script type="text/javascript">
                    $('#user_editor').dialog({
                        buttons: {
                            '<?php echo utf8_decode(PCC::l()->val('index.user.buttons.back')); ?>': function() {
                                $.get('index.php?ajax=userlist', function(data, textStatus, jqXHR) {
                                    $('#user_editor').html(data);
                                });

                                $('#user_editor').dialog({
                                    buttons: {
                                        '<?php echo PCC::l()->val('index.user.buttons.createUser'); ?>': function() {

                                            $.get('index.php?ajax=userlist&req=create', function(data, textStatus, jqXHR) {
                                                $('#user_editor').html(data);
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    });
                </script>
                <?php
            }
        }
    } elseif ($request->issetParam('req', Request::GET) && $request->getParam('req', Request::GET, Request::STRING) == 'edit' && $request->issetParam('uid', Request::GET)) {

        //Benuter bearbeiten
        //Benutzerdaten Laden
        if ($request->getParam('uid', Request::GET, Request::PLAIN) == 'visitor') {

            $user = UserFactory::getFactory()->getVisitor();
        } else {

            $user = UserFactory::getFactory()->getUserById($request->getParam('uid', Request::GET, Request::INT));
        }

        //Formular
        if ($user !== null && !count($_POST)) {
            ?>
            <div>
                <div id="user_tabs">
                    <ul>
                        <?php if ($user instanceof User) { ?><li><a href="#user_tab_1"><?php echo PCC::l()->val('index.user.tabs.1'); ?></a></li><?php } ?>
                        <?php if ($user->isOriginator() == false) { ?>
                            <li><a href="#user_tab_2"><?php echo PCC::l()->val('index.user.tabs.2'); ?></a></li>
                            <li><a href="#user_tab_3"><?php echo PCC::l()->val('index.user.tabs.3'); ?></a></li>
                        <?php } ?>
                    </ul>
                    <form id="user_create">
                        <?php if ($user instanceof User) { ?>
                            <div id="user_tab_1">
                                <div class="description">
                                    <div class="descriptionText">
                                        <?php echo PCC::l()->val('index.user.tabs.1.dec'); ?>
                                    </div>
                                </div>
                                <div class="form_content">
                                    <div class="form_title"><?php echo PCC::l()->val('index.user.form.name'); ?> <span class="red">*</span></div>
                                    <div class="form_element"><input type="text" name="name" class="defaultInputField" maxlength="25" value="<?php echo String::encodeHTML($user->getName()); ?>" /></div>
                                    <div class="form_description"><?php echo PCC::l()->val('index.user.form.name.desc'); ?></div>
                                </div>
                                <div class="form_content">
                                    <div class="form_title"><?php echo PCC::l()->val('index.user.form.pass'); ?> <span class="red">*</span></div>
                                    <div class="form_element"><input type="password" name="password" class="defaultInputField" maxlength="20" /></div>
                                    <div class="form_description"><?php echo PCC::l()->val('index.user.form.pass.desc'); ?></div>
                                </div>
                                <div class="form_content">
                                    <div class="form_title"><?php echo PCC::l()->val('index.user.form.pass1'); ?> <span class="red">*</span></div>
                                    <div class="form_element"><input type="password" name="password1" class="defaultInputField" maxlength="20" /></div>
                                    <div class="form_description"><?php echo PCC::l()->val('index.user.form.pass1.desc'); ?></div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php $premissions = $user->listPremissions(); ?>
                        <?php if ($user->isOriginator() == false) { ?>
                            <div id="user_tab_2">
                                <div class="description">
                                    <div class="descriptionText">
                                        <?php echo PCC::l()->val('index.user.tabs.2.dec'); ?>
                                    </div>
                                </div>
                                <div class="form_content">
                                    <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.showState'); ?></div>
                                    <div class="form_element">
                                        <div style="margin: 5px;"  name="showState">
                                            <label for="showState_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="showState" id="showState_on" value="1" <?php echo ($premissions['showState'] === true ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                            </label>
                                            <label for="showState_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="showState" id="showState_off" value="0" <?php echo ($premissions['showState'] == false ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.showState.desc'); ?></div>
                                </div>
                                <div class="form_content">
                                    <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.showData'); ?></div>
                                    <div class="form_element">
                                        <div style="margin: 5px;"  name="showData">
                                            <label for="showData_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="showData" id="showData_on" value="1" <?php echo ($premissions['showData'] === true ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                            </label>
                                            <label for="showData_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="showData" id="showData_off" value="0" <?php echo ($premissions['showData'] == false ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.showData.desc'); ?></div>
                                </div>
                                <div class="form_content">
                                    <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.remoteRPi'); ?></div>
                                    <div class="form_element">
                                        <div style="margin: 5px;"  name="remoteRPi">
                                            <label for="remoteRPi_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="remoteRPi" id="remoteRPi_on" value="1" <?php echo ($premissions['remoteRPi'] === true ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                            </label>
                                            <label for="remoteRPi_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="remoteRPi" id="remoteRPi_off" value="0" <?php echo ($premissions['remoteRPi'] == false ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.remoteRPi.desc'); ?></div>
                                </div>
                                <div class="form_content">
                                    <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.showRemoteRPiState'); ?></div>
                                    <div class="form_element">
                                        <div style="margin: 5px;"  name="showRemoteRPiState">
                                            <label for="showRemoteRPiState_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="showRemoteRPiState" id="showRemoteRPiState_on" value="1" <?php echo ($premissions['showRemoteRPiState'] === true ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                            </label>
                                            <label for="showRemoteRPiState_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="showRemoteRPiState" id="showRemoteRPiState_off" value="0" <?php echo ($premissions['showRemoteRPiState'] == false ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.showRemoteRPiState.desc'); ?></div>
                                </div>
                                <div class="form_content">
                                    <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.showRemoteRPiData'); ?></div>
                                    <div class="form_element">
                                        <div style="margin: 5px;"  name="showRemoteRPiData">
                                            <label for="showRemoteRPiData_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="showRemoteRPiData" id="showRemoteRPiData_on" value="1" <?php echo ($premissions['showRemoteRPiData'] === true ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                            </label>
                                            <label for="showRemoteRPiData_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="showRemoteRPiData" id="showRemoteRPiData_off" value="0" <?php echo ($premissions['showRemoteRPiData'] == false ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.showRemoteRPiData.desc'); ?></div>
                                </div>
                            </div>
                            <div id="user_tab_3">
                                <div class="description">
                                    <div class="descriptionText">
                                        <?php echo PCC::l()->val('index.user.tabs.3.dec'); ?>
                                    </div>
                                </div>
                                <div class="form_content">
                                    <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.useAcpMenu'); ?></div>
                                    <div class="form_element">
                                        <div style="margin: 5px;"  name="useAcpMenu">
                                            <label for="useAcpMenu_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="useAcpMenu" id="useAcpMenu_on" value="1" <?php echo ($premissions['useAcpMenu'] === true ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                            </label>
                                            <label for="useAcpMenu_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="useAcpMenu" id="useAcpMenu_off" value="0" <?php echo ($premissions['useAcpMenu'] == false ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.useAcpMenu.desc'); ?></div>
                                </div>
                                <div class="form_content">
                                    <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.acpUsers'); ?></div>
                                    <div class="form_element">
                                        <div style="margin: 5px;"  name="acpUsers">
                                            <label for="acpUsers_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="acpUsers" id="acpUsers_on" value="1" <?php echo ($premissions['acpUsers'] === true ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                            </label>
                                            <label for="acpUsers_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="acpUsers" id="acpUsers_off" value="0" <?php echo ($premissions['acpUsers'] == false ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.acpUsers.desc'); ?></div>
                                </div>
                                <div class="form_content">
                                    <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.acpRemote'); ?></div>
                                    <div class="form_element">
                                        <div style="margin: 5px;"  name="acpRemote">
                                            <label for="acpRemote_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="acpRemote" id="acpRemote_on" value="1" <?php echo ($premissions['acpRemote'] === true ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                            </label>
                                            <label for="acpRemote_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="acpRemote" id="acpRemote_off" value="0" <?php echo ($premissions['acpRemote'] == false ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.acpRemote.desc'); ?></div>
                                </div>
                                <div class="form_content">
                                    <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.acpSettings'); ?></div>
                                    <div class="form_element">
                                        <div style="margin: 5px;"  name="acpSettings">
                                            <label for="acpSettings_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="acpSettings" id="acpSettings_on" value="1" <?php echo ($premissions['acpSettings'] === true ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                            </label>
                                            <label for="acpSettings_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="acpSettings" id="acpSettings_off" value="0" <?php echo ($premissions['acpSettings'] == false ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.acpSettings.desc'); ?></div>
                                </div>
                                <div class="form_content">
                                    <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.acpUpdates'); ?></div>
                                    <div class="form_element">
                                        <div style="margin: 5px;"  name="acpUpdates">
                                            <label for="acpUpdates_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="acpUpdates" id="acpUpdates_on" value="1" <?php echo ($premissions['acpUpdates'] === true ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                            </label>
                                            <label for="acpUpdates_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                <input type="radio" style="margin: 0 5px 0 0;" name="acpUpdates" id="acpUpdates_off" value="0" <?php echo ($premissions['acpUpdates'] == false ? 'checked="checked" ' : ''); ?>/>
                                                <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.acpUpdates.desc'); ?></div>
                                </div>
                            </div>
                        <?php } ?>
                    </form>
                </div>
            </div>
            <script type="text/javascript">
                $(function() {

                    $('#user_tabs').tabs();
                    $('#user_editor').dialog({
                        buttons: {
                            '<?php echo utf8_decode(PCC::l()->val('index.user.buttons.back', null, false, false)); ?>': function() {
                                $.get('index.php?ajax=userlist', function(data, textStatus, jqXHR) {
                                    $('#user_editor').html(data);
                                });

                                $('#user_editor').dialog({
                                    buttons: {
                                        '<?php echo PCC::l()->val('index.user.buttons.createUser'); ?>': function() {

                                            $.get('index.php?ajax=userlist&req=create', function(data, textStatus, jqXHR) {
                                                $('#user_editor').html(data);
                                            });
                                        }
                                    }
                                });
                            },
                            '<?php echo PCC::l()->val('index.user.buttons.reset'); ?>': function() {

                                resetForm('#user_create');
                            },
                                    '<?php echo PCC::l()->val('index.user.buttons.save'); ?>': function() {

                                //Anmeldedaten an den Server schicken
                                var $form = $('#user_create');
                                var $inputs = $form.find("input, select, button, textarea");
                                var serializedData = $form.serialize();
                                $inputs.prop("disabled", true);

                                request = $.ajax({
                                    url: "index.php?ajax=userlist&req=edit&uid=<?php
            if ($user instanceof Visitor) {
                echo 'visitor';
            } else {
                echo $user->getUserId();
            }
            ?>",
                                    type: "post",
                                    data: serializedData
                                });

                                request.done(function(response, textStatus, jqXHR) {
                                    //Antwort vom Server
                                    $('#user_editor').html(response);
                                });

                                request.fail(function(jqXHR, textStatus, errorThrown) {
                                    //Anfrage Fehlgeschlagen

                                });

                                request.always(function() {
                                    $inputs.prop("disabled", false);
                                });
                            }
                        }
                    });
                });
            </script>
            <?php
        } elseif ($user !== null && count($_POST)) {

            //Daten Pruefen und Speichern
            $invalid = false;
            $invalidFields = array();

            //Nur fuer Benutzer
            if ($user instanceof User) {

                //Benutzername
                $name = $request->getParam('name', Request::POST, Request::STRING);
                if ($name === null || !preg_match('#[a-z0-9\#\_\!\-\.\,\;\+\*\?]{3,25}#i', $name)) {

                    $invalid = true;
                    $invalidFields[] = 'name';
                }

                //Pruefen ob der Benutzername frei ist
                if (!UserFactory::getFactory()->checkUserName($name) && $name != $user->getName()) {

                    $invalid = true;
                    $invalidFields[] = 'doubleName';
                }

                //Passwort
                $pass1 = $request->getParam('password', Request::POST, Request::STRING);
                $pass2 = $request->getParam('password1', Request::POST, Request::STRING);
                $pass_not_change = false;
                if ($pass1 == '' && $pass2 == '') {

                    //Passwort nicht aendern
                    $pass_not_change = true;
                } elseif ($pass1 === null || $pass2 === null || $pass1 != $pass2 || !String::checkLength($pass1, 5, 20)) {

                    $invalid = true;
                    $invalidFields[] = 'password';
                    $invalidFields[] = 'password1';
                }
            }

            //Benutzerechte
            $premissions = array();
            if ($user->isOriginator() == false) {

                //Statusseite
                $showState = $request->getParam('showState', Request::POST, Request::BOOL);
                if ($showState === null) {

                    $invalid = true;
                    $invalidFields[] = 'showState';
                } else {
                    $premissions['showState'] = $showState;
                }

                //Daten Seite
                $showData = $request->getParam('showData', Request::POST, Request::BOOL);
                if ($showData === null) {

                    $invalid = true;
                    $invalidFields[] = 'showData';
                } else {
                    $premissions['showData'] = $showData;
                }

                //Externe Raspberrys
                $remoteRPi = $request->getParam('remoteRPi', Request::POST, Request::BOOL);
                if ($remoteRPi === null) {

                    $invalid = true;
                    $invalidFields[] = 'remoteRPi';
                } else {
                    $premissions['remoteRPi'] = $remoteRPi;
                }

                //Statusdaten von externen Raspberrys
                $showRemoteRPiState = $request->getParam('showRemoteRPiState', Request::POST, Request::BOOL);
                if ($showRemoteRPiState === null) {

                    $invalid = true;
                    $invalidFields[] = 'showRemoteRPiState';
                } else {
                    $premissions['showRemoteRPiState'] = $showRemoteRPiState;
                }

                //Kenndaten von externen Raspberrys
                $showRemoteRPiData = $request->getParam('showRemoteRPiData', Request::POST, Request::BOOL);
                if ($showRemoteRPiData === null) {

                    $invalid = true;
                    $invalidFields[] = 'showRemoteRPiData';
                } else {
                    $premissions['showRemoteRPiData'] = $showRemoteRPiData;
                }

                //Admin Menue
                $useAcpMenu = $request->getParam('useAcpMenu', Request::POST, Request::BOOL);
                if ($useAcpMenu === null) {

                    $invalid = true;
                    $invalidFields[] = 'useAcpMenu';
                } else {
                    $premissions['useAcpMenu'] = $useAcpMenu;
                }

                //Benutzerverwaltung
                $acpUsers = $request->getParam('acpUsers', Request::POST, Request::BOOL);
                if ($acpUsers === null) {

                    $invalid = true;
                    $invalidFields[] = 'acpUsers';
                } else {
                    $premissions['acpUsers'] = $acpUsers;
                }

                //externen Raspberrys verwalten
                $acpRemote = $request->getParam('acpRemote', Request::POST, Request::BOOL);
                if ($acpRemote === null) {

                    $invalid = true;
                    $invalidFields[] = 'acpRemote';
                } else {
                    $premissions['acpRemote'] = $acpRemote;
                }

                //Einstellungen
                $acpSettings = $request->getParam('acpSettings', Request::POST, Request::BOOL);
                if ($acpSettings === null) {

                    $invalid = true;
                    $invalidFields[] = 'acpSettings';
                } else {
                    $premissions['acpSettings'] = $acpSettings;
                }

                //Aktualisierungen
                $acpUpdates = $request->getParam('acpUpdates', Request::POST, Request::BOOL);
                if ($acpUpdates === null) {

                    $invalid = true;
                    $invalidFields[] = 'acpUpdates';
                } else {
                    $premissions['acpUpdates'] = $acpUpdates;
                }
            }

            $successfully = false;
            if ($invalid === true) {

                //Formular nochmal Anzeigen
                ?>
                <div>
                    <div class="message_error">
                        <div class="message_icon"></div>
                        <div class="message"><?php echo PCC::l()->val('message.user.error'); ?></div>
                        <ul>
                            <?php
                            foreach ($invalidFields as $field) {
                                if ($field == 'password1') {
                                    continue;
                                }
                                ?>
                                <li><?php echo PCC::l()->val('message.user.create.inputError.' . $field); ?></li>
                            <?php } ?> 
                        </ul>
                    </div>
                </div>
                <div>
                    <div id="user_tabs">
                        <ul>
                            <?php if ($user instanceof User) { ?><li><a href="#user_tab_1"><?php echo PCC::l()->val('index.user.tabs.1'); ?></a></li><?php } ?>
                            <?php if ($user->isOriginator() == false) { ?>
                                <li><a href="#user_tab_2"><?php echo PCC::l()->val('index.user.tabs.2'); ?></a></li>
                                <li><a href="#user_tab_3"><?php echo PCC::l()->val('index.user.tabs.3'); ?></a></li>
                            <?php } ?>
                        </ul>
                        <form id="user_create">
                            <?php if ($user instanceof User) { ?>
                                <div id="user_tab_1">
                                    <div class="description">
                                        <div class="descriptionText">
                                            <?php echo PCC::l()->val('index.user.tabs.1.dec'); ?>
                                        </div>
                                    </div>
                                    <div class="form_content">
                                        <div class="form_title"><?php echo PCC::l()->val('index.user.form.name'); ?> <span class="red">*</span></div>
                                        <div class="form_element"><input type="text" name="name" class="defaultInputField<?php echo (in_array('name', $invalidFields) || in_array('doubleName', $invalidFields) ? ' invalidFormField' : ''); ?>" maxlength="25" value="<?php echo String::encodeHTML($name); ?>" /></div>
                                        <div class="form_description"><?php echo PCC::l()->val('index.user.form.name.desc'); ?></div>
                                    </div>
                                    <div class="form_content">
                                        <div class="form_title"><?php echo PCC::l()->val('index.user.form.pass'); ?> <span class="red">*</span></div>
                                        <div class="form_element"><input type="password" name="password" class="defaultInputField<?php echo (in_array('password', $invalidFields) ? ' invalidFormField' : ''); ?>" maxlength="20" /></div>
                                        <div class="form_description"><?php echo PCC::l()->val('index.user.form.pass.desc'); ?></div>
                                    </div>
                                    <div class="form_content">
                                        <div class="form_title"><?php echo PCC::l()->val('index.user.form.pass1'); ?> <span class="red">*</span></div>
                                        <div class="form_element"><input type="password" name="password1" class="defaultInputField<?php echo (in_array('password1', $invalidFields) ? ' invalidFormField' : ''); ?>" maxlength="20" /></div>
                                        <div class="form_description"><?php echo PCC::l()->val('index.user.form.pass1.desc'); ?></div>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($user->isOriginator() == false) { ?>
                                <div id="user_tab_2">
                                    <div class="description">
                                        <div class="descriptionText">
                                            <?php echo PCC::l()->val('index.user.tabs.2.dec'); ?>
                                        </div>
                                    </div>
                                    <div class="form_content">
                                        <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.showState'); ?></div>
                                        <div class="form_element">
                                            <div style="margin: 5px;"  name="showState">
                                                <label for="showState_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                    <input type="radio" style="margin: 0 5px 0 0;" name="showState" id="showState_on" value="1" <?php echo ($showState === true ? 'checked="checked" ' : ''); ?>/>
                                                    <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                                </label>
                                                <label for="showState_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                    <input type="radio" style="margin: 0 5px 0 0;" name="showState" id="showState_off" value="0" <?php echo ($showState == false ? 'checked="checked" ' : ''); ?>/>
                                                    <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.showState.desc'); ?></div>
                                    </div>
                                    <div class="form_content">
                                        <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.showData'); ?></div>
                                        <div class="form_element">
                                            <div style="margin: 5px;"  name="showData">
                                                <label for="showData_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                    <input type="radio" style="margin: 0 5px 0 0;" name="showData" id="showData_on" value="1" <?php echo ($showData === true ? 'checked="checked" ' : ''); ?>/>
                                                    <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                                </label>
                                                <label for="showData_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                    <input type="radio" style="margin: 0 5px 0 0;" name="showData" id="showData_off" value="0" <?php echo ($showData == false ? 'checked="checked" ' : ''); ?>/>
                                                    <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.showData.desc'); ?></div>
                                    </div>
                                    <div class="form_content">
                                        <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.remoteRPi'); ?></div>
                                        <div class="form_element">
                                            <div style="margin: 5px;"  name="remoteRPi">
                                                <label for="remoteRPi_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                    <input type="radio" style="margin: 0 5px 0 0;" name="remoteRPi" id="remoteRPi_on" value="1" <?php echo ($remoteRPi === true ? 'checked="checked" ' : ''); ?>/>
                                                    <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                                </label>
                                                <label for="remoteRPi_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                    <input type="radio" style="margin: 0 5px 0 0;" name="remoteRPi" id="remoteRPi_off" value="0" <?php echo ($remoteRPi == false ? 'checked="checked" ' : ''); ?>/>
                                                    <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.remoteRPi.desc'); ?></div>
                                    </div>
                                    <div class="form_content">
                                        <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.showRemoteRPiState'); ?></div>
                                        <div class="form_element">
                                            <div style="margin: 5px;"  name="showRemoteRPiState">
                                                <label for="showRemoteRPiState_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                    <input type="radio" style="margin: 0 5px 0 0;" name="showRemoteRPiState" id="showRemoteRPiState_on" value="1" <?php echo ($showRemoteRPiState === true ? 'checked="checked" ' : ''); ?>/>
                                                    <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                                </label>
                                                <label for="showRemoteRPiState_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                    <input type="radio" style="margin: 0 5px 0 0;" name="showRemoteRPiState" id="showRemoteRPiState_off" value="0" <?php echo ($showRemoteRPiState == false ? 'checked="checked" ' : ''); ?>/>
                                                    <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.showRemoteRPiState.desc'); ?></div>
                                    </div>
                                    <div class="form_content">
                                        <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.showRemoteRPiData'); ?></div>
                                        <div class="form_element">
                                            <div style="margin: 5px;"  name="showRemoteRPiData">
                                                <label for="showRemoteRPiData_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                    <input type="radio" style="margin: 0 5px 0 0;" name="showRemoteRPiData" id="showRemoteRPiData_on" value="1" <?php echo ($showRemoteRPiData === true ? 'checked="checked" ' : ''); ?>/>
                                                    <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                                </label>
                                                <label for="showRemoteRPiData_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                    <input type="radio" style="margin: 0 5px 0 0;" name="showRemoteRPiData" id="showRemoteRPiData_off" value="0" <?php echo ($showRemoteRPiData == false ? 'checked="checked" ' : ''); ?>/>
                                                    <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.showRemoteRPiData.desc'); ?></div>
                                    </div>
                                </div>
                                <div id="user_tab_3">
                                    <div class="description">
                                        <div class="descriptionText">
                                            <?php echo PCC::l()->val('index.user.tabs.3.dec'); ?>
                                        </div>
                                    </div>
                                    <div class="form_content">
                                        <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.useAcpMenu'); ?></div>
                                        <div class="form_element">
                                            <div style="margin: 5px;"  name="useAcpMenu">
                                                <label for="useAcpMenu_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                    <input type="radio" style="margin: 0 5px 0 0;" name="useAcpMenu" id="useAcpMenu_on" value="1" <?php echo ($useAcpMenu === true ? 'checked="checked" ' : ''); ?>/>
                                                    <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                                </label>
                                                <label for="useAcpMenu_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                    <input type="radio" style="margin: 0 5px 0 0;" name="useAcpMenu" id="useAcpMenu_off" value="0" <?php echo ($useAcpMenu == false ? 'checked="checked" ' : ''); ?>/>
                                                    <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.useAcpMenu.desc'); ?></div>
                                    </div>
                                    <div class="form_content">
                                        <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.acpUsers'); ?></div>
                                        <div class="form_element">
                                            <div style="margin: 5px;"  name="acpUsers">
                                                <label for="acpUsers_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                    <input type="radio" style="margin: 0 5px 0 0;" name="acpUsers" id="acpUsers_on" value="1" <?php echo ($acpUsers === true ? 'checked="checked" ' : ''); ?>/>
                                                    <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                                </label>
                                                <label for="acpUsers_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                    <input type="radio" style="margin: 0 5px 0 0;" name="acpUsers" id="acpUsers_off" value="0" <?php echo ($acpUsers == false ? 'checked="checked" ' : ''); ?>/>
                                                    <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.acpUsers.desc'); ?></div>
                                    </div>
                                    <div class="form_content">
                                        <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.acpRemote'); ?></div>
                                        <div class="form_element">
                                            <div style="margin: 5px;"  name="acpRemote">
                                                <label for="acpRemote_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                    <input type="radio" style="margin: 0 5px 0 0;" name="acpRemote" id="acpRemote_on" value="1" <?php echo ($acpRemote === true ? 'checked="checked" ' : ''); ?>/>
                                                    <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                                </label>
                                                <label for="acpRemote_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                    <input type="radio" style="margin: 0 5px 0 0;" name="acpRemote" id="acpRemote_off" value="0" <?php echo ($acpRemote == false ? 'checked="checked" ' : ''); ?>/>
                                                    <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.acpRemote.desc'); ?></div>
                                    </div>
                                    <div class="form_content">
                                        <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.acpSettings'); ?></div>
                                        <div class="form_element">
                                            <div style="margin: 5px;"  name="acpSettings">
                                                <label for="acpSettings_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                    <input type="radio" style="margin: 0 5px 0 0;" name="acpSettings" id="acpSettings_on" value="1" <?php echo ($acpSettings === true ? 'checked="checked" ' : ''); ?>/>
                                                    <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                                </label>
                                                <label for="acpSettings_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                    <input type="radio" style="margin: 0 5px 0 0;" name="acpSettings" id="acpSettings_off" value="0" <?php echo ($acpSettings == false ? 'checked="checked" ' : ''); ?>/>
                                                    <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.acpSettings.desc'); ?></div>
                                    </div>
                                    <div class="form_content">
                                        <div class="form_title"><?php echo PCC::l()->val('index.user.form.premmission.acpUpdates'); ?></div>
                                        <div class="form_element">
                                            <div style="margin: 5px;"  name="acpUpdates">
                                                <label for="acpUpdates_on" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #d6eca6; border: #8dc93e solid 2px;">
                                                    <input type="radio" style="margin: 0 5px 0 0;" name="acpUpdates" id="acpUpdates_on" value="1" <?php echo ($acpUpdates === true ? 'checked="checked" ' : ''); ?>/>
                                                    <?php echo PCC::l()->val('global.form.yesno.yes'); ?>
                                                </label>
                                                <label for="acpUpdates_off" style="font-weight: bold; font-size: 0.9em; margin: 5px 0 0 5px; padding: 4px 6px; display: inline-block; width: 60px; background: #eca6d6; border: #c93e8d solid 2px;">
                                                    <input type="radio" style="margin: 0 5px 0 0;" name="acpUpdates" id="acpUpdates_off" value="0" <?php echo ($acpUpdates == false ? 'checked="checked" ' : ''); ?>/>
                                                    <?php echo PCC::l()->val('global.form.yesno.no'); ?>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form_description"><?php echo PCC::l()->val('index.user.form.premmission.acpUpdates.desc'); ?></div>
                                    </div>
                                </div>
                            <?php } ?>
                        </form>
                    </div>
                </div>
                <script type="text/javascript">
                    $(function() {

                        $('#user_tabs').tabs();
                        $('#user_editor').dialog({
                            buttons: {
                                '<?php echo utf8_decode(PCC::l()->val('index.user.buttons.back', null, false, false)); ?>': function() {
                                    $.get('index.php?ajax=userlist', function(data, textStatus, jqXHR) {
                                        $('#user_editor').html(data);
                                    });

                                    $('#user_editor').dialog({
                                        buttons: {
                                            '<?php echo PCC::l()->val('index.user.buttons.createUser'); ?>': function() {

                                                $.get('index.php?ajax=userlist&req=create', function(data, textStatus, jqXHR) {
                                                    $('#user_editor').html(data);
                                                });
                                            }
                                        }
                                    });
                                },
                                '<?php echo PCC::l()->val('index.user.buttons.reset'); ?>': function() {

                                    resetForm('#user_create');
                                },
                                        '<?php echo PCC::l()->val('index.user.buttons.save'); ?>': function() {

                                    //Anmeldedaten an den Server schicken
                                    var $form = $('#user_create');
                                    var $inputs = $form.find("input, select, button, textarea");
                                    var serializedData = $form.serialize();
                                    $inputs.prop("disabled", true);

                                    request = $.ajax({
                                        url: "index.php?ajax=userlist&req=edit&uid=<?php
                if ($user instanceof Visitor) {
                    echo 'visitor';
                } else {
                    echo $user->getUserId();
                }
                ?>",
                                        type: "post",
                                        data: serializedData
                                    });

                                    request.done(function(response, textStatus, jqXHR) {
                                        //Antwort vom Server
                                        $('#user_editor').html(response);
                                    });

                                    request.fail(function(jqXHR, textStatus, errorThrown) {
                                        //Anfrage Fehlgeschlagen

                                    });

                                    request.always(function() {
                                        $inputs.prop("disabled", false);
                                    });
                                }
                            }
                        });
                    });
                </script>
                <?php
            } elseif ($invalid === false && $user instanceof User && $pass_not_change == false && UserFactory::getFactory()->editUser($request->getParam('uid', Request::GET, Request::INT), $name, $pass1, $premissions)) {

                $successfully = true;
            } elseif ($invalid === false && $user instanceof User && $pass_not_change == true && UserFactory::getFactory()->editUser($request->getParam('uid', Request::GET, Request::INT), $name, null, $premissions)) {

                $successfully = true;
            } elseif ($invalid === false && $user instanceof Visitor && UserFactory::getFactory()->editVisitor($premissions)) {

                $successfully = true;
            }

            if ($invalid == false && $successfully === true) {

                //Erfolgsmeldung
                ?>
                <div>
                    <div class="message_successfully">
                        <div class="message_icon"></div>
                        <div class="message"><?php echo PCC::l()->val('message.user.edit.success'); ?></div>
                    </div>
                </div>
                <script type="text/javascript">
                    $('#user_editor').dialog({
                        buttons: {
                            '<?php echo utf8_decode(PCC::l()->val('index.user.buttons.back')); ?>': function() {
                                $.get('index.php?ajax=userlist', function(data, textStatus, jqXHR) {
                                    $('#user_editor').html(data);
                                });

                                $('#user_editor').dialog({
                                    buttons: {
                                        '<?php echo PCC::l()->val('index.user.buttons.createUser'); ?>': function() {

                                            $.get('index.php?ajax=userlist&req=create', function(data, textStatus, jqXHR) {
                                                $('#user_editor').html(data);
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    });
                </script>
                <?php
            } elseif ($invalid == false) {

                //Fehlschlag
                ?>
                <div>
                    <div class="message_error">
                        <div class="message_icon"></div>
                        <div class="message"><?php echo PCC::l()->val('message.user.edit.error'); ?></div>
                    </div>
                </div>
                <script type="text/javascript">
                    $('#user_editor').dialog({
                        buttons: {
                            '<?php echo utf8_decode(PCC::l()->val('index.user.buttons.back')); ?>': function() {
                                $.get('index.php?ajax=userlist', function(data, textStatus, jqXHR) {
                                    $('#user_editor').html(data);
                                });

                                $('#user_editor').dialog({
                                    buttons: {
                                        '<?php echo PCC::l()->val('index.user.buttons.createUser'); ?>': function() {

                                            $.get('index.php?ajax=userlist&req=create', function(data, textStatus, jqXHR) {
                                                $('#user_editor').html(data);
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    });
                </script>
                <?php
            }
        } else {

            //Benutzer existiert nicht
            ?>
            <div>
                <div class="message_error">
                    <div class="message_icon"></div>
                    <div class="message"><?php echo PCC::l()->val('message.user.edit.notExists'); ?></div>
                </div>
            </div>
            <?php
        }
    } elseif ($request->issetParam('req', Request::GET) && $request->getParam('req', Request::GET, Request::STRING) == 'delete' && $request->issetParam('uid', Request::GET)) {

        //Benutzer loeschen
        $id = $request->getParam('uid', Request::GET, Request::INT);
        if (UserFactory::getFactory()->deleteUser($id)) {
            ?>
            <div>
                <div class="message_successfully">
                    <div class="message_icon"></div>
                    <div class="message"><?php echo PCC::l()->val('message.user.del.success'); ?></div>
                </div>
            </div>
            <?php
        } else {
            ?>
            <div>
                <div class="message_error">
                    <div class="message_icon"></div>
                    <div class="message"><?php echo PCC::l()->val('message.user.del.error'); ?></div>
                </div>
            </div>
            <?php
        }
        ?>
        <script type="text/javascript">
            $(function() {

                $('#user_editor').dialog({
                    buttons: {
                        '<?php echo utf8_decode(PCC::l()->val('index.user.buttons.back', null, true, false)); ?>': function() {
                            $.get('index.php?ajax=userlist', function(data, textStatus, jqXHR) {
                                $('#user_editor').html(data);
                            });

                            $('#user_editor').dialog({
                                buttons: {
                                    '<?php echo PCC::l()->val('index.user.buttons.createUser'); ?>': function() {

                                        $.get('index.php?ajax=userlist&req=create', function(data, textStatus, jqXHR) {
                                            $('#user_editor').html(data);
                                        });
                                    }
                                }
                            });
                        }
                    }
                });
            });
        </script>
        <?php
    } else {

        //Benutzerliste
        ?>
        <div>
            <table class="content_table">
                <thead>
                    <tr>
                        <th style="width: 70%"><?php echo PCC::l()->val('index.user.table.user'); ?></th>
                        <th style="width: 20%"><?php echo PCC::l()->val('index.user.table.registerDate'); ?></th>
                        <th style="width: 10%"><?php echo PCC::l()->val('index.user.table.options'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?php echo PCC::l()->val('index.user.guest'); ?></td>
                        <td></td>
                        <td>
                            <a href="#" id="edit_visitor" class="edit" title="<?php echo PCC::l()->val('index.tooltip.edit'); ?>"></a>
                        </td>
                    </tr>
                <script type="text/javascript">
                    $(function() {

                        $('#edit_visitor').click(function() {

                            $.get('index.php?ajax=userlist&req=edit&uid=visitor', function(data, textStatus, jqXHR) {
                                $('#user_editor').html(data);
                            });
                        });
                    });
                </script>
                <?php foreach (UserFactory::getFactory()->listUsers() as $user) { ?>
                    <tr>
                        <td><?php echo String::encodeHTML($user['name']); ?></td>
                        <td>
                            <?php
                            $date = new DateTimeExtendet($user['date']);
                            echo $date->format('d.m.Y');
                            ?>
                        </td>
                        <td>
                            <a href="#"  id="edit_user_<?php echo $user['id']; ?>" class="edit" title="<?php echo PCC::l()->val('index.tooltip.edit'); ?>"></a>
                            <?php if ($user['isOriginator'] !== true) { ?>
                                <a href="#" id="del_user_<?php echo $user['id']; ?>" class="delete" title="<?php echo PCC::l()->val('index.tooltip.delete'); ?>"></a>
                            <?php } ?>
                        </td>
                    </tr>
                    <script type="text/javascript">
                        $(function() {

                            $('.edit, .delete').tooltip({
                                track: false,
                                position: {
                                    my: "left+15 center",
                                    at: "right center"
                                }
                            });

                            $('#edit_user_<?php echo $user['id']; ?>').click(function() {

                                $.get('index.php?ajax=userlist&req=edit&uid=<?php echo $user['id']; ?>', function(data, textStatus, jqXHR) {
                                    $('#user_editor').html(data);
                                });
                            });
                            $('#del_user_<?php echo $user['id']; ?>').click(function() {

                                $.get('index.php?ajax=userlist&req=delete&uid=<?php echo $user['id']; ?>', function(data, textStatus, jqXHR) {
                                    $('#user_editor').html(data);
                                });
                            });
                        });
                    </script>
                <?php } ?>
                </tbody>
            </table>
        </div>
        <?php
    }
} else {
    ?>
    <div>
        <div class="message_error">
            <div class="message_icon"></div>
            <div class="message"><?php echo PCC::l()->val('message.accessdenied'); ?></div>
        </div>
    </div>
    <?php
}
?>
<?php
/**
 * Verwaltung der Remote Server
 * 
 * @author     Oliver Kleditzsch
 * @copyright  Copyright (c) 2013, Oliver Kleditzsch
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License
 * @since      1.0.0-0
 * @version    1.0.0-0
 */
PCC::l()->loadModule('index');
if (PCC::getUser()->checkPremission('acpRemote')) {

    $request = PCC::getRequest();
    if ($request->issetParam('req') && $request->getParam('req', Request::GET, Request::STRING) == 'create') {

        if (!count($_POST)) {
            //Formular zum Eintragen
            ?>
            <div>
                <form id="rpi_create">
                    <div class="description">
                        <div class="descriptionText">
                            <?php echo PCC::l()->val('index.remote.create.desc'); ?>
                        </div>
                    </div>
                    <div class="form_content">
                        <div class="form_title"><?php echo PCC::l()->val('index.remote.create.name'); ?> <span class="red">*</span></div>
                        <div class="form_element"><input type="text" name="name" class="defaultInputField" value="" maxlength="60" /></div>
                        <div class="form_description"><?php echo PCC::l()->val('index.remote.create.name.desc'); ?></div>
                    </div>
                    <div class="form_content">
                        <div class="form_title"><?php echo PCC::l()->val('index.remote.create.ip'); ?> <span class="red">*</span></div>
                        <div class="form_element">
                            <input type="text" name="ip0" class="defaultInputField" value="192" maxlength="3" style="width: 3em;" />
                            .
                            <input type="text" name="ip1" class="defaultInputField" value="168" maxlength="3" style="width: 3em;" />
                            .
                            <input type="text" name="ip2" class="defaultInputField" value="" maxlength="3" style="width: 3em;" />
                            .
                            <input type="text" class="defaultInputField" name="ip3" value="" maxlength="3" style="width: 3em;" />
                        </div>
                        <div class="form_description"><?php echo PCC::l()->val('index.remote.create.ip.desc'); ?></div>
                    </div>
                    <div class="form_content">
                        <div class="form_title"><?php echo PCC::l()->val('index.remote.create.port'); ?> <span class="red">*</span></div>
                        <div class="form_element"><input type="text" name="port" class="defaultInputField" value="9273" maxlength="5" /></div>
                        <div class="form_description"><?php echo PCC::l()->val('index.remote.create.port.desc'); ?></div>
                    </div>
                </form>
            </div>
            <script type="text/javascript">
                $(function() {

                    $('#remote_editor').dialog({
                        buttons: {
                            '<?php echo utf8_decode(PCC::l()->val('index.user.buttons.back', null, false, false)); ?>': function() {
                                $.get('index.php?ajax=rrpiacp', function(data, textStatus, jqXHR) {
                                    $('#remote_editor').html(data);
                                });

                                $('#remote_editor').dialog({
                                    buttons: {
                                        '<?php echo PCC::l()->val('index.remote.create.button'); ?>': function() {

                                            $.get('index.php?ajax=rrpiacp&req=create', function(data, textStatus, jqXHR) {
                                                $('#remote_editor').html(data);
                                            });
                                        }
                                    }
                                });
                            },
                            '<?php echo PCC::l()->val('index.user.buttons.reset'); ?>': function() {

                                resetForm('#rpi_create');
                            },
                                    '<?php echo PCC::l()->val('index.user.buttons.save'); ?>': function() {

                                //Anmeldedaten an den Server schicken
                                var $form = $('#rpi_create');
                                var $inputs = $form.find("input, select, button, textarea");
                                var serializedData = $form.serialize();
                                $inputs.prop("disabled", true);

                                request = $.ajax({
                                    url: "index.php?ajax=rrpiacp&req=create",
                                    type: "post",
                                    data: serializedData
                                });

                                request.done(function(response, textStatus, jqXHR) {
                                    //Antwort vom Server
                                    $('#remote_editor').html(response);
                                });

                                request.fail(function(jqXHR, textStatus, errorThrown) {
                                    //Anfrage Fehlgeschlagen

                                });

                                request.always(function() {
                                    $inputs.prop("disabled", false);
                                });
                            }
                        }
                    });
                });
            </script>
            <?php
        } else {

            //Formulardaten Pruefen und Speichern
            $invalid = false;
            $invalidFields = array();

            //Name des RPi
            $name = $request->getParam('name', Request::POST, Request::STRING);
            if (!String::checkLength($name, 1, 60)) {

                $invalid = true;
                $invalidFields[] = 'name';
            }

            //IP Adresse
            $ip0 = '';
            try {

                $ip0 = $request->getParam('ip0', Request::POST, Request::INT);
                if ($ip0 < 0 || $ip0 > 255) {

                    $invalid = true;
                    $invalidFields[] = 'ip0';
                }
            } catch (Exception $e) {

                $invalid = true;
                $invalidFields[] = 'ip0';
            }

            $ip1 = '';
            try {

                $ip1 = $request->getParam('ip1', Request::POST, Request::INT);
                if ($ip1 < 0 || $ip1 > 255) {

                    $invalid = true;
                    $invalidFields[] = 'ip1';
                }
            } catch (Exception $e) {

                $invalid = true;
                $invalidFields[] = 'ip1';
            }

            $ip2 = '';
            try {

                $ip2 = $request->getParam('ip2', Request::POST, Request::INT);
                if ($ip2 < 0 || $ip2 > 255) {

                    $invalid = true;
                    $invalidFields[] = 'ip2';
                }
            } catch (Exception $e) {

                $invalid = true;
                $invalidFields[] = 'ip2';
            }

            $ip3 = '';
            try {

                $ip3 = $request->getParam('ip3', Request::POST, Request::INT);
                if ($ip3 < 0 || $ip3 > 255) {

                    $invalid = true;
                    $invalidFields[] = 'ip3';
                }
            } catch (Exception $e) {

                $invalid = true;
                $invalidFields[] = 'ip3';
            }

            //Port
            $port = '';
            try {
                $port = $request->getParam('port', Request::POST, Request::INT);
                if ($port < 0 || $port > 65535) {

                    $invalid = true;
                    $invalidFields[] = 'port';
                }
            } catch (Exception $e) {

                $invalid = true;
                $invalidFields[] = 'port';
            }

            //Daten Speichern
            if ($invalid === true) {

                //Fehlerhafte eingaben
                ?>
                <div>
                    <div class="message_error">
                        <div class="message_icon"></div>
                        <div class="message"><?php echo PCC::l()->val('message.remote.create.error'); ?></div>
                        <ul>
                            <?php foreach ($invalidFields as $field) { ?>
                                <li><?php echo PCC::l()->val('message.remote.create.inputError.' . $field); ?></li>
                            <?php } ?> 
                        </ul>
                    </div>
                </div>
                <div>
                    <form id="rpi_create">
                        <div class="description">
                            <div class="descriptionText">
                                <?php echo PCC::l()->val('index.remote.create.desc'); ?>
                            </div>
                        </div>
                        <div class="form_content">
                            <div class="form_title"><?php echo PCC::l()->val('index.remote.create.name'); ?> <span class="red">*</span></div>
                            <div class="form_element"><input type="text" name="name" class="defaultInputField<?php echo (in_array('name', $invalidFields) ? ' invalidFormField' : ''); ?>" value="<?php echo String::encodeHTML($name); ?>" maxlength="60" /></div>
                            <div class="form_description"><?php echo PCC::l()->val('index.remote.create.name.desc'); ?></div>
                        </div>
                        <div class="form_content">
                            <div class="form_title"><?php echo PCC::l()->val('index.remote.create.ip'); ?> <span class="red">*</span></div>
                            <div class="form_element">
                                <input type="text" name="ip0" class="defaultInputField<?php echo (in_array('ip0', $invalidFields) ? ' invalidFormField' : ''); ?>" value="<?php echo String::encodeHTML($ip0); ?>" maxlength="3" style="width: 3em;" />
                                .
                                <input type="text" name="ip1" class="defaultInputField<?php echo (in_array('ip1', $invalidFields) ? ' invalidFormField' : ''); ?>" value="<?php echo String::encodeHTML($ip1); ?>" maxlength="3" style="width: 3em;" />
                                .
                                <input type="text" name="ip2" class="defaultInputField<?php echo (in_array('ip2', $invalidFields) ? ' invalidFormField' : ''); ?>" value="<?php echo String::encodeHTML($ip2); ?>" maxlength="3" style="width: 3em;" />
                                .
                                <input type="text" name="ip3" class="defaultInputField<?php echo (in_array('ip3', $invalidFields) ? ' invalidFormField' : ''); ?>" value="<?php echo String::encodeHTML($ip3); ?>" maxlength="3" style="width: 3em;" />
                            </div>
                            <div class="form_description"><?php echo PCC::l()->val('index.remote.create.ip.desc'); ?></div>
                        </div>
                        <div class="form_content">
                            <div class="form_title"><?php echo PCC::l()->val('index.remote.create.port'); ?> <span class="red">*</span></div>
                            <div class="form_element"><input type="text" name="port" class="defaultInputField<?php echo (in_array('port', $invalidFields) ? ' invalidFormField' : ''); ?>" value="<?php echo String::encodeHTML($port); ?>" maxlength="5" /></div>
                            <div class="form_description"><?php echo PCC::l()->val('index.remote.create.port.desc'); ?></div>
                        </div>
                    </form>
                </div>
                <?php
            } else {

                if (RemoteRPi::createRPi($name, $ip0 . '.' . $ip1 . '.' . $ip2 . '.' . $ip3, $port)) {

                    //Erfolgreich
                    ?>
                    <div>
                        <div class="message_successfully">
                            <div class="message_icon"></div>
                            <div class="message"><?php echo PCC::l()->val('message.remote.success'); ?></div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $('#remote_editor').dialog({
                            buttons: {
                                '<?php echo utf8_decode(PCC::l()->val('index.user.buttons.back', null, false, false)); ?>': function() {
                                    $.get('index.php?ajax=rrpiacp', function(data, textStatus, jqXHR) {
                                        $('#remote_editor').html(data);
                                    });

                                    $('#remote_editor').dialog({
                                        buttons: {
                                            '<?php echo PCC::l()->val('index.remote.create.button'); ?>': function() {

                                                $.get('index.php?ajax=rrpiacp&req=create', function(data, textStatus, jqXHR) {
                                                    $('#remote_editor').html(data);
                                                });
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    </script>
                    <?php
                } else {

                    //Fehlgeschlagen
                    ?>
                    <div>
                        <div class="message_error">
                            <div class="message_icon"></div>
                            <div class="message"><?php echo PCC::l()->val('message.remote.error'); ?></div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $('#remote_editor').dialog({
                            buttons: {
                                '<?php echo utf8_decode(PCC::l()->val('index.user.buttons.back', null, false, false)); ?>': function() {
                                    $.get('index.php?ajax=rrpiacp', function(data, textStatus, jqXHR) {
                                        $('#remote_editor').html(data);
                                    });

                                    $('#remote_editor').dialog({
                                        buttons: {
                                            '<?php echo PCC::l()->val('index.remote.create.button'); ?>': function() {

                                                $.get('index.php?ajax=rrpiacp&req=create', function(data, textStatus, jqXHR) {
                                                    $('#remote_editor').html(data);
                                                });
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    </script>
                    <?php
                }
            }
        }
    } elseif ($request->issetParam('req') && $request->getParam('req', Request::GET, Request::STRING) == 'edit' && $request->issetParam('rid', Request::GET)) {

        //Raspberry Pi bearbeiten
        if (!count($_POST)) {
            try {

                $rpi = new RemoteRPi($request->getParam('rid', Request::GET, Request::INT));
            } catch (Exception $e) {
                ?>
                <div>
                    <div class="message_error">
                        <div class="message_icon"></div>
                        <div class="message"><?php echo PCC::l()->val('message.remote.edit.notExists'); ?></div>
                    </div>
                </div>
                <script type="text/javascript">
                    $('#remote_editor').dialog({
                        buttons: {
                            '<?php echo utf8_decode(PCC::l()->val('index.user.buttons.back', null, false, false)); ?>': function() {
                                $.get('index.php?ajax=rrpiacp', function(data, textStatus, jqXHR) {
                                    $('#remote_editor').html(data);
                                });

                                $('#remote_editor').dialog({
                                    buttons: {
                                        '<?php echo PCC::l()->val('index.remote.create.button'); ?>': function() {

                                            $.get('index.php?ajax=rrpiacp&req=create', function(data, textStatus, jqXHR) {
                                                $('#remote_editor').html(data);
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    });
                </script>
                <?php
            }
            $parts = preg_split('#\.#', $rpi->getAddress());
            ?>
            <div>
                <form id="rpi_create">
                    <div class="description">
                        <div class="descriptionText">
            <?php echo PCC::l()->val('index.remote.create.desc'); ?>
                        </div>
                    </div>
                    <div class="form_content">
                        <div class="form_title"><?php echo PCC::l()->val('index.remote.create.name'); ?> <span class="red">*</span></div>
                        <div class="form_element"><input type="text" name="name" class="defaultInputField" value="<?php echo String::encodeHTML($rpi->getName()); ?>" maxlength="60" /></div>
                        <div class="form_description"><?php echo PCC::l()->val('index.remote.create.name.desc'); ?></div>
                    </div>
                    <div class="form_content">
                        <div class="form_title"><?php echo PCC::l()->val('index.remote.create.ip'); ?> <span class="red">*</span></div>
                        <div class="form_element">
                            <input type="text" name="ip0" class="defaultInputField" value="<?php echo String::encodeHTML($parts[0]); ?>" maxlength="3" style="width: 3em;" />
                            .
                            <input type="text" name="ip1" class="defaultInputField" value="<?php echo String::encodeHTML($parts[1]); ?>" maxlength="3" style="width: 3em;" />
                            .
                            <input type="text" name="ip2" class="defaultInputField" value="<?php echo String::encodeHTML($parts[2]); ?>" maxlength="3" style="width: 3em;" />
                            .
                            <input type="text" name="ip3" class="defaultInputField" value="<?php echo String::encodeHTML($parts[3]); ?>" maxlength="3" style="width: 3em;" />
                        </div>
                        <div class="form_description"><?php echo PCC::l()->val('index.remote.create.ip.desc'); ?></div>
                    </div>
                    <div class="form_content">
                        <div class="form_title"><?php echo PCC::l()->val('index.remote.create.port'); ?> <span class="red">*</span></div>
                        <div class="form_element"><input type="text" name="port" class="defaultInputField" value="<?php echo String::encodeHTML($rpi->getPort()); ?>" maxlength="5" /></div>
                        <div class="form_description"><?php echo PCC::l()->val('index.remote.create.port.desc'); ?></div>
                    </div>
                </form>
            </div>
            <script type="text/javascript">
                $(function() {

                    $('#remote_editor').dialog({
                        buttons: {
                            '<?php echo utf8_decode(PCC::l()->val('index.user.buttons.back', null, false, false)); ?>': function() {
                                $.get('index.php?ajax=rrpiacp', function(data, textStatus, jqXHR) {
                                    $('#remote_editor').html(data);
                                });

                                $('#remote_editor').dialog({
                                    buttons: {
                                        '<?php echo PCC::l()->val('index.remote.create.button'); ?>': function() {

                                            $.get('index.php?ajax=rrpiacp&req=create', function(data, textStatus, jqXHR) {
                                                $('#remote_editor').html(data);
                                            });
                                        }
                                    }
                                });
                            },
                            '<?php echo PCC::l()->val('index.user.buttons.reset'); ?>': function() {

                                resetForm('#rpi_create');
                            },
                                    '<?php echo PCC::l()->val('index.user.buttons.save'); ?>': function() {

                                //Anmeldedaten an den Server schicken
                                var $form = $('#rpi_create');
                                var $inputs = $form.find("input, select, button, textarea");
                                var serializedData = $form.serialize();
                                $inputs.prop("disabled", true);

                                request = $.ajax({
                                    url: "index.php?ajax=rrpiacp&req=edit&rid=<?php echo $rpi->getId(); ?>",
                                    type: "post",
                                    data: serializedData
                                });

                                request.done(function(response, textStatus, jqXHR) {
                                    //Antwort vom Server
                                    $('#remote_editor').html(response);
                                });

                                request.fail(function(jqXHR, textStatus, errorThrown) {
                                    //Anfrage Fehlgeschlagen

                                });

                                request.always(function() {
                                    $inputs.prop("disabled", false);
                                });
                            }
                        }
                    });
                });
            </script>
            <?php
        } else {

            //Daten Pruefen und Speichern
            //Formulardaten Pruefen und Speichern
            $invalid = false;
            $invalidFields = array();

            //Name des RPi
            $name = $request->getParam('name', Request::POST, Request::STRING);
            if (!String::checkLength($name, 1, 60)) {

                $invalid = true;
                $invalidFields[] = 'name';
            }

            //IP Adresse
            $ip0 = '';
            try {

                $ip0 = $request->getParam('ip0', Request::POST, Request::INT);
                if ($ip0 < 0 || $ip0 > 255) {

                    $invalid = true;
                    $invalidFields[] = 'ip0';
                }
            } catch (Exception $e) {

                $invalid = true;
                $invalidFields[] = 'ip0';
            }

            $ip1 = '';
            try {

                $ip1 = $request->getParam('ip1', Request::POST, Request::INT);
                if ($ip1 < 0 || $ip1 > 255) {

                    $invalid = true;
                    $invalidFields[] = 'ip1';
                }
            } catch (Exception $e) {

                $invalid = true;
                $invalidFields[] = 'ip1';
            }

            $ip2 = '';
            try {

                $ip2 = $request->getParam('ip2', Request::POST, Request::INT);
                if ($ip2 < 0 || $ip2 > 255) {

                    $invalid = true;
                    $invalidFields[] = 'ip2';
                }
            } catch (Exception $e) {

                $invalid = true;
                $invalidFields[] = 'ip2';
            }

            $ip3 = '';
            try {

                $ip3 = $request->getParam('ip3', Request::POST, Request::INT);
                if ($ip3 < 0 || $ip3 > 255) {

                    $invalid = true;
                    $invalidFields[] = 'ip3';
                }
            } catch (Exception $e) {

                $invalid = true;
                $invalidFields[] = 'ip3';
            }

            //Port
            $port = '';
            try {
                $port = $request->getParam('port', Request::POST, Request::INT);
                if ($port < 0 || $port > 65535) {

                    $invalid = true;
                    $invalidFields[] = 'port';
                }
            } catch (Exception $e) {

                $invalid = true;
                $invalidFields[] = 'port';
            }

            if ($invalid === true) {

                //Fehlerhafte Eingaben
                ?>
                <div>
                    <div class="message_error">
                        <div class="message_icon"></div>
                        <div class="message"><?php echo PCC::l()->val('message.remote.create.error'); ?></div>
                        <ul>
                <?php foreach ($invalidFields as $field) { ?>
                                <li><?php echo PCC::l()->val('message.remote.create.inputError.' . $field); ?></li>
                            <?php } ?> 
                        </ul>
                    </div>
                </div>
                <div>
                    <form id="rpi_create">
                        <div class="description">
                            <div class="descriptionText">
                <?php echo PCC::l()->val('index.remote.create.desc'); ?>
                            </div>
                        </div>
                        <div class="form_content">
                            <div class="form_title"><?php echo PCC::l()->val('index.remote.create.name'); ?> <span class="red">*</span></div>
                            <div class="form_element"><input type="text" name="name" class="defaultInputField<?php echo (in_array('name', $invalidFields) ? ' invalidFormField' : ''); ?>" value="<?php echo String::encodeHTML($name); ?>" maxlength="60" /></div>
                            <div class="form_description"><?php echo PCC::l()->val('index.remote.create.name.desc'); ?></div>
                        </div>
                        <div class="form_content">
                            <div class="form_title"><?php echo PCC::l()->val('index.remote.create.ip'); ?> <span class="red">*</span></div>
                            <div class="form_element">
                                <input type="text" name="ip0" class="defaultInputField<?php echo (in_array('ip0', $invalidFields) ? ' invalidFormField' : ''); ?>" value="<?php echo String::encodeHTML($ip0); ?>" maxlength="3" style="width: 3em;" />
                                .
                                <input type="text" name="ip1" class="defaultInputField<?php echo (in_array('ip1', $invalidFields) ? ' invalidFormField' : ''); ?>" value="<?php echo String::encodeHTML($ip1); ?>" maxlength="3" style="width: 3em;" />
                                .
                                <input type="text" name="ip2" class="defaultInputField<?php echo (in_array('ip2', $invalidFields) ? ' invalidFormField' : ''); ?>" value="<?php echo String::encodeHTML($ip2); ?>" maxlength="3" style="width: 3em;" />
                                .
                                <input type="text" name="ip3" class="defaultInputField<?php echo (in_array('ip3', $invalidFields) ? ' invalidFormField' : ''); ?>" value="<?php echo String::encodeHTML($ip3); ?>" maxlength="3" style="width: 3em;" />
                            </div>
                            <div class="form_description"><?php echo PCC::l()->val('index.remote.create.ip.desc'); ?></div>
                        </div>
                        <div class="form_content">
                            <div class="form_title"><?php echo PCC::l()->val('index.remote.create.port'); ?> <span class="red">*</span></div>
                            <div class="form_element"><input type="text" name="port" class="defaultInputField<?php echo (in_array('port', $invalidFields) ? ' invalidFormField' : ''); ?>" value="<?php echo String::encodeHTML($port); ?>" maxlength="5" /></div>
                            <div class="form_description"><?php echo PCC::l()->val('index.remote.create.port.desc'); ?></div>
                        </div>
                    </form>
                </div>
                <?php
            } else {

                //Speichern
                if (RemoteRPi::editRPi($request->getParam('rid', Request::GET, Request::INT), $name, $ip0 . '.' . $ip1 . '.' . $ip2 . '.' . $ip3, $port)) {

                    //Erfolgreich
                    ?>
                    <div>
                        <div class="message_successfully">
                            <div class="message_icon"></div>
                            <div class="message"><?php echo PCC::l()->val('message.remote.edit.success'); ?></div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $('#remote_editor').dialog({
                            buttons: {
                                '<?php echo utf8_decode(PCC::l()->val('index.user.buttons.back', null, false, false)); ?>': function() {
                                    $.get('index.php?ajax=rrpiacp', function(data, textStatus, jqXHR) {
                                        $('#remote_editor').html(data);
                                    });

                                    $('#remote_editor').dialog({
                                        buttons: {
                                            '<?php echo PCC::l()->val('index.remote.create.button'); ?>': function() {

                                                $.get('index.php?ajax=rrpiacp&req=create', function(data, textStatus, jqXHR) {
                                                    $('#remote_editor').html(data);
                                                });
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    </script>
                    <?php
                } else {

                    //Fehler
                    ?>
                    <div>
                        <div class="message_error">
                            <div class="message_icon"></div>
                            <div class="message"><?php echo PCC::l()->val('message.remote.edit.error'); ?></div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $('#remote_editor').dialog({
                            buttons: {
                                '<?php echo utf8_decode(PCC::l()->val('index.user.buttons.back', null, false, false)); ?>': function() {
                                    $.get('index.php?ajax=rrpiacp', function(data, textStatus, jqXHR) {
                                        $('#remote_editor').html(data);
                                    });

                                    $('#remote_editor').dialog({
                                        buttons: {
                                            '<?php echo PCC::l()->val('index.remote.create.button'); ?>': function() {

                                                $.get('index.php?ajax=rrpiacp&req=create', function(data, textStatus, jqXHR) {
                                                    $('#remote_editor').html(data);
                                                });
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    </script>
                    <?php
                }
            }
        }
    } elseif ($request->issetParam('req') && $request->getParam('req', Request::GET, Request::STRING) == 'delete' && $request->issetParam('rid', Request::GET)) {

        //RPi loeschen
        $id = $request->getParam('rid', Request::GET, Request::INT);
        if (RemoteRPi::deleteRPi($id)) {
            ?>
            <div>
                <div class="message_successfully">
                    <div class="message_icon"></div>
                    <div class="message"><?php echo PCC::l()->val('message.remote.del.success'); ?></div>
                </div>
            </div>
            <?php
        } else {
            ?>
            <div>
                <div class="message_error">
                    <div class="message_icon"></div>
                    <div class="message"><?php echo PCC::l()->val('message.remote.del.error'); ?></div>
                </div>
            </div>
            <?php
        }
        ?>
        <script type="text/javascript">
            $('#remote_editor').dialog({
                buttons: {
                    '<?php echo utf8_decode(PCC::l()->val('index.user.buttons.back', null, false, false)); ?>': function() {
                        $.get('index.php?ajax=rrpiacp', function(data, textStatus, jqXHR) {
                            $('#remote_editor').html(data);
                        });

                        $('#remote_editor').dialog({
                            buttons: {
                                '<?php echo PCC::l()->val('index.remote.create.button'); ?>': function() {

                                    $.get('index.php?ajax=rrpiacp&req=create', function(data, textStatus, jqXHR) {
                                        $('#remote_editor').html(data);
                                    });
                                }
                            }
                        });
                    }
                }
            });
        </script>
        <?php
    } else {

        //Remote RPi's AUflisten
        $list = RemoteRPi::listRemotRPis();
        ?>
        <div>
            <table class="content_table">
                <thead>
                    <tr>
                        <th style="width: 70%"><?php echo PCC::l()->val('index.remote.table.name'); ?></th>
                        <th style="width: 20%"><?php echo PCC::l()->val('index.remote.table.lasteConnect'); ?></th>
                        <th style="width: 10%"><?php echo PCC::l()->val('index.remote.table.options'); ?></th>
                    </tr>
                </thead>
                <tbody>
        <?php foreach ($list as $rpi) { ?>
                        <tr>
                            <td><?php echo String::encodeHTML($rpi->getName()); ?></td>
                            <td><?php echo String::encodeHTML($rpi->getLastConnection()->format('d.m.Y H:i:s')); ?></td>
                            <td>
                                <a href="#"  id="edit_rrpi_<?php echo $rpi->getId(); ?>" class="edit" title="<?php echo PCC::l()->val('index.tooltip.edit'); ?>"></a>
                                <a href="#" id="del_rrpi_<?php echo $rpi->getId(); ?>" class="delete" title="<?php echo PCC::l()->val('index.tooltip.delete'); ?>"></a>
                            </td>
                        </tr>
                    <script type="text/javascript">
                        $(function() {

                            $('.edit, .delete').tooltip({
                                track: false,
                                position: {
                                    my: "left+15 center",
                                    at: "right center"
                                }
                            });

                            $('#edit_rrpi_<?php echo $rpi->getId(); ?>').click(function() {

                                $.get('index.php?ajax=rrpiacp&req=edit&rid=<?php echo $rpi->getId(); ?>', function(data, textStatus, jqXHR) {
                                    $('#remote_editor').html(data);
                                });
                            });
                            $('#del_rrpi_<?php echo $rpi->getId(); ?>').click(function() {

                                $.get('index.php?ajax=rrpiacp&req=delete&rid=<?php echo $rpi->getId(); ?>', function(data, textStatus, jqXHR) {
                                    $('#remote_editor').html(data);
                                });
                            });
                        });
                    </script>
        <?php } ?>
                </tbody>
            </table>
        </div>
        <?php
    }
} else {
    //Keine Berechtigung
    ?>
    <div>
        <div class="message_error">
            <div class="message_icon"></div>
            <div class="message"><?php echo PCC::l()->val('message.accessdenied'); ?></div>
        </div>
    </div>
    <?php
}
?>

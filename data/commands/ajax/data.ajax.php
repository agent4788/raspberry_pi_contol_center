<?php

/**
 * liefert die Systemdaten des RPi als JSON zurueck
 * 
 * @author     Oliver Kleditzsch
 * @copyright  Copyright (c) 2013, Oliver Kleditzsch
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License
 * @since      1.0.0-0
 * @version    1.0.0-0
 */
if (PCC::getUser()->checkPremission('showData')) {

    $data = array();
    $rpi = new RPi();

    //Betriebssystem
    $data['osName'] = String::encodeHTML($rpi->getOsName());
    $data['kernel'] = String::encodeHTML($rpi->getKernelVersion());
    $data['firmwareShort'] = String::encodeHTML(String::subString($rpi->getFirmwareVersion(), 0, 45) . ' ...');
    $data['firmware'] = String::encodeHTML($rpi->getFirmwareVersion());
    $split = $rpi->getMemorySplit();
    $data['splitSystem'] = String::encodeHTML($split['system']);
    $data['splitVideo'] = String::encodeHTML($split['video']);
    $data['hostname'] = String::encodeHTML($rpi->getHostname());

    //Systemdaten
    $data['cpuType'] = String::encodeHTML($rpi->getCPUType());
    $data['cpuFeatures'] = String::encodeHTML($rpi->getCPUFeatures());
    $data['serial'] = String::encodeHTML($rpi->getRpiSerial());
    $rev = $rpi->getRpiRevision();
    if ($rev == 2) {
        $data['revision'] = 'Model B Revision 1.0';
    } elseif ($rev == 3) {
        $data['revision'] = 'Model B Revision 1.0 + Fuses mod and D14 removed';
    } elseif ($rev == 4) {
        $data['revision'] = 'Model B Revision 2.0 256MB (Sony)';
    } elseif ($rev == 5) {
        $data['revision'] = 'Model B Revision 2.0 256MB (Qisda)';
    } elseif ($rev == 6) {
        $data['revision'] = 'Model B Revision 2.0 256MB (Egoman)';
    } elseif ($rev == 7) {
        $data['revision'] = 'Model A Revision 2.0 256MB (Egoman)';
    } elseif ($rev == 8) {
        $data['revision'] = 'Model A Revision 2.0 256MB (Sony)';
    } elseif ($rev == 9) {
        $data['revision'] = 'Model A Revision 2.0 256MB (Qisda)';
    } elseif ($rev == 13) {
        $data['revision'] = 'Model B Revision 2.0 512MB (Egoman)';
    } elseif ($rev == 14) {
        $data['revision'] = 'Model B Revision 2.0 512MB (Sony)';
    } elseif ($rev == 15) {
        $data['revision'] = 'Model B Revision 2.0 512MB (Qisda)';
    } else {
        $data['revision'] = PCC::l()->get('global.unknown');
    }

    //Video Lizensen
    if ($rpi->isSetMPEGCode()) {
        $data['mpeg2'] = PCC::l()->val('index.data.video.box.activated');
    } else {
        $data['mpeg2'] = PCC::l()->val('index.data.video.box.deactivated');
    }
    if ($rpi->isSetVC1Code()) {
        $data['vc1'] = PCC::l()->val('index.data.video.box.activated');
    } else {
        $data['vc1'] = PCC::l()->val('index.data.video.box.deactivated');
    }

    //USB
    $usb = $rpi->getUsbDevices();
    $data['usb'] = '';
    foreach ($usb as $device) {
        $data['usb'] .= '<tr><td>' . String::encodeHTML($device) . '</td></tr>';
    }

    //PCC Daten
    $version = PCC::getVersionsData();
    $data['pccVersion'] = String::encodeHTML($version['versionString']);
    $data['installDate'] = String::encodeHTML($version['installDate']);
    $data['pccSize'] = FileUtil::formatBytesBinary(FileUtil::getDirSize(BASE_DIR));
    $data['phpVersion'] = PHP_VERSION;

//Zeitmessung
    $data['generationTime'] = PCC::l()->get('global.footline.generationTime', PHP_VERSION, getMicrotime());
} else {

    $data['error'] = PCC::l()->val('message.accessdenied');
}

//Daten senden
echo json_encode($data);
?>

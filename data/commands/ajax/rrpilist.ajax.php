<?php
/**
 * listet alle Eingetragenen Remote Server auf
 * 
 * @author     Oliver Kleditzsch
 * @copyright  Copyright (c) 2013, Oliver Kleditzsch
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License
 * @since      1.0.0-0
 * @version    1.0.0-0
 */
PCC::l()->loadModule('index');
if (PCC::getUser()->checkPremission('remoteRPi')) {
    $list = RemoteRPi::listRemotRPis();
    ?>
    <div class="content_box_collapsable ui-tabs ui-widget ui-widget-content ui-corner-all">
        <div class="content_box_collapsable_header ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
            <?php echo PCC::l()->val('index.tabs.remoteRPi'); ?>
        </div>
        <div class="content_box_collapsable_body">
            <table class="content_table">
                <thead>
                    <tr>
                        <th style="width: 60%"><?php echo PCC::l()->val('index.remote.table.name'); ?></th>
                        <th style="width: 20%"><?php echo PCC::l()->val('index.remote.table.lasteConnect'); ?></th>
                        <th style="width: 10%"><?php echo PCC::l()->val('index.remote.table.ip'); ?></th>
                        <th style="width: 10%"><?php echo PCC::l()->val('index.remote.table.port'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($list as $rpi) { ?>
                        <tr>
                            <td><a href="#" id="rrpi_<?php echo $rpi->getId(); ?>"><?php echo String::encodeHTML($rpi->getName()); ?></a></td>
                            <td><?php echo String::encodeHTML($rpi->getLastConnection()->format('d.m.Y H:i:s')); ?></td>
                            <td><?php echo String::encodeHTML($rpi->getAddress()); ?></td>
                            <td><?php echo String::encodeHTML($rpi->getPort()); ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php foreach ($list as $rpi) { ?>
        <div style="display: none;" id="rrpi_<?php echo $rpi->getId(); ?>_dialog" title="<?php echo String::encodeHTML($rpi->getName()) ?>">
            <div id="rrpi_<?php echo $rpi->getId(); ?>_tab">
                <ul>
                    <?php if (PCC::getUser()->checkPremission('showRemoteRPiState')) { ?><li><a href="index.php?ajax=remoterpistate&amp;rrpiid=<?php echo $rpi->getId(); ?>">Status</a></li><?php } ?>
                    <?php if (PCC::getUser()->checkPremission('showRemoteRPiData')) { ?><li><a href="index.php?ajax=remoterpidata&amp;rrpiid=<?php echo $rpi->getId(); ?>">Daten</a></li><?php } ?>
                </ul>
            </div>
        </div>
        <script type="text/javascript">
            $(function() {
                $('#rrpi_<?php echo $rpi->getId(); ?>').click(function() {

                    $('#rrpi_<?php echo $rpi->getId(); ?>_tab').tabs();
                    $('#rrpi_<?php echo $rpi->getId(); ?>_dialog').dialog({
                        modal: true,
                        resizable: false,
                        width: 750,
                        position: {my: "center top", at: "center bottom", of: $('#headline')}
                    });
                });
            });
        </script>
    <?php } ?>
<?php } else { ?>
    <div>
        <div class="message_error">
            <div class="message_icon"></div>
            <div class="message"><?php echo PCC::l()->val('message.accessdenied'); ?></div>
        </div>
    </div>
<?php } ?>


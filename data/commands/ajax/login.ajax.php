<?php

/**
 * meldet den User am PCC an
 * 
 * @author     Oliver Kleditzsch
 * @copyright  Copyright (c) 2013, Oliver Kleditzsch
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License
 * @since      1.0.0-0
 * @version    1.0.0-0
 */
$data = array();
if (Login::userLogin(
                PCC::getRequest()->getParam('user', Request::POST, Request::PLAIN), PCC::getRequest()->getParam('password', Request::POST, Request::PLAIN))
) {

    $data['success'] = true;
    $data['message'] = 'true';
} else {

    $data['success'] = false;
    $data['message'] = Login::getMessage()->getMessage();
}

//Daten senden
echo json_encode($data);
?>

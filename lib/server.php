<?php

/**
 * Initialisiert und Verwaltet den Remote Server
 * 
 * @author     Oliver Kleditzsch
 * @copyright  Copyright (c) 2013, Oliver Kleditzsch
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License
 * @since      1.0.0-0
 * @version    1.0.0-0
 */
//max_execution_time auser kraft setzen
set_time_limit(0);
//direkte ausgabe der Daten
ob_implicit_flush();

//Commandozeilen Tool initialisieren
$cli = new CommandLine();

//Sprache Initialisieren
PCC::l()->loadModule('index');

//Server Konfiguration
if (in_array('-c', $argv) || in_array('--config', $argv)) {

    //IP Adresse
    $n = 0;
    $valid = true;
    $valid_address = '';
    $address_not_change = false;
    while ($n < 5) {

        $address = $cli->input(PCC::l()->get('index.server.ipInput', PCC::getSettings()->getValue('serverAddress')));

        //Adresse nicht aendern
        if (String::length($address) == 0) {

            $address_not_change = true;
            $valid = true;
            break;
        }

        //Adresse pruefen
        $parts = explode('.', $address);
        for ($i = 0; $i < 3; $i++) {

            if (isset($parts[$i]) && (int) $parts[$i] >= 0 && (int) $parts[$i] <= 255) {

                continue;
            }

            $cli->writeLineColored(PCC::l()->val('index.server.ipInvalid'), 'red');
            $n++;
            $valid = false;
            break;
        }

        if ($valid === true) {

            $valid_address = $address;
            break;
        }
    }

    if ($valid === false) {

        $cli->writeLineColored(PCC::l()->val('index.server.ipArlredyInvalid'), 'red');
        exit(1);
    }

    //Port
    $n = 0;
    $valid = true;
    $valid_port = '';
    $port_not_change = false;
    while ($n < 5) {

        $port = $cli->input(PCC::l()->get('index.server.port', PCC::getSettings()->getValue('serverPort')));

        //Port nicht aendern
        if (String::length($port) == 0) {

            $port_not_change = true;
            $valid = true;
            break;
        }

        if (!preg_match('#^[0-9]{1,5}$#', $port) || (int) $port <= 0 || (int) $port >= 65000) {

            $cli->writeLineColored(PCC::l()->val('index.server.portInvalid'), 'red');
            $n++;
            $valid = false;
            continue;
        }

        if ($valid === true) {

            $valid_port = $port;
            break;
        }
    }

    if ($valid === false) {

        $cli->writeLineColored(PCC::l()->val('index.server.portAlredyInvalid'), 'red');
        exit(1);
    }

    //Daten Speichern
    if ($address_not_change === false) {

        PCC::getSettings()->updateSetting('serverAddress', $valid_address);
    }
    if ($port_not_change === false) {

        PCC::getSettings()->updateSetting('serverPort', $valid_port);
    }
    if (PCC::getSettings()->saveAndReload()) {

        $cli->writeLineColored(PCC::l()->val('index.server.saveSuccessfully'), 'green');
    } else {

        $cli->writeLineColored(PCC::l()->val('index.server.saveError'), 'red');
        exit(1);
    }

    //Fragen ob Server gestartet werden soll
    $i = 0;
    while (true) {

        $i++;
        $in = strtolower($cli->input(PCC::l()->val('index.server.startServer')));

        if (preg_match('#^(' . PCC::l()->val('index.server.yes') . ')|(' . PCC::l()->val('index.server.yesShort') . ')$#i', $in)) {

            break;
        } elseif (preg_match('#^(' . PCC::l()->val('index.server.no') . ')|(' . PCC::l()->val('index.server.noShort') . ')$#i', $in)) {

            exit(0);
        } elseif ($i < 5) {

            $cli->writeLineColored(PCC::l()->val('index.server.invalidInput'), 'red');
        } elseif ($i == 5) {

            $cli->writeLineColored(PCC::l()->val('index.server.invalidInput.exit'), 'red');
            exit(1);
        }
    }
} elseif (in_array('-u', $argv) || in_array('--update', $argv)) {

    //Update
    $cli->writeLine(PCC::l()->val('index.server.updateSearch'));

    //Suchen
    if (!Update::checkForUpdates()) {

        $cli->writeLineColored(PCC::l()->val('index.server.updateSearchError'), 'red');
        exit(1);
    }

    //Pruefen ob neuere Version verfuegbar
    if (Update::isUpdateAvailable()) {

        $cli->writeLineColored(PCC::l()->val('index.server.updateFound'), 'green');
    } else {

        $cli->writeLineColored(PCC::l()->val('index.server.notUpdateFound'), 'yellow');
        exit(0);
    }

    //Changelog Anzeigen
    if ($data = @file_get_contents(UPDATE_CHANGELOG)) {

        $cli->writeLine('***********************************CHANGELOG**********************************************');
        $cli->writeLine("\n" . $data);
        $cli->writeLine('***********************************CHANGELOG**********************************************');
    } else {

        $cli->writeLine(PCC::l()->val('index.update.changelogError'));
    }

    //nachfragen ob es installiert werden soll
    $i = 0;
    while (true) {

        $i++;
        $in = strtolower($cli->input(PCC::l()->val('index.server.installAlert')));

        if (preg_match('#^(' . PCC::l()->val('index.server.yes') . ')|(' . PCC::l()->val('index.server.yesShort') . ')$#i', $in)) {

            break;
        } elseif (preg_match('#^(' . PCC::l()->val('index.server.no') . ')|(' . PCC::l()->val('index.server.noShort') . ')$#i', $in)) {

            $cli->writeLineColored(PCC::l()->val('index.server.invokeLater'), 'yellow');
            exit(0);
        } elseif ($i < 5) {

            $cli->writeLineColored(PCC::l()->val('index.server.invalidInput'), 'red');
        } else {

            $cli->writeLineColored(PCC::l()->val('index.server.invalidInput.exit'), 'red');
            exit(1);
        }
    }

    //Update Installieren
    $code = Update::installUpdate();

    if ($code == 2000) {

        $cli->writeLineColored(PCC::l()->val('message.update.successfully'), 'green');
        exit(0);
    } else {

        if ($code == 130) {

            $cli->writeLineColored(PCC::l()->val('message.update.error.130'), 'yellow');
        } else {

            $cli->writeLineColored(PCC::l()->val('message.update.error.' . $code), 'red');
            $cli->writeLineColored(PCC::l()->val('message.update.backupInfo'), 'red');
        }
        exit(1);
    }
} elseif (in_array('-v', $argv) || in_array('--version', $argv)) {

    //Version Anzeigen
    $version = PCC::getVersionsData();
    $cli->writeLine($version['versionString']);
    exit(0);
} elseif (in_array('-s', $argv) || in_array('--stop', $argv)) {

    //Server Stoppen
    $sock = new Socket(PCC::getSettings()->getValue('serverAddress'), PCC::getSettings()->getValue('serverPort'));
    $sock->open();
    $sock->write(json_encode(array('command' => 'shut_down')));
    $cli->writeLineColored($sock->read(), 'yellow');
    $sock->close();
    exit(0);
}

//Debuk Modus
if (in_array('-d', $argv) || in_array('-debug', $argv)) {

    $cli->writeLineColored(PCC::l()->val('index.server.debugActive'), 'yellow');
    define('DEBUG', true);
} else {

    define('DEBUG', false);
}

//Server Beenden
//if (in_array('-sd', $argv) || in_array('-shut down', $argv)) {
//    
//    $xml = new SimpleXMLElement(file_get_contents(XML . 'settings.xml'));
//    foreach ($xml->setting as $setting) {
//
//        $attributes = $setting->attributes();
//        if($attributes->name == 'serverAddress') {
//            
//             $address = $attributes->value;
//        } elseif($attributes->name == 'serverPort') {
//            
//            $port = $attributes->value;
//        }
//    }
//    $client = new Socket($address, $port);
//    $client->open();
//    $client->write(json_encode(array('command' => 'SHUT DOWN')));
//    $answer = $client->read();
//    echo "$answer\n";
//    exit();
//}
//Server Starten
$socketServer = new SocketServer(PCC::getSettings()->getValue('serverAddress'), PCC::getSettings()->getValue('serverPort'));
$socketServer->startServer();

$cli->writeLineColored(PCC::l()->get('index.server.start', $socketServer->getAddress(), $socketServer->getPort()), 'green');
while (true) {

    $socketServerClient = $socketServer->accept();
    $data = $socketServerClient->read(4096);

    //Initialisierung
//    if (PCC::getSettings()->getValue('serverInitFinally') == false && preg_match("#^init\n\n#", $data) == true) {
//
//        $data = String::subString($data, 6);
//        $request = json_decode($data, true);
//
//        if (isset($request['request']) && $request['request'] == 'getKeys') {
//
//            $response = array(
//                'encryptionKey' => PCC::getSettings()->getValue('encryptionKey'),
//                'publicKey' => PCC::getSettings()->getValue('publicKey'),
//                'privateKey' => PCC::getSettings()->getValue('privateKey')
//            );
//
//            $socketServerClient->write(json_encode($response));
//            $socketServerClient->close();
//            continue;
//        }
//    }
    //Anfragen behandeln
    $request = json_decode($data, true);
    if (isset($request['request']) && $request['request'] == 'getState') {

        $data = array();
        $rpi = new RPi();

        //Systemzeit
        $data['uptime'] = $rpi->getUptime();
        $data['lastBootTime'] = $rpi->getLastBootTime();

        //CPU
        $sysload = $rpi->getSysLoad();
        $data['sysload_0'] = $sysload[0];
        $data['sysload_1'] = $sysload[1];
        $data['sysload_2'] = $sysload[2];

        $data['cpuClock'] = $rpi->getCpuClock();

        $data['coreTemp'] = $rpi->getCoreTemprature();

        //Speicher
        $memory = $rpi->getMemoryUsage();
        $data['memoryPercent'] = $memory['percent'];
        $data['memoryTotal'] = $memory['total'];
        $data['memoryFree'] = $memory['free'];
        $data['memoryUsed'] = $memory['used'];

        //Swap
        $swap = $rpi->getSwapUsage();
        $data['swapPercent'] = $swap['percent'];
        $data['swapTotal'] = $swap['total'];
        $data['swapFree'] = $swap['free'];
        $data['swapUsed'] = $swap['used'];

        //Systemspeicher
        $sysMemory = $rpi->getMemoryInfo();
        $data['sysMemory'] = array();
        foreach ($sysMemory as $index => $mem) {

            if ($index != (count($sysMemory) - 1)) {

                $data['sysMemory'][$index] = array(
                    'device' => $mem['device'],
                    'mountpoint' => $mem['mountpoint'],
                    'percent' => $mem['percent'],
                    'total' => $mem['total'],
                    'used' => $mem['used'],
                    'free' => $mem['free']
                );
            } else {

                $data['sysMemoryTotal'] = array(
                    'percent' => $mem['percent'],
                    'total' => $mem['total'],
                    'used' => $mem['used'],
                    'free' => $mem['free']
                );
            }
        }

        //Netzwerk
        $network = $rpi->getNetworkDevices();
        $data['network'] = array();
        foreach ($network as $index => $net) {

            $data['network'][$index] = array(
                'name' => $net['name'],
                'in' => $net['in'],
                'out' => $net['out'],
                'errors' => $net['errors'],
                'drops' => $net['drops']
            );
        }

        //Debug ausgabe
        if (DEBUG) {

            echo "state request: \n\n";
            var_dump($data);
        }

        //Daten senden
        $socketServerClient->write(json_encode($data));
        $socketServerClient->close();
        continue;
    } elseif (isset($request['request']) && $request['request'] == 'getData') {

        $data = array();
        $rpi = new RPi();

        //Betriebssystem
        $data['osName'] = $rpi->getOsName();
        $data['kernel'] = $rpi->getKernelVersion();
        $data['firmware'] = $rpi->getFirmwareVersion();
        $split = $rpi->getMemorySplit();
        $data['splitSystem'] = $split['system'];
        $data['splitVideo'] = $split['video'];
        $data['hostname'] = $rpi->getHostname();

        //Systemdaten
        $data['cpuType'] = $rpi->getCPUType();
        $data['cpuFeatures'] = $rpi->getCPUFeatures();
        $data['serial'] = $rpi->getRpiSerial();
        $data['revision'] = $rpi->getRpiRevision();

        //Video Lizensen
        $data['mpeg2'] = $rpi->isSetMPEGCode();
        $data['vc1'] = $rpi->isSetVC1Code();

        //USB
        $usb = $rpi->getUsbDevices();
        $data['usb'] = array();
        foreach ($usb as $device) {
            $data['usb'][] = $device;
        }

        //PCC Daten
        $version = PCC::getVersionsData();
        $data['pccVersion'] = $version['versionString'];
        $data['installDate'] = $version['installDate'];
        $data['pccSize'] = FileUtil::getDirSize(BASE_DIR);
        $data['phpVersion'] = PHP_VERSION;

        //Debug ausgabe
        if (DEBUG) {

            echo "data request: \n\n";
            var_dump($data);
            var_dump($data);
        }

        //Daten senden
        $socketServerClient->write(json_encode($data));
        $socketServerClient->close();
        continue;
    } elseif (isset($request['command']) && $request['command'] == 'shut_down') {

        $socketServerClient->write(PCC::l()->val('index.server.shutDown'));
        $socketServerClient->close();
        shutdown();
        break;
    }
}

function shutdown() {

    global $socketServer, $cli;
    $socketServer->stopServer();
    $cli->writeLineColored(PCC::l()->val('index.server.shutDown'), 'green');
    exit(0);
}

?>

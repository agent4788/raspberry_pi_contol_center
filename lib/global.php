<?php

/**
 * Globale Grundeinstellungen
 * 
 * @author     Oliver Kleditzsch
 * @copyright  Copyright (c) 2013, Oliver Kleditzsch
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License
 * @since      1.0.0-0
 * @version    1.0.0-0
 */
//Globale Konstanten
define('BASE_DIR', dirname(__DIR__) . '/');
define('CLASS_PATH', BASE_DIR . 'lib/');
define('XML', BASE_DIR . 'data/xml/');
define('TEMPLATES', BASE_DIR . 'data/templates/');
define('SESSION', BASE_DIR . 'data/xml/sessions/');
define('BACKUP_DIR', BASE_DIR . 'backup/');
define('UPDATE_CHANGELOG', 'http://update.rpi-controlcenter.de/changelog.txt');
define('UPDATE_INFO', 'http://update.rpi-controlcenter.de/update_info.xml');
define('UPDATE_URL', 'http://update.rpi-controlcenter.de/');
define('UPDATE_DIR', BASE_DIR . 'update/');
define('UPDATE_DATA_DIR', UPDATE_DIR . 'data/');
define('UPDATE_LOCALE_FILE', UPDATE_DIR . 'update.zip');
define('UPDATE_CONF_XML', UPDATE_DATA_DIR . 'update.xml');

//Datum/Zeit
define('TIME_NOW', time());
define('MICROTIME_NOW', strtok(microtime(), ' ') + strtok(''));
date_default_timezone_set('Europe/Berlin');

//Einstellungen
define('DATETIME_TIMEZONE', 'Europe/Berlin');

//Klassen Einbinden
require_once(CLASS_PATH . 'classes.php');

//gibt die Zeit in Milisekunden seit dem Start des Scriptes an
function getMicrotime() {

    return round(strtok(microtime(), ' ') + strtok('') - MICROTIME_NOW, 4);
}

?>

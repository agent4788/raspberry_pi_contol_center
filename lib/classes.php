<?php 
/**
 * Diese Datei wird automatisch erstellt und sollte nicht von hand verändert werden
 * Erstellt am: Thu, 05 Sep 2013 09:49:08 +0200
 * @author Oliver Kleditzsch
 * @copyright Copyright (c) 2013, Oliver Kleditzsch
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

class DateTimeExtendet extends DateTime {
    const NOW = 'now';
    const TOMORROW = 'tomorrow';
    const TODAY = 'today';
    const YESTERDAY = 'yesterday';
    protected $useDaylightSavingTime = false;
    public static function previousDay(DateTimeZone $timezone = null) {

        return new DateTime(self::YESTERDAY, $timezone);
    }
    public static function today(DateTimeZone $timezone = null) {

        return new DateTime(self::TODAY, $timezone);
    }
    public static function now(DateTimeZone $timezone = null) {

        return new DateTime(self::NOW, $timezone);
    }
    public static function nextDay(DateTimeZone $timezone = null) {

        return new DateTime(self::TOMORROW, $timezone);
    }
    public function __construct($time = null, $useDaylightSavingTime = false, \DateTimeZone $timezone = null) {
        $this->useDaylightSavingTime($useDaylightSavingTime);
        if ($timezone === null && defined('DATETIME_TIMEZONE')) {

            $timezone = new DateTimeZone(DATETIME_TIMEZONE);
        } elseif ($timezone === null) {

            $timezone = new DateTimeZone('Europe/London');
        }

        parent::__construct($time, $timezone);
    }
    public function format($format) {

        if ($this->useDaylightSavingTime === true && parent::format('I') == 1) {

            $oneHour = new DateInterval('PT1H');
            $this->add($oneHour);
            $formated = parent::format($format);
            $this->sub($oneHour);
            return $formated;
        }

        return parent::format($format);
    }
    public function useDaylightSavingTime($enabled) {

        $this->useDaylightSavingTime = ($enabled == true ? true : false);
    }
    public function getYear() {

        return $this->format('Y');
    }
    public function getMonth() {

        return $this->format('m');
    }
    public function getDay() {

        return $this->format('d');
    }
    public function getHour() {

        return $this->format('H');
    }
    public function getMinute() {

        return $this->format('i');
    }
    public function getSecond() {

        return $this->format('s');
    }
    public function getDayOfWeek() {

        $day = $this->format('w');
        $days = array(1 => '0', 2 => '1', 3 => '2', 4 => '3', 5 => '4', 6 => '5', 0 => '6');
        return $days[$day];
    }
    public function getDayOfYear() {

        return $this->format('z');
    }
    public function getWeekOfYear() {

        return $this->format('W');
    }
    public function getQuarterOfYear() {

        $month = intval($this->format('n'));
        if ($month <= 3) {

            return 1;
        } elseif ($month >= 4 && $month <= 6) {

            return 2;
        } elseif ($month >= 7 && $month <= 9) {

            return 3;
        }

        return 4;
    }
    public function isLeapYear() {

        $year = $this->format('Y');
        if (($year % 400) == 0 || (($year % 4) == 0 && ($year % 100) != 0)) {

            return true;
        }
        return false;
    }
    public function isPast() {

        $date = new DateTime(self::NOW, $this->getTimezone());
        if ($this < $date) {

            return true;
        }

        return false;
    }
    public function isFuture() {

        $date = new DateTime(self::NOW, $this->getTimezone());
        if ($this > $date) {

            return true;
        }

        return false;
    }

}
class RemoteRPi {

    protected static $remoteRPis = array();
    protected $thisRPi = array();
    public function __construct($id) {
        if (count(self::$remoteRPis) == 0) {

            self::readXML();
        }

        if (isset(self::$remoteRPis[$id])) {

            $this->thisRPi = self::$remoteRPis[$id];
        } else {

            throw new Exception('', 10);
        }
    }
    public static function listRemotRPis() {
        if (count(self::$remoteRPis) == 0) {

            self::readXML();
        }
        $return = array();
        foreach (self::$remoteRPis as $id => $rpi) {

            $return[] = new RemoteRPi($id);
        }

        return $return;
    }
    protected static function readXML() {

        $xml = new SimpleXMLElement(file_get_contents(XML . 'remoterpi.xml'));

        foreach ($xml->rpi as $rpi) {

            self::$remoteRPis[(int) $rpi->id] = array(
                'id' => (int) $rpi->id,
                'name' => (string) $rpi->name,
                'address' => (string) $rpi->address,
                'port' => (string) $rpi->port,
                'lastConnection' => (string) $rpi->lastConnection
            );
        }
    }
    public function getId() {

        return $this->thisRPi['id'];
    }
    public function getName() {

        return $this->thisRPi['name'];
    }
    public function getAddress() {

        return $this->thisRPi['address'];
    }
    public function getPort() {

        return $this->thisRPi['port'];
    }
    public function getLastConnection() {
        
        if ($this->thisRPi['lastConnection'] == '') {

            return new DateTimeExtendet('1970-01-01');
        }
        return DateTimeExtendet::createFromFormat('Y-m-d H-i-s', $this->thisRPi['lastConnection']);
    }
    
    public function updateLastConnection() {
        
        $xml = new SimpleXMLElement(file_get_contents(XML . 'remoterpi.xml'));

        foreach ($xml->rpi as $rpi) {
            
            if($rpi->id == $this->getId()) {
                
                $date = new DateTimeExtendet(DateTimeExtendet::NOW);
                $rpi->lastConnection = $date->format('Y-m-d H-i-s');
            }
        }
        
        $xml->saveXML(XML . 'remoterpi.xml');
    }
    public static function createRPi($name, $ip, $port) {
        $xml = new SimpleXMLElement(file_get_contents(XML . 'remoterpi.xml'));
        $nextId = (int) $xml->nextAutoIncrement;
        $xml->nextAutoIncrement = $nextId + 1;
        
        $rpi = $xml->addChild('rpi');
        $rpi->addChild('id', $nextId);
        $rpi->addChild('name', $name);
        $rpi->addChild('address', $ip);
        $rpi->addChild('port', $port);
        $rpi->addChild('lastConnection');
        
        if ($xml->saveXML(XML . 'remoterpi.xml')) {

            return $nextId;
        }
        return null;
    }
    
    
    public static function editRPi($id, $name = null, $ip = null, $port = null) {
        $xml = new SimpleXMLElement(file_get_contents(XML . 'remoterpi.xml'));
        
        foreach ($xml->rpi as $rpi) {
            
            if($rpi->id == $id) {
                if($name !== null) {
                    
                    $rpi->name = $name;
                }
                if($ip !== null) {
                    
                    $rpi->address = $ip;
                }
                if($port !== null) {
                    
                    $rpi->port = $port;
                }
            }
        }
        
        if ($xml->saveXML(XML . 'remoterpi.xml')) {

            return true;
        }
        return false;
    }
    public static function deleteRPi($id) {
        $xml = new SimpleXMLElement(file_get_contents(XML . 'remoterpi.xml'));
        
        for ($i = 0; $i < count($xml->rpi); $i++) {
            if ($xml->rpi[$i]->id == $id) {
                unset($xml->rpi[$i]);
                if ($xml->saveXML(XML . 'remoterpi.xml')) {

                    return true;
                }
                return false;
            }
        }
        return false;
    }

}
class IOException extends \Exception {
	protected $context = null;
	public function __construct($message, $code, Object $context = null) {
		
		$this->message = $message;
		$this->code    = $code;
		$this->context = $context;
		
	}
	public function getContext() {
		
		return $this->context;
		
	}
	
}
class AccessDeniedException extends Exception {
    protected $premission = '';
    function __construct($message, $code, $premission = '') {

        $this->message = $message;
        $this->code = $code;
        $this->premission = $premission;
    }
    public function getPremission() {

        return $this->premission;
    }

}
class RPi {
    protected $cpuInfo = '';
    protected $cache = array();
    public function __construct() {

        $this->cpuInfo = file_get_contents('/proc/cpuinfo');
    }
    public function getHostname() {

        if (!isset($this->cache['hostmane'])) {

            $this->cache['hostmane'] = trim(file_get_contents('/proc/sys/kernel/hostname'));
        }
        return $this->cache['hostmane'];
    }
    public function getHostAddr() {

        if (!isset($this->cache['hostaddr'])) {

            if (!isset($_SERVER['SERVER_ADDR'])) {

                $this->cache['hostaddr'] = 'unknown';
            }

            if (!$ip = $_SERVER['SERVER_ADDR']) {

                $this->cache['hostaddr'] = gethostbyname($this->getHostname());
            }
        }
        return $this->cache['hostaddr'];
    }
    public function getKernelVersion() {

        if (!isset($this->cache['kernel'])) {

            $version = file_get_contents('/proc/version');
            preg_match('#version\s+([^\s]*)#', $version, $match);
            $this->cache['kernel'] = $match[1];
        }
        return $this->cache['kernel'];
    }
    
    public function getFirmwareVersion() {
        
         exec('uname -a', $firmware);
         return $firmware[0];
    }
    public function getCoreTemprature() {

        if (!isset($this->cache['coreTemperature'])) {

            $file = @file_get_contents('/sys/class/thermal/thermal_zone0/temp');
            if ($file != false) {

                $this->cache['coreTemperature'] = round(substr(trim($file), 0, 2) . '.' . substr(trim($file), 2), 2);
            } else {

                $this->cache['coreTemperature'] = 0.0;
            }
        }

        return $this->cache['coreTemperature'];
    }
    public function getCpuClock() {

        if (!isset($this->cache['cpuClock'])) {

            if (file_exists('/sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq')) {

                $file = trim(file_get_contents('/sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq'));
                $this->cache['cpuClock'] = floatval(substr($file, 0, -3) . '.' . substr($file, -3));
            } else {

                $file = file_get_contents('/proc/cpuinfo');
                preg_match('#BogoMIPS\s*:\s*(\d*\.\d*)#i', $file, $match);
                $this->cache['cpuClock'] = floatval($match[1]);
            }
        }
        return $this->cache['cpuClock'];
    }
    public function getCPUType() {

        if (!isset($this->cache['cpuType'])) {

            preg_match('#Hardware\s*:\s*([^\s]+)#i', $this->cpuInfo, $match);
            if (isset($match[1])) {

                $this->cache['cpuType'] = $match[1];
            } else {

                $this->cache['cpuType'] = null;
            }
        }
        return $this->cache['cpuType'];
    }
    public function getCPUFeatures() {

        if (!isset($this->cache['cpuFeatures'])) {

            preg_match('#Features\s*:\s*(.+)#i', $this->cpuInfo, $match);

            if (isset($match[1])) {

                $this->cache['cpuFeatures'] = $match[1];
            } else {
                $this->cache['cpuFeatures'] = null;
            }
        }
        return $this->cache['cpuFeatures'];
    }
    public function getRpiSerial() {

        if (!isset($this->cache['rpiSerial'])) {

            preg_match('#Serial\s*:\s*([\da-f]+)#i', $this->cpuInfo, $match);

            if (isset($match[1])) {

                $this->cache['rpiSerial'] = $match[1];
            } else {
                $this->cache['rpiSerial'] = null;
            }
        }

        return $this->cache['rpiSerial'];
    }
    public function getRpiRevision() {
        if (!isset($this->cache['rpiRevision'])) {

            preg_match('#\nRevision\s*:\s*([\da-f]+)#i', $this->cpuInfo, $match);

            if (isset($match[1])) {

                $this->cache['rpiRevision'] = hexdec($match[1]);
            } else {
                $this->cache['rpiRevision'] = null;
            }
        }
        return $this->cache['rpiRevision'];
    }
    public function getMemorySplit() {

        if (!isset($this->cache['memorySplit'])) {
            $rev = $this->getRpiRevision();
            $gpuMem = array();
            $config = @file_get_contents('/boot/config.txt');
            preg_match('#\ngpu_mem=([0-9]+)#i', $config, $match);
            if (isset($match[1])) {
                $gpuMem[0] = $match[1];
            }
            $match = null;
            preg_match('#\ngpu_mem_256=([0-9]+)#i', $config, $match);
            if (isset($match[1])) {
                $gpuMem[1] = $match[1];
            }
            $match = null;
            preg_match('#\ngpu_mem_512=([0-9]+)#i', $config, $match);
            if (isset($match[1])) {
                $gpuMem[2] = $match[1];
            }
            $match = null;
            if ($rev == 13 || $rev == 14 || $rev == 15) {

                if (isset($gpuMem[2])) {

                    $total = $gpuMem[2];
                } elseif (isset($gpuMem[0])) {

                    $total = $gpuMem[0];
                } else {

                    $mem = $this->getMemoryUsage();
                    $total = round($mem ['total'] / 1024 / 1024, 0);
                    if ($total <= 256) {

                        $this->cache['memorySplit'] = array('system' => '256 MiB', 'video' => '256 MiB');
                    } elseif ($total > 256 && $total <= 384) {

                        $this->cache['memorySplit'] = array('system' => '384 MiB', 'video' => '128 MiB');
                    } elseif ($total > 384 && $total <= 448) {

                        $this->cache['memorySplit'] = array('system' => '448 MiB', 'video' => '64 MiB');
                    } elseif ($total > 448 && $total <= 480) {

                        $this->cache['memorySplit'] = array('system' => '480 MiB', 'video' => '32 MiB');
                    } else {

                        $this->cache['memorySplit'] = array('system' => '496 MiB', 'video' => '16 MiB');
                    }
                }

                if ($total == 16) {

                    $this->cache['memorySplit'] = array('system' => '496 MiB', 'video' => '16 MiB');
                } elseif ($total == 32) {

                    $this->cache['memorySplit'] = array('system' => '480 MiB', 'video' => '32 MiB');
                } elseif ($total == 64) {

                    $this->cache['memorySplit'] = array('system' => '448 MiB', 'video' => '64 MiB');
                } elseif ($total == 128) {

                    $this->cache['memorySplit'] = array('system' => '384 MiB', 'video' => '128 MiB');
                } else {

                    $this->cache['memorySplit'] = array('system' => '256 MiB', 'video' => '256 MiB');
                }
            } else {
                if (isset($gpuMem[1])) {

                    $total = $gpuMem[1];
                } elseif (isset($gpuMem[0])) {

                    $total = $gpuMem[0];
                } else {

                    $mem = $this->getMemoryUsage();
                    $total = round($mem ['total'] / 1024 / 1024, 0);

                    if ($total <= 128) {

                        return array('system' => '128 MiB', 'video' => '128 MiB');
                    } elseif ($total > 128 && $total <= 192) {

                        $this->cache['memorySplit'] = array('system' => '192 MiB', 'video' => '64 MiB');
                    } elseif ($total > 192 && $total <= 224) {

                        $this->cache['memorySplit'] = array('system' => '224 MiB', 'video' => '32 MiB');
                    } else {

                        $this->cache['memorySplit'] = array('system' => '240 MiB', 'video' => '16 MiB');
                    }
                }

                if ($total == 16) {

                    $this->cache['memorySplit'] = array('system' => '240 MiB', 'video' => '16 MiB');
                } elseif ($total == 32) {

                    $this->cache['memorySplit'] = array('system' => '224 MiB', 'video' => '32 MiB');
                } elseif ($total == 64) {

                    $this->cache['memorySplit'] = array('system' => '192 MiB', 'video' => '64 MiB');
                } else {

                    $this->cache['memorySplit'] = array('system' => '128 MiB', 'video' => '128 MiB');
                }
            }
        }
        return $this->cache['memorySplit'];
    }
    public function getSysLoad() {

        if (!isset($this->cache['sysLoad'])) {

            $this->cache['sysLoad'] = sys_getloadavg();
        }
        return $this->cache['sysLoad'];
    }
    public function getMemoryUsage() {

        if (!isset($this->cache['memoryUsage'])) {

            exec('free -bo', $data);
            list($type, $total, $used, $free, $shared, $buffers, $cached) = preg_split('#\s+#', $data[1]);
            $usage = round(($used - $buffers - $cached) / $total * 100);

            $this->cache['memoryUsage'] = array('percent' => $usage, 'total' => (int) $total, 'free' => ($free + $buffers + $cached), 'used' => ($used - $buffers - $cached));
        }
        return $this->cache['memoryUsage'];
    }
    public function getSwapUsage() {

        if (!isset($this->cache['swapUsage'])) {

            exec('free -bo', $data);
            list($type, $total, $used, $free) = preg_split('#\s+#', $data[2]);
            $usage = 0;
            if ($total > 0 || $used > 0) {

                $usage = round($used / $total * 100);
            }

            $this->cache['swapUsage'] = array('percent' => $usage, 'total' => (int) $total, 'free' => (int) $free, 'used' => (int) $used);
        }
        return $this->cache['swapUsage'];
    }
    public function getUptime() {

        if (!isset($this->cache['uptime'])) {

            $file = file_get_contents('/proc/uptime');
            $time = preg_split('#\s+#', trim($file));

            $this->cache['uptime'] = round($time[0], 0);
        }
        return $this->cache['uptime'];
    }
    public function getLastBootTime() {

        if (!isset($this->cache['lastBoot'])) {

            $this->cache['lastBoot'] = TIME_NOW - $this->getUptime();
        }
        return $this->cache['lastBoot'];
    }
    public function getMemoryInfo() {

        if (!isset($this->cache['memoryInfo'])) {

            exec('df -lT | grep -vE "tmpfs|rootfs|Filesystem|Dateisystem"', $data);

            $devices = array();
            $totalSize = 0;
            $usedSize = 0;
            foreach ($data as $row) {

                list($device, $type, $blocks, $use, $available, $used, $mountpoint) = preg_split('#[^\dA-Z/]+#i', $row);

                $totalSize += $blocks * 1024;
                $usedSize += $use * 1024;

                $devices[] = array(
                    'device' => $device,
                    'type' => $type,
                    'total' => $blocks * 1024,
                    'used' => $use * 1024,
                    'free' => $available * 1024,
                    'percent' => round(($use * 100 / $blocks), 0),
                    'mountpoint' => $mountpoint
                );
            }

            $devices[] = array('total' => $totalSize, 'used' => $usedSize, 'free' => $totalSize - $usedSize, 'percent' => round(($usedSize * 100 / $totalSize), 0));
            $this->cache['memoryInfo'] = $devices;
        }
        return $this->cache['memoryInfo'];
    }
    public function getNetworkDevices() {

        if (!isset($this->cache['networkDevices'])) {

            $dev = file_get_contents('/proc/net/dev');
            $devices = preg_split('#\n#', $dev, -1, PREG_SPLIT_NO_EMPTY);
            unset($devices[0], $devices[1]);

            $netDev = array();
            foreach ($devices as $device) {

                list($dev_name, $stats) = preg_split('#:#', $device);
                $stats = preg_split('#\s+#', trim($stats));
                $netDev[] = array(
                    'name' => trim($dev_name),
                    'in' => $stats[0],
                    'out' => $stats[8],
                    'errors' => $stats[2] + $stats[10],
                    'drops' => $stats[3] + $stats[11]
                );
            }

            $this->cache['networkDevices'] = $netDev;
        }
        return $this->cache['networkDevices'];
    }
    public function getUsbDevices() {

        if (!isset($this->cache['usbDevices'])) {

            exec('lsusb', $data);
            $devices = array();

            foreach ($data as $row) {

                preg_match('#[0-9a-f]{4}:[0-9a-f]{4}\s+(.+)#i', $row, $match);
                $devices[] = trim($match[1]);
            }

            $this->cache['usbDevices'] = $devices;
        }
        return $this->cache['usbDevices'];
    }
    public function getOsName() {

        if (!isset($this->cache['osName'])) {

            $files = FileUtil::listDirFiles('/etc');

            foreach ($files as $file) {

                if (isset($file['name']) && preg_match('#[a-z]+-release#', $file['name'])) {

                    $content = file_get_contents('/etc/' . $file['name']);
                    preg_match('#pretty_name="(.+)"#i', $content, $match);
                    if (isset($match[1])) {

                        $this->cache['osName'] = $match[1];
                    }
                }
            }
            if (!isset($this->cache['osName'])) {

                $this->cache['osName'] = 'unkonwn';
            }
        }
        return $this->cache['osName'];
    }
    public function isSetVC1Code() {

        if (!isset($this->cache['vc1'])) {

            $config = @file_get_contents('/boot/config.txt');
            if (preg_match('#decode_WVC1=0x[0-9a-z]+#i', $config)) {

                $this->cache['vc1'] = true;
            } else {

                $this->cache['vc1'] = false;
            }
        }
        return $this->cache['vc1'];
    }
    public function isSetMPEGCode() {

        if (!isset($this->cache['mpeg2'])) {

            $config = @file_get_contents('/boot/config.txt');
            if (preg_match('#decode_MPG2=0x[0-9a-z]+#i', $config)) {

                $this->cache['mpeg2'] = true;
            } else {

                $this->cache['mpeg2'] = false;
            }
        }
        return $this->cache['mpeg2'];
    }

}
class Session {
    protected $sessionVars = array();
    protected $sid = '';
    protected $newSession = true;
    public function __construct() {
        if (isset($_COOKIE['pccsession'])) {

            $this->sid = $_COOKIE['pccsession'];
            
        } else {

            $this->sid = String::randomStr(64);
            
        }
        if(file_exists(SESSION . $this->sid .'.session.dat')) {
            
            $this->sessionVars = unserialize(file_get_contents(SESSION . $this->sid .'.session.dat'));
            
        }
        setcookie('pccsession', $this->sid, TIME_NOW + 3600);
        $dir = opendir(SESSION);
        while($file = readdir($dir)) {
            
            if(preg_match('#^(\.|\.\.)$#', $file)) {
                
                continue;
            }
            
            $now = new DateTime('now');
            $now->sub(new DateInterval('PT15M'));
            $time = new DateTime();
            $time->setTimestamp(@filemtime(SESSION . $file));
            if($time < $now) {
                
                @unlink(SESSION . $file);
                
            }
            
        }
        
    }
    public function getSessionId() {

        return $this->sid;
    }
    public function regenerateSessionId() {
        
        $oldSid    = $this->sid;
        $this->sid = String::randomStr(64);
        setcookie('pccsession', $this->sid, TIME_NOW + 3600);
        
        @rename(SESSION . $oldSid .'.session.dat', SESSION . $this->sid .'.session.dat');
        
        return true;
    }
    public function get($var) {

        if (isset($this->sessionVars['vars'][$var])) {

            return $this->sessionVars['vars'][$var];
        }

        return null;
    }
    public function set($var, $value) {

        $this->sessionVars['vars'][$var] = $value;
    }
    public function delete($var) {

        if (isset($this->sessionVars['vars'][$var])) {

            $this->sessionVars['vars'][$var] = null;
            unset($this->sessionVars['vars'][$var]);
        }
    }
    public function issetVar($name) {

        if (isset($this->sessionVars['vars'][$name])) {

            return true;
        }

        return false;
    }
    public function setMessage(Message $message) {

        $this->sessionVars['message'] = $message;
    }
    public function getMessage() {

        if (isset($this->sessionVars['message'])) {

            return $this->sessionVars['message'];
        }

        return null;
    }
    public function deleteMessage() {

        unset($this->sessionVars['message']);
    }
    public function finish() {

        $data = serialize($this->sessionVars);
        @file_put_contents(SESSION . $this->sid .'.session.dat', $data);
        
    }

}
class PccDirectory extends FileSystemObject {
    public function exists() {

        if (@is_dir($this->object)) {
            return true;
        }
        return false;
    }
    public function getSize($recursive = true) {

        return FileUtil::getDirectorySize($this->object);
    }
    public function create($chmod = 0777) {
        if ($this->exists()) {

            return true;
        }
        if (!FileUtil::createDirectory($this->object, $chmod)) {

            return false;
        }

        return true;
    }
    public function getLastAccessTime() {

        return @fileatime($this->object);
    }
    public function getLastEditTime() {

        return @filemtime($this->object);
    }
    public function copy($destination, $chmod = 0777) {

        return FileUtil::copyDirectory($this->object, $destination, $chmod);
    }
    public function move($destination, $chmod = 0777) {

        return FileUtil::moveDirectory($this->object, $destination, $chmod);
    }
    public function rename($newName) {

        if (FileUtil::renameDirectory($this->object, $newName)) {

            $this->object = FileUtil::addTrailingSlash(dirname($this->object)) . $newName;
        }

        return false;
    }
    public function delete($deleteNoEmpty = true) {

        return FileUtil::deleteDirectory($this->object, $deleteNoEmpty);
    }

}
class SocketServer {
	protected $address = '127.0.0.1';
	protected $port = 9273;
	protected $socket = null;
	public function __construct($address = null, $port = null) {
		set_time_limit(0);
		ob_implicit_flush();
		if($address !== null) {
			
			$this->setAddress($address);
			
		}
		if($port !== null) {
			
			$this->setPort($port);
			
		}
		
	}
	public function setAddress($address) {
		$this->address = $address;
		
	}
	public function getAddress() {
		
		return $this->address;
		
	}
	public function setPort($port) {
		
		$this->port = intval($port);
		
	}
	public function getPort() {
		
		return $this->port;
		
	}
	public function startServer() {
		if(($this->socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false) {
			
			throw new \Exception('"'. socket_last_error() .': '. socket_strerror(socket_last_error()) .'"', 1000);
			
		}
		if(socket_bind($this->socket, $this->address, $this->port) === false) {
				
			throw new \Exception('"'. socket_last_error() .': '. socket_strerror(socket_last_error()) .'"', 1001);
				
		}
		if(socket_listen($this->socket, 10) === false) {
		
			throw new \Exception('"'. socket_last_error() .': '. socket_strerror(socket_last_error()) .'"', 1002);
		
		}
		
	}
	public function accept() {
		if(($incommingSocket = socket_accept($this->socket)) === false) {
		
			throw new \Exception('"'. socket_last_error() .': '. socket_strerror(socket_last_error()) .'"', 1002);
		
		}
		
		return new SocketServerClient($incommingSocket);
		
	}
	public function stopServer() {
		
		socket_close($this->socket);
		
	}
	
}
class Socket {
    protected $host = '127.0.0.1';
    protected $port = 9273;
    protected $timeout = 10;
    protected $errorNumber = 0;
    protected $errorString = '';
    protected $socket = null;
    public function __construct($host = null, $port = null, $timeout = null) {

        if ($host !== null) {

            $this->setHost($host);
        }

        if ($port !== null) {

            $this->setPort($port);
        }

        if ($timeout !== null) {

            $this->setTimeout($timeout);
        }
    }
    public function setHost($host) {

        $this->host = $host;
    }
    public function getHost() {

        return $this->host;
    }
    public function setPort($port) {

        $this->port = $port;
    }
    public function getPort() {

        return $this->port;
    }
    public function setTimeout($timeout) {

        $this->timeout = $timeout;
    }
    public function getTimeout() {

        return $this->timeout;
    }
    public function getErrorNumber() {

        return $this->errorNumber;
    }
    public function getErrorString() {

        return $this->errorString;
    }
    public function open() {

        $this->socket = socket_create(AF_INET, SOCK_STREAM, 0);
        $connected = @socket_connect($this->socket, $this->host, $this->port);

        if ($this->socket === false || $connected == false) {

            throw new \Exception('Verbindung zu "' . $this->host . ':' . $this->port . '" fehlgeschalgen', 1500);
        }
    }
    public function readBytes($length = 1024) {

        socket_recv($this->socket, $buff, $length, MSG_DONTWAIT);
        return $buff;
    }
    public function read($length = 1024) {

        return socket_read($this->socket, 8192, PHP_BINARY_READ);
    }
    public function readLine() {

        return socket_read($this->socket, 8192, PHP_NORMAL_READ);
    }
    public function write($str) {

        return socket_send($this->socket, $str, strlen($str), MSG_EOR);
    }
    public function writeLn($str) {

        return socket_send($this->socket, $str . "\n", strlen($str), MSG_EOR);
    }
    public function close() {

        return socket_close($this->socket);
    }

}
class SocketServerClient {
	protected $socket = null;
	public function __construct($socket) {
		
		$this->socket = $socket;
		
	}
	public function read($length) {
		
		return socket_read($this->socket, $length, PHP_BINARY_READ);
		
	}
	public function readLine($length) {
	
		return socket_read($this->socket, $length, PHP_NORMAL_READ);
	
	}
	public function write($buffer) {
		
		return socket_send($this->socket, $buffer, strlen($buffer), MSG_EOR);
		
	}
	public function writeLn($buffer) {
	
		return socket_send($this->socket, $buffer ."\n", strlen($buffer), MSG_EOR);
	
	}
	public function flush() {
	
		@fflush($this->socket);
	
	}
	public function close() {
		
		socket_close($this->socket);
		
	}
	
}
class FileSystemObject {
    protected $object = '';
    public function __construct($object = null) {

        if ($object !== null) {

            $this->object = $object;
        }
    }
    public function exists() {

        if (@file_exists($this->object)) {

            return true;
        }

        return false;
    }
    public function getSize($recursive = true) {

        return @filesize($this->object);
    }
    public function isFile() {

        if (@is_file($this->object)) {

            return true;
        }

        return false;
    }
    public function createFileObject() {

        return new File($this->getObject());
    }
    public function isDrirectory() {

        if (@is_dir($this->object)) {

            return true;
        }

        return false;
    }
    public function createDrirectoryObject() {

        return new PccDirectory($this->getObject());
    }
    public function isReadable() {

        if (@is_readable($this->object)) {

            return true;
        }

        return false;
    }
    public function isWriteable() {

        if (@is_writeable($this->object)) {

            return true;
        }

        return false;
    }
    public function isExecuteable() {

        if (@is_executable($this->object)) {

            return true;
        }

        return false;
    }
    public function isLink() {

        if (@is_link($this->object)) {

            return true;
        }

        return false;
    }
    public function setChmod($mod = 0777, $recursive = true) {

        if (@chmod($this->object, $mod)) {

            clearstatcache();
            return true;
        }

        return false;
    }
    public function getChmod() {

        $premissions = @fileperms($this->object);
        if ($premissions !== false) {

            $premissions = decoct($premissions);
            $matcher = new StringMatcher($premissions);
            $premissions = $matcher->replace('#\d{1,2}(\d{4})#', '$1');
            return $premissions;
        }

        return null;
    }
    public function setObject($object) {

        $this->object = $object;
    }
    public function getObject() {

        return $this->object;
    }
    public function __toString() {

        return $this->getObject();
    }

}
class File extends FileSystemObject {
    protected static $finfo = null;
    public function getFileName() {

        return basename($this->object);
    }
    public function getPath() {

        return dirname($this->object);
    }
    public function create($chmod = 0777, $createPath = true, $overwrite = false) {

        if ($this->exists() && $overwrite == false) {

            return false;
        }
        $path = $this->getPath();
        if (!@is_dir($path) && $createPath == true) {

            if (!FileUtil::createDirectory($path, true, $chmod)) {

                return false;
            }
        }
        if ($this->exists() && $overwrite == true) {

            if (!FileUtil::deleteFile($this->object)) {

                return false;
            }
        }
        if (!FileUtil::createFile($this->object, $chmod, false)) {

            return false;
        }

        return true;
    }
    public function getLastAccessTime() {

        return @fileatime($this->object);
    }
    public function getLastEditTime() {

        return @filemtime($this->object);
    }
    public function copy($destination, $chmod = 0777, $overwrite = false) {

        return FileUtil::copyFile($this->object, $destination, $chmod, $overwrite);
    }
    public function move($destination, $chmod = 0777) {

        return FileUtil::moveFile($this->object, $destination, $chmod);
    }
    public function rename($newName) {

        if (FileUtil::renameFile($this->object, $newName)) {
            $this->object = FileUtil::addTrailingSlash($this->getPath()) . $newName;
            return true;
        }

        return false;
    }
    public function getMimeType() {
        if (class_exists('\finfo', false)) {

            if (self::$finfo === null) {

                self::$finfo = new \Finfo(FILEINFO_MIME_TYPE);
            }


            if (self::$finfo === false) {

                return null;
            }

            $rest = @self::$finfo->file($this->getElement());
            if ($rest === false) {

                return null;
            }

            return $rest;
        }
        $rest = @mime_content_type($this->getElement());
        if ($rest === false) {

            return null;
        }

        return $rest;
    }
    public function delete() {

        return FileUtil::deleteFile($this->object);
    }

}
class RemoteRPiSocket extends Socket {
    public function __construct(RemoteRPi $rrpi) {
        
        $this->setHost($rrpi->getAddress());
        $this->setPort($rrpi->getPort());
        
    }
    public function getState() {
        $this->write(json_encode(array('request' => 'getState')));
        $data = $this->read(4096);
        $response = json_decode($data, true);
        return $response;
    }
    public function getData() {
        $this->write(json_encode(array('request' => 'getData')));
        $data = $this->read(4096);
        $response = json_decode($data, true);
        return $response;
    }
    
}
abstract class Update {
    public static function checkForUpdates() {
        
        if(!($data = @file_get_contents(UPDATE_INFO))) {
            $version = new SimpleXMLElement(file_get_contents(XML . 'version.xml'));
            $date = new DateTime('now');
            $version->lastCheck = $date->format('Y-m-d H:i');
            $attr = $version->lastCheck->attributes();
            $attr->successfully = 'false';
            $version->saveXML(XML . 'version.xml');
                    
            return false;
        }
        
        $updateInfo = new SimpleXMLElement($data);
        $version = new SimpleXMLElement(file_get_contents(XML . 'version.xml'));

        $newVersion = $updateInfo->version;
        $thisVersion = $version->version;

        if (version_compare($newVersion, $thisVersion, '>')) {

            $version->nextVersion = $updateInfo->version;
            $version->nextVersionString = $updateInfo->versionString;
            $version->nextVersionDate = $updateInfo->versionDate;
            $version->nextVersionFile = $updateInfo->file;
            $date = new DateTime('now');
            $version->lastCheck = $date->format('Y-m-d H:i');
            $attr = $version->lastCheck->attributes();
            $attr->successfully = 'true';
            
            if($version->saveXML(XML . 'version.xml')) {
                
                return true;
            }
            return false;
        }
        
        return true;
    }
    public static function checkForUpdatesAutomatic() {

        if (PCC::getSettings()->getValue('searchUpdates') === true) {

            $updateIntervall = PCC::getSettings()->getValue('searchIntervall');
            $version = new SimpleXMLElement(file_get_contents(XML . 'version.xml'));

            $dateNow = new DateTime('now');
            $dateNextCheck = DateTime::createFromFormat('Y-m-d H:i', $version->lastCheck);
            $dateNextCheck->add(new DateInterval('P' . $updateIntervall . 'D'));

            $attr = $version->lastCheck->attributes();
            $dateNextTry = DateTime::createFromFormat('Y-m-d H:i', $version->lastCheck);
            $dateNextTry->add(new DateInterval('PT4H'));
            
            if (($dateNextCheck <= $dateNow) || ($attr->successfully == 'false' && $dateNextTry <= $dateNow)) {

                return Update::checkForUpdates();
            }
            return true;
        }
    }
    public static function isUpdateAvailable() {

        $version = new SimpleXMLElement(file_get_contents(XML . 'version.xml'));
        if (strlen($version->nextVersion) > 0) {

            return true;
        }
        return false;
    }
    public static function getNextVersion() {

        $version = new SimpleXMLElement(file_get_contents(XML . 'version.xml'));
        if (strlen($version->nextVersionString) > 0) {

            return $version->nextVersionString;
        }
        return null;
    }
    public static function makeBackup() {
        
        $version = new SimpleXMLElement(file_get_contents(XML . 'version.xml'));
        $date = new DateTime('now');
        
        $file = new File(BACKUP_DIR .'pcc_'. $version->version .'_'. $date->format('Y_m_d') .'.zip');
        if($file->exists()) {
            
            $file->delete();
        }
        if(!$file->create(0777)) {
            return 10;
        }
        $zip = new ZipArchive();
        if(!$zip->open($file)) {
            return 20;
        }
        
        if(!self::addDir($zip, BASE_DIR)) {
            return 30;
        }
        
        $zip->close();
        return 1000;
    }
    protected static function addDir(ZipArchive &$zip, $path, $innerArchivPath = '', $ignoreHiddenFiles = false) {
        if (!@is_dir($path)) {

            return false;
        }
        $dir = opendir($path);
        $path = FileUtil::addTrailigSlash($path);
        while ($file = readdir($dir)) {
            $fileSystemElement = $path . $file;
            if ($innerArchivPath != '') {

                $innerArchiveElement = FileUtil::addTrailigSlash($innerArchivPath) . $file;
            } else {

                $innerArchiveElement = $file;
            }
            if (preg_match('#^(\.|\.\.)$#', $file)) {

                continue;
            }
            if ($ignoreHiddenFiles == true && preg_match('#^\.#', $file)) {

                continue;
            }
            if (@is_file($fileSystemElement)) {
                
                $zip->addFile($fileSystemElement, $innerArchiveElement);
                continue;
            }
            if (@is_dir($fileSystemElement) && $innerArchiveElement != 'backup') {
                
                $zip->addEmptyDir($innerArchiveElement);
                self::addDir($zip, $fileSystemElement, $innerArchiveElement, $ignoreHiddenFiles);
                continue;
            }
        }
        return true;
    }
    public static function installUpdate() {

        $versionXML = new SimpleXMLElement(file_get_contents(XML . 'version.xml'));
        if (strlen($versionXML->nextVersion) > 0) {
            $code = self::makeBackup();
            if($code != 1000) {
                return $code;
            }
            if ($sock = @fopen(UPDATE_URL . $versionXML->nextVersionFile, 'rb')) {
                $updateDir = new PccDirectory(UPDATE_DIR);
                if(!$updateDir->create(0777)) {
                    return 40;
                }
                $localFile = new File(UPDATE_LOCALE_FILE);
                if(!$localFile->create(0777)) {
                    return 50;
                }
                $localFileHandle = fopen($localFile, 'wb');
                $i = 0;
                while (!feof($sock)) {

                    $data = fread($sock, 4096);
                    fwrite($localFileHandle, $data);
                    $i++;
                }
                @fclose($sock);
                @fclose($localFileHandle);
            } else {
                return 60;
            }
            if (file_exists(UPDATE_LOCALE_FILE)) {
                $zip = new ZipArchive();
                if ($zip->open(UPDATE_LOCALE_FILE)) {

                    if(!$zip->extractTo(UPDATE_DATA_DIR)) {
                        return 70;
                    }
                    $zip->close();
                } else {
                    return 80;
                }
            } else {
                return 90;
            }
            $updateXML = new SimpleXMLElement(file_get_contents(UPDATE_CONF_XML));
            $thisVersion = $versionXML->version;
            $updates = array();
            $start = false;
            foreach ($updateXML->versions->version as $updateVersion) {

                $attr = $updateVersion->attributes();
                if ((string) $attr['from'] == $thisVersion || $start === true) {

                    $start = true;
                    $updates[(int) $attr['updateOrder']] = array(
                        'updateOrder' => (int) $attr['updateOrder'],
                        'id' => (string) $attr['id'],
                        'from' => (string) $attr['from'],
                        'to' => (string) $updateVersion
                    );
                }
            }
            $lastUpdate = null;
            foreach ($updates as $update) {
                foreach ($updateXML->updates->update as $updateXMLEntry) {
                    if ($update['id'] == (string) $updateXMLEntry->id) {
                        $lastUpdate = $updateXMLEntry;
                        $dir = (string) $updateXMLEntry->dir;
                        foreach ($updateXMLEntry->file as $file) {
                            $attr = $file->attributes();
                            $src = UPDATE_DATA_DIR . $dir . '/' . (string) $attr['src'];
                            $dest = BASE_DIR . (string) $attr['dest'];
                            $destDir = new PccDirectory(dirname($dest));
                            if (!$destDir->exists()) {

                                if (!$destDir->create(0777)) {
                                    return 100;
                                }
                            }
                            $srcFile = new File($src);
                            if ($srcFile->exists()) {

                                if (!$srcFile->copy($dest, 0777, true)) {
                                    return 110;
                                }
                            }
                        }
                        if (file_exists(UPDATE_DATA_DIR . $dir . '/' . (string) $updateXMLEntry->data)) {

                            require_once(UPDATE_DATA_DIR . $dir . '/' . (string) $updateXMLEntry->data);
                        }
                    }
                }
            }
            $zipFile = new File(UPDATE_LOCALE_FILE);
            $zipFile->delete();
            $updateDir = new PccDirectory(UPDATE_DATA_DIR);
            $updateDir->delete();
            $updateDir->delete();
            $versionXML->version = $lastUpdate->version;
            $versionXML->versionString = $lastUpdate->versionString;
            $versionXML->nextVersion = '';
            $versionXML->nextVersionString = '';
            $versionXML->nextVersionDate = '';
            $versionXML->nextVersionFile = '';

            if ($versionXML->saveXML(XML . 'version.xml')) {
                return 2000;
            }
            return 120;
        }
        return 130;
    }

}
class PCC {
    protected static $request = null;
    protected static $response = null;
    protected static $settings = null;
    protected static $language = null;
    protected static $session = null;
    protected static $user = null;
    protected static $visitor = null;
    protected static $versionXml = array();
    public function __construct() {
        if (function_exists('mb_internal_encoding')) {

            mb_internal_encoding('UTF-8');
            define('MULTIBYTE_STRING', true);
        } else {

            define('MULTIBYTE_STRING', false);
        }

        if (PHP_SAPI == 'cli') {

            $this->initSettings();
            $this->initLanguage();
            $this->initVersion();
        } else {

            $this->initRequest();
            $this->initSession();
            $this->initSettings();
            $this->initLanguage();
            $this->initVersion();
            $this->initUser();
        }
    }
    protected function initVersion() {
        $xml = new SimpleXMLElement(file_get_contents(BASE_DIR . 'data/xml/version.xml'));
        self::$versionXml = array(
            'version' => $xml->version,
            'versionString' => $xml->versionString,
            'installDate' => $xml->installDate,
            'nextVersion' => $xml->nextVersion,
            'lastCheck' => $xml->lastCheck
        );
        if($xml->installType == 'webapp' && PHP_SAPI == 'cli') {
            $cli = new CommandLine();
            $cli->writeLineColored(PCC::l()->val('message.pcc.webAppError'), 'red');
            exit(1);
        } elseif($xml->installType == 'server' && PHP_SAPI != 'cli') {
            echo PCC::l()->val('message.pcc.serverError');
            exit(1);
        }
    }
    protected function initRequest() {

        self::$request = new Request();
        self::$response = new Response();
    }
    protected function initSettings() {

        self::$settings = new Settings();
    }
    protected function initLanguage() {

        self::$language = new language(self::$settings->getValue('language'));
    }
    protected function initSession() {

        self::$session = new Session();
    }
    protected function initUser() {

        self::$user = UserFactory::getFactory()->getUserByAuthCode(self::$session->get('authCode'));
        self::$visitor = UserFactory::getFactory()->getVisitor();
    }
    public static function getVersionsData() {

        return self::$versionXml;
    }
    public static function getRequest() {

        return self::$request;
    }
    public static function getResponse() {

        return self::$response;
    }
    public static function getSettings() {

        return self::$settings;
    }
    public static function getLanguage() {

        return self::$language;
    }
    public static function l() {

        return self::getLanguage();
    }
    public static function getSession() {

        return self::$session;
    }
    public static function getUser() {

        if (self::$user !== null) {

            return self::$user;
        }
        return self::$visitor;
    }
    public static function checkPremission($premission) {

        if (self::getUser()->checkPremission($premission) === false) {

            throw new AccessDeniedException(self::l()->val('message.accessdenied'), 10202, $premission);
        }
    }
    public static function finally() {

        if(self::$settings !== null) {
            
            self::$settings->finish();
        }
        if (self::$session !== null) {
            
            self::$session->finish();
        }
    }

}
class Response {
    const STATE_OK = '200 OK';
    const STATE_MOVED_PERMANENT = '301 Moved Permanently';
    const STATE_UNAUTHORIZED = '401 Unauthorized';
    const STATE_FORBIDDEN = '403 Forbidden';
    const STATE_NOT_FOUND = '404 Not Found';
    protected $httpStatus = '200 OK';
    protected $httpContentType = 'text/html';
    protected $httpHeader = array();
    protected $httpBody = '';
    protected $i = 0;
    public function setStatus($status) {

        $this->httpStatus = $status;
    }
    public function setContentType($contentType) {

        $this->httpContentType = $contentType;
    }
    public function addHeader($name, $value, $http_response_code = null) {

        $this->httpHeader[$this->i]['name'] = $name;
        $this->httpHeader[$this->i]['value'] = $value;
        $this->httpHeader[$this->i]['http_response_code'] = $http_response_code;
        $this->i++;
    }
    public function getBody() {

        return $this->httpBody;
    }
    public function setBody($content) {

        $this->httpBody = $content;
    }
    public function write($content) {

        $this->httpBody .= $content;
    }
    public function writeln($content) {

        $this->httpBody .= $content . "\n";
    }
    public function flush() {

        header('HTTP/1.0 ' . $this->httpStatus);
        header('Content-type: ' . $this->httpContentType . '; charset=utf-8');
        foreach ($this->httpHeader as $head) {
            if ($head['http_response_code'] === null) {
                header($head['name'] . ': ' . $head['value']);
            } else {
                header($head['name'] . ': ' . $head['value'], $head['http_response_code']);
            }
        }
        echo utf8_encode($this->httpBody);

        $this->httpHeader = array();
        $this->httpContentType = 'text/html';
        $this->httpBody = '';
    }

}
class Request {
    const POST = 'post';
    const GET = 'get';
    const COOKIE = 'cookie';
    const SERVER = 'server';
    const FILE = 'file';
    const ENV = 'env';
    const REQUEST = 'request';
    const INT = 'int';
    const FLOAT = 'float';
    const BOOL = 'bool';
    const STRING = 'string';
    const MD5 = 'md5';
    const PLAIN = 'plain';
    protected $httpVars = array();
    public function __construct() {

        $superglobals = array('_GET', '_POST', '_SERVER', '_COOKIE', '_FILES', '_ENV', 'GLOBALS');
        foreach ($superglobals as $var) {
            if (isset($_REQUEST[$var]) || isset($_FILES[$var])) {
                throw new Exception('Hacking versuch entdeckt', 10000);
            }
        }
        if (version_compare(PHP_VERSION, '5.3', '<')) {
            if (get_magic_quotes_gpc()) {
                $_POST = ArrayUtil::stripSlashes($_POST);
                $_GET = ArrayUtil::stripSlashes($_GET);
                $_COOKIE = ArrayUtil::stripSlashes($_COOKIE);
                $_SERVER = ArrayUtil::stripSlashes($_SERVER);
            }
            set_magic_quotes_runtime(0);
            @ini_set('magic_quotes_gpc', 0);
            @ini_set('magic_quotes_runtime', 0);
        }
        if ((bool) @ini_get('register_globals')) {
            $superglobals = array('_GET', '_POST', '_SERVER', '_COOKIE', '_FILES', '_ENV');
            foreach ($superglobals as $superglobal) {
                $superglobal = array_keys($superglobal);
                foreach ($superglobal as $global) {
                    if (isset($GLOBALS[$global])) {
                        unset($GLOBALS[$global]);
                        unset($GLOBALS[$global]);
                    }
                }
            }
        }

        $this->httpVars['post'] = $_POST;
        $this->httpVars['get'] = $_GET;
        $this->httpVars['server'] = $_SERVER;
        $this->httpVars['cookie'] = $_COOKIE;
        $this->httpVars['file'] = $_FILES;
        $this->httpVars['env'] = $_ENV;
        $this->httpVars['request'] = $_REQUEST;
    }
    public function issetParam($name, $method = 'get') {

        return isset($this->httpVars[$method][$name]);
    }
    public function getParam($name, $method = 'get', $type = 'plain') {

        if ($this->issetParam($name, $method)) {
            switch ($type) {

                case self::BOOL:
                    $value = $this->httpVars[$method][$name];
                    if ((int) $value === 1 || (string) $value === 'true') {
                        return true;
                    } elseif ((int) $value === 0 || (string) $value === 'false') {
                        return false;
                    }
                    throw new Exception('Hacking versuch entdeckt', 10000);
                    break;
                case self::INT:
                    $value = (int) $this->httpVars[$method][$name];
                    if ((string) $value == $this->httpVars[$method][$name]) {
                        return $value;
                    }
                    throw new Exception('Hacking versuch entdeckt', 10000);
                    break;
                case self::FLOAT:
                    $value = (float) $this->httpVars[$method][$name];
                    if ((string) $value == $this->httpVars[$method][$name]) {
                        return $value;
                    }
                    throw new Exception('Hacking versuch entdeckt', 10000);
                    break;
                case self::MD5:
                    $value = $this->httpVars[$method][$name];
                    if (preg_match('#[0-9a-f]{32}#i', $value)) {
                        return $value;
                    }
                    throw new Exception('Hacking versuch entdeckt', 10000);
                    break;
                case self::STRING:
                    return $this->httpVars[$method][$name];
                    break;
                case self::PLAIN;
                default:
                    return $this->httpVars[$method][$name];
            }
        }
        return null;
    }
    public function getParamNames() {

        return array_keys($_REQUEST);
    }
    public function getHeader($name, $type = 'string') {

        if ($this->issetParam($name, self::SERVER)) {
            return $this->getParam($name, self::SERVER, $type);
        }
        return null;
    }

}
class language {
    protected $language = '';
    protected $modules = array();
    protected $languageItems = array();
    protected $currentLanguage = 0;
    protected $params = array();
    protected $tousandsSeparator = ',';
    protected $decimalSeparator = '.';
    protected $unknown = 'unknown';
    protected $cli = false;
    public function __construct($lang = 'de') {

        if(PHP_SAPI == 'cli') {
            
            $this->cli = true;
        }
        
        $this->language = $lang;
        $this->loadModule('global');
        $this->loadModule('message');

        $this->tousandsSeparator = $this->getPlain('global.tousandsSeparator');
        $this->decimalSeparator = $this->getPlain('global.decimalSeparator');
    }
    public function loadModule($name) {

        if (in_array($name, $this->modules)) {

            return true;
        }

        if (file_exists(BASE_DIR . 'data/lang/' . $this->language . '/' . $name . '.lang.php') && @require(BASE_DIR . 'data/lang/' . $this->language . '/' . $name . '.lang.php')) {

            $this->languageItems = array_merge($this->languageItems, $l);
            $this->modules[] = $name;
            return true;
        }
        return false;
    }
    public function getPlain($var, $default = null, $useTranslation = true, $encodeHTML = true) {
        
        if($this->cli === true) {
            
            $encodeHTML = false;
        }
        if($default !== null && $useTranslation !== null && $useTranslation == false) {
            
            return $default;
            
        }
        if (isset($this->languageItems[$var])) {
            
            if($encodeHTML == true) {
                
                return String::encodeHTML($this->languageItems[$var]);
            }
            return $this->languageItems[$var];
        }
        
        if($encodeHTML == true) {
            
            return String::encodeHTML($this->unknown);
        }
        return $this->unknown;
    }
    public function val($var, $default = null, $useTranslation = true, $encodeHTML = true) {
        
        return $this->getPlain($var, $default, $useTranslation, $encodeHTML);
        
    }
    public function get($var) {

        $this->params = array();
        $this->params = func_get_args();

        if($this->cli === false) {
            
            if (isset($this->languageItems[$var])) {

                return String::encodeHTML(preg_replace('#\{(\d+):(i|f|s)(?::([^:]+))?(?::(\d+))?\}#ie', "\$this->parse('$1', '$2', '$3', '$4')\n", $this->languageItems[$var]));
            }

            return String::encodeHTML($this->unknown);
        } else {
            
            if (isset($this->languageItems[$var])) {

                return preg_replace('#\{(\d+):(i|f|s)(?::([^:]+))?(?::(\d+))?\}#ie', "\$this->parse('$1', '$2', '$3', '$4')\n", $this->languageItems[$var]);
            }

            return $this->unknown;
        }
    }
    protected function parse($id, $type, $default = '', $decimals = '') {

        $return = '';
        $i = intval($id);
        $type = String::toLower(String::trim($type));
        switch ($type) {

            case 's':
                if (isset($this->params[$i])) {

                    $return = $this->params[$i];
                    break;
                }

                $return = $default;

                break;
            case 'i':
                if (isset($this->params[$i])) {

                    $return = number_format($this->params[$i], 0, $this->decimalSeparator, $this->tousandsSeparator);
                    break;
                } elseif ($default != '') {

                    $return = number_format($default, 0, $this->decimalSeparator, $this->tousandsSeparator);
                    break;
                }

                $return = 0;

                break;
            case 'f':
                if ($decimals == '') {

                    $decimals = 2;
                }

                if (isset($this->params[$i])) {

                    $return = number_format($this->params[$i], $decimals, $this->decimalSeparator, $this->tousandsSeparator);
                    break;
                } elseif ($default != '') {

                    $return = number_format($default, $decimals, $this->decimalSeparator, $this->tousandsSeparator);
                    break;
                }

                $return = 0.0;

                break;
            default:
                throw new \Exception('Incorrect data type in a language variable', '1400');
        }

        return $return;
    }
    public function getTousandsSeparator() {

        return $this->tousandsSeparator;
    }
    public function getDecimalSeparator() {

        return $this->decimalSeparator;
    }

}
class Message {

    const ERROR = 1;
    const WARNING = 2;
    const MESSAGE = 4;
    const SUCCESSFULLY = 8;
    protected $type = 0;
    protected $message = '';
    protected $subMessages = array();
    public function __construct($type = 0, $message = '', $submassages = array()) {

        $this->type = $type;
        $this->message = $message;
        $this->subMessages = $submassages;
    }
    public function getType() {

        return $this->type;
    }
    public function setType($type) {

        $this->type = $type;
    }
    public function getMessage() {

        return $this->message;
    }
    public function setMessage($message) {

        $this->message = $message;
    }
    public function getSubMessages() {

        return $this->subMessages;
    }
    public function addSubMessage($message) {

        $this->subMessages[] = $message;
    }
    public function removeSubMessages() {

        $this->subMessages = array();
    }

}
abstract class String {
    public static function randomStr($length = 10) {

        $set = array("a", "A", "b", "B", "c", "C", "d", "D", "e", "E", "f", "F", "g", "G", "h", "H", "i", "I", "j", "J",
            "k", "K", "l", "L", "m", "M", "n", "N", "o", "O", "p", "P", "q", "Q", "r", "R", "s", "S", "t", "T",
            "u", "U", "v", "V", "w", "W", "x", "X", "y", "Y", "z", "Z", "1", "2", "3", "4", "5", "6", "7", "8",
            "9");
        $str = '';

        for ($i = 1; $i <= $length; ++$i) {

            $ch = mt_rand(0, count($set) - 1);
            $str .= $set[$ch];
        }

        return $str;
    }
    public static function formatFloat($value, $decimals = 2) {

        return number_format($value, $decimals, PCC::getLanguage()->getDecimalSeparator(), PCC::getLanguage()->getTousandsSeparator());
    }
    public static function formatInteger($value) {

        return number_format($value, 0, PCC::getLanguage()->getDecimalSeparator(), PCC::getLanguage()->getTousandsSeparator());
    }
    public static function checkMD5($str) {

        if (preg_match('#^[0-9a-f]{32}$#i', $str)) {
            return true;
        }
        return false;
    }
    public static function randomId() {

        return md5(microtime() . uniqid(mt_rand(), true));
    }
    public static function convertToUnixLines($str) {

        return preg_replace("%(\r\n)|(\r)%", "\n", $str);
    }
    public static function trim($str) {

        return trim($str);
    }
    public static function encodeHTML($str) {

        return @htmlentities($str, ENT_QUOTES, 'UTF-8');
    }
    public static function decodeHTML($str) {

        $str = str_ireplace('&nbsp;', ' ', $str);
        return @html_entity_decode($str, '"', 'UTF-8');
    }
    public static function numberFormat($number) {

        if (is_int($number)) {
            return self::formatInteger($number);
        } elseif (is_float($number)) {
            return self::formatFloat($number);
        }

        if (floatval($numeric) - (float) intval($numeric)) {
            return self::formatFloat($number);
        } else {
            return self::formatInteger($number);
        }
    }
    public static function sort(&$str) {

        return asort($str, SORT_LOCALE_STRING);
    }
    public static function length($str) {

        if (MULTIBYTE_STRING) {
            return mb_strlen($str);
        }
        return strlen($str);
    }
    public static function indexOf($hayStack, $needle, $offset = 0) {

        if (MULTIBYTE_STRING) {
            return mb_strpos($hayStack, $needle, $offset);
        }
        return strpos($hayStack, $needle, $offset);
    }
    public static function indexOfIgnoreCase($hayStack, $needle, $offset = 0) {

        if (MULTIBYTE_STRING) {
            return mb_strpos(self::toLower($hayStack), self::toLower($needle), $offset);
        } else {
            return stripos($hayStack, $needle, $offset);
        }
    }
    public static function subString($str, $start, $length = null) {

        if (MULTIBYTE_STRING) {
            if ($length !== null) {
                return mb_substr($str, $start, $length);
            }
            return mb_substr($str, $start);
        }
        if ($length !== null) {
            return substr($str, $start, $length);
        }
        return substr($str, $start);
    }
    public static function toLower($str) {

        if (MULTIBYTE_STRING) {
            return mb_strtolower($str);
        }
        return strtolower($str);
    }
    public static function toUpper($str) {

        if (MULTIBYTE_STRING) {
            return mb_strtoupper($str);
        }
        return strtoupper($str);
    }
    public static function countSubString($hayStack, $needle) {

        if (MULTIBYTE_STRING) {
            return mb_substr_count($hayStack, $needle);
        }
        return substr_count($hayStack, $needle);
    }
    public static function firstCharToUpper($str) {

        if (MULTIBYTE_STRING) {
            return self::toUpper(self::subString($str, 0, 1)) . self::subString($str, 1);
        }
        return ucfirst($str);
    }
    public static function wordsToUpper($str) {

        if (MULTIBYTE_STRING) {
            return mb_convert_case($str, MB_CASE_TITLE);
        }
        return ucwords($str);
    }
    public static function replace($search, $replace, $subject, &$count = 0) {

        return str_replace($search, $replace, $subject, $count);
    }
    public static function replaceIgnoreCase($search, $replace, $subject, &$count = 0) {

        if (MULTIBYTE_STRING) {
            $startPos = self::indexOf(self::toLower($subject), self::toLower($search));
            if ($startPos === false)
                return $subject;
            else {
                $endPos = $startPos + self::length($search);
                $count++;
                return self::subString($subject, 0, $startPos) . $replace . self::replaceIgnoreCase($search, $replace, self::subString($subject, $endPos), $count);
            }
        }
        return str_ireplace($search, $replace, $subject, $count);
    }
    public static function convertEncoding($inCharset, $outCharset, $str) {

        if ($inCharset == 'ISO-8859-1' && $outCharset == 'UTF-8') {
            return utf8_encode($str);
        }
        if ($inCharset == 'UTF-8' && $outCharset == 'ISO-8859-1') {
            return utf8_decode($str);
        }

        if (function_exists('mb_convert_encoding')) {
            return mb_convert_encoding($str, $outCharset, $inCharset);
        }
        return $str;
    }
    public static function stripHTML($str) {

        return preg_replace('#</?[a-z]+[1-6]?(?:\s*[a-z]+\s*=\s*(?:"[^"\\\\]*(?:\\\\.[^"\\\\]*)*"|\'[^\'\\\\]*(?:\\\\.[^\'\\\\]*)*\'|[^\s>]))*\s*/#i', '', $str);
    }
    public static function checkLength($str, $min = 0, $max = 0) {

        if ($min > 0 && $max == 0) {
            $legth = self::length($str);
            if ($legth >= $min) {
                return true;
            } else {
                return false;
            }
        } elseif ($min == 0 && $max > 0) {
            $legth = self::length($str);
            if ($legth <= $max) {
                return true;
            } else {
                return false;
            }
        } elseif ($min > 0 && $max > 0) {
            $legth = self::length($str);
            if ($legth >= $min && $legth <= $max) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
    public static function varDump($array, $html = false) {

        ob_start();
        var_dump($array);
        $string = ob_get_contents();
        ob_end_clean();
        $string = StringUtil::convertEncoding('ISO-8859-1', 'UTF-8', $string);

        if ($html === true) {
            $string = StringUtil::encodeHTML($string);
            $string = StringUtil::replace(' ', '&nbsp;', $string);
            $string = nl2br($string);
        }
        return $string;
    }
    public static function encrypt($str, $key) {
        mt_srand($key);
        $out = array();
        for ($x = 0, $l = strlen($str); $x < $l; $x++) {
            $out[$x] = (ord($str[$x]) * 3) + mt_rand(350, 16000);
        }

        mt_srand();
        return implode('-', $out);
    }
    public static function decrypt($str, $key) {
        mt_srand($key);
        $blocks = explode('-', $str);
        $out = array();
        foreach ($blocks as $block) {
            $ord = (intval($block) - mt_rand(350, 16000)) / 3;
            $out[] = chr($ord);
        }

        mt_srand();
        return implode('', $out);
    }

}
abstract class TimeUtil {
    public static function formatTimefromSeconds($seconds, $short = false) {

        if ($seconds < 0) {
            $seconds = $seconds * -1;
        }

        $jears = 0;
        $month = 0;
        $weeks = 0;
        $days = 0;
        $hours = 0;
        $minutes = 0;
        $sec = 0;
        $jears_in_sec = 365 * 24 * 60 * 60;
        if ($seconds >= $jears_in_sec) {
            $jears = floor($seconds / $jears_in_sec);
            $seconds -= $jears * $jears_in_sec;
        }
        $month_in_sec = 30 * 24 * 60 * 60;
        if ($seconds >= $month_in_sec) {
            $month = floor($seconds / $month_in_sec);
            $seconds -= $month * $month_in_sec;
        }
        $weeks_in_sec = 7 * 24 * 60 * 60;
        if ($seconds >= $weeks_in_sec) {
            $weeks = floor($seconds / $weeks_in_sec);
            $seconds -= $weeks * $weeks_in_sec;
        }
        $days_in_sec = 24 * 60 * 60;
        if ($seconds >= $days_in_sec) {
            $days = floor($seconds / $days_in_sec);
            $seconds -= $days * $days_in_sec;
        }
        if ($seconds >= 3600) {
            $hours = floor($seconds / 3600);
            $seconds -= $hours * 3600;
        }
        if ($seconds >= 60) {
            $minutes = floor($seconds / 60);
            $seconds -= $minutes * 60;
        }
        $sec = $seconds;

        $first = false;
        $string = '';
        if ($jears > 0 || $first == true) {
            if ($short == true) {
                $string .= $jears . ' J, ';
            } else {
                $string .= $jears . ' ' . ($jears == 1 ? 'Jahr' : 'Jahre') . ', ';
            }
            $first = true;
        }
        if ($month > 0 || $first == true) {
            if ($short == true) {
                $string .= $month . ' M, ';
            } else {
                $string .= $month . ' ' . ($month == 1 ? 'Monat' : 'Monate') . ', ';
            }
            $first = true;
        }
        if ($weeks > 0 || $first == true) {
            if ($short == true) {
                $string .= $weeks . ' W, ';
            } else {
                $string .= $weeks . ' ' . ($weeks == 1 ? 'Woche' : 'Wochen') . ', ';
            }
            $first = true;
        }
        if ($days > 0 || $first == true) {
            if ($short == true) {
                $string .= $days . ' D, ';
            } else {
                $string .= $days . ' ' . ($days == 1 ? 'Tag' : 'Tage') . ', ';
            }
            $first = true;
        }
        if ($hours > 0 || $first == true) {
            if ($short == true) {
                $string .= $hours . ' S, ';
            } else {
                $string .= $hours . ' ' . ($hours == 1 ? 'Stunde' : 'Stunden') . ', ';
            }
            $first = true;
        }
        if ($minutes > 0 || $first == true) {
            if ($short == true) {
                $string .= $minutes . ' min, ';
            } else {
                $string .= $minutes . ' ' . ($minutes == 1 ? 'Minute' : 'Minuten') . ', ';
            }
            $first = true;
        }
        if ($short == true) {
            $string .= $sec . ' s';
        } else {
            $string .= $sec . ' ' . ($sec == 1 ? 'Sekunde' : 'Sekunden');
        }

        return trim($string);
    }
    public static function formatMilisecForDisplay($time) {

        if ($time < 1.0) {
            $time *= 1000;
            $time = String::formatFloat($time, 6) . ' ms';
        } else {
            $time = String::formatFloat($time, 6) . ' s';
        }

        return $time;
    }
    public static function timeDiff($mainTimestamp, $compareTimestamp) {

        return $compareTimestamp - $mainTimestamp;
    }

}
class CommandLine {
    protected static $in = null;
    protected $foregroundColors = array(
        'black' => '0;30',
        'dark_gray' => '1;30',
        'blue' => '0;34',
        'light_blue' => '1;34',
        'green' => '0;32',
        'light_green' => '1;32',
        'cyan' => '0;36',
        'light_cyan' => '1;36',
        'red' => '0;31',
        'light_red' => '1;31',
        'purple' => '0;35',
        'light_purple' => '1;35',
        'brown' => '0;33',
        'yellow' => '1;33',
        'light_gray' => '0;37',
        'white' => '1;37',
        'black_u' => '4;30',
        'red_u' => '4;31',
        'green_u' => '4;32',
        'yellow_u' => '4;33',
        'blue_u' => '4;34',
        'purple_u' => '4;35',
        'cyan_u' => '4;36',
        'white_u' => '4;37'
    );
    protected $backgroundColors = array(
        'black' => '40',
        'red' => '41',
        'green' => '42',
        'yellow' => '43',
        'blue' => '44',
        'magenta' => '45',
        'cyan' => '46',
        'light_gray' => '47'
    );
    public function write($str) {

        print($str);
    }
    public function writeLine($str) {

        print($str . "\n");
    }
    public function writeColored($str, $color, $backgroundColor = null) {

        $this->colorStart($color, $backgroundColor);
        $this->write($str);
        $this->reset();
    }
    public function writeLineColored($str, $color, $backgroundColor = null) {

        $this->colorStart($color, $backgroundColor);
        $this->writeLine($str);
        $this->reset();
    }
    public function input($message, &$handle = null) {
        if ($handle === null && self::$in == null) {

            self::$in = fopen('php://stdin', 'r');
            $in = self::$in;
        } elseif (self::$in !== null) {

            $in = self::$in;
        } else {

            $in = $handle;
        }
        print($message);
        $data = trim(fgets($in));

        return $data;
    }
    public function colorStart($color, $backgroundColor) {

        if (isset($this->foregroundColors[$color])) {

            print("\033[" . $this->foregroundColors[$color] . 'm');
        }
        if (isset($this->backgroundColors[$backgroundColor])) {

            print("\033[" . $this->foregroundColors[$backgroundColor] . 'm');
        }
    }
    public function colorNext($color) {

        print("\033[" . $this->foregroundColors[$color] . 'm');
    }
    public function reset() {

        print("\033[0m");
    }
    public function listColors() {

        return array_keys($this->foregroundColors);
    }
    public function listBackgroundColors() {

        return array_keys($this->backgroundColors);
    }

}
abstract class FileUtil {
    const FILE = 0;
    const DIR = 1;
    const UNCOMPRESSED = 0;
    const GZIP_COMPRESSED = 1;
    const BZIP_COMPRESSED = 2;
    public static function recognizeNewLineDelimiter($file, $type = self::UNCOMPRESSED) {
        if (!@file_exists($file) || !@is_readable($file)) {

            throw new IOException('the file "' . $file . '" does not exists or is not readable', '100.000.250');
        }

        if ($type == self::UNCOMPRESSED) {

            $handle = fopen($file, 'r');
            $chars = fread($handle, 2048);
        } elseif ($type == self::GZIP_COMPRESSED) {

            $handle = gzopen($file, 'r');
            $chars = gzread($handle, 2048);
        } elseif ($type == self::BZIP_COMPRESSED) {

            $handle = bzopen($file, 'r');
            $chars = bzread($handle, 2048);
        } else {

            throw new IOException('unknown file type of "' . $file . '"', '100.000.251');
        }

        $matcher = new StringMatcher($chars);
        $delimiter = '';
        if ($matcher->match("#\r\n#")) {

            $delimiter = "\r\n";
        } elseif ($matcher->match("#\n\r#")) {

            $delimiter = "\n\r";
        } elseif ($matcher->match("#\r#")) {

            $delimiter = "\r";
        } else {

            $delimiter = "\n";
        }

        if ($type == self::UNCOMPRESSED) {

            fclose($handle);
        } elseif ($type == self::GZIP_COMPRESSED) {

            gzclose($handle);
        } elseif ($type == self::BZIP_COMPRESSED) {

            bzclose($handle);
        }

        return $delimiter;
    }
    public static function formatBytesBinary($size, $short = true) {

        if ($short === true) {
            $norm = array('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB',
                'EiB', 'ZiB', 'YiB');
        } else {
            $norm = array('Byte',
                'Kibibyte',
                'Mebibyte',
                'Gibibyte',
                'Tebibyte',
                'Pebibyte',
                'Exbibyte',
                'Zebibyte',
                'Yobibyte');
        }

        $factor = 1024;

        $count = count($norm) - 1;

        $x = 0;
        while ($size >= $factor && $x < $count) {
            $size /= $factor;
            $x++;
        }

        $size = String::formatFloat($size, 2) . ' ' . $norm[$x];
        return $size;
    }
    public static function formatBytes($size, $short = true) {

        if ($short === true) {
            $norm = array('B', 'KB', 'MB', 'GB', 'TB', 'PB',
                'EB', 'ZB', 'YB');
        } else {
            $norm = array('Byte',
                'Kilobyte',
                'Megabyte',
                'Gigabyte',
                'Terrabyte',
                'Petabyte',
                'Exabyte',
                'Zettabyte',
                'Yottabyte');
        }

        $factor = 1000;

        $count = count($norm) - 1;

        $x = 0;
        while ($size >= $factor && $x < $count) {
            $size /= $factor;
            $x++;
        }

        $size = String::formatFloat($size, 2) . ' ' . $norm[$x];
        return $size;
    }
    public static function removeLeadingSlash($path) {

        if (substr($path, 0, 1) == '/') {
            return substr($path, 1);
        }
        return $path;
    }
    public static function addLeadingSlash($path) {

        if (substr($path, 0, 1) == '/') {
            return '/' . $path;
        }
        return $path;
    }
    public static function removeTrailingSlash($path) {

        if (substr($path, -1) == '/') {
            return substr($path, 0, -1);
        }
        return $path;
    }
    public static function addTrailigSlash($path) {

        if (substr($path, -1) != '/') {
            return $path . '/';
        }
        return $path;
    }
    public static function convertDirSeperatorsToUnix($path) {

        $path = str_replace('\\\\', '/', $path);
        $path = str_replace('\\', '/', $path);
        return $path;
    }
    public static function isURL($path) {

        if (preg_match('#^(https?|ftps?)://#', $path)) {

            return true;
        }

        return false;
    }
    public static function createFile($path, $chmod = 0777, $recursive = true) {

        if (file_exists($path)) {

            return false;
        }
        if (!@is_dir(dirname($path))) {

            if ($recursive == true && !self::createDir(dirname($path))) {

                return false;
            }
        }
        if (!file_put_contents($path, '') == false) {

            return false;
        }
        if (!@chmod($path, $chmod)) {

            clearstatcache();
            return false;
        }

        return true;
    }
    public static function copyFile($src, $dest, $chmod = 0777, $overwrite = false, $recursive = true) {
        if (!@is_file($src)) {

            return false;
        }
        if (@is_file($dest) && $overwrite === false) {

            return false;
        }
        $dir = dirname($dest);
        if (!@is_dir($dir) && $recursive == true) {

            if (!FileUtil::createDirectory($dir)) {

                return false;
            }
        }
        if (!@copy($src, $dest)) {

            return false;
        }
        if (@chmod($dest, $chmod)) {

            clearstatcache();
        } else {

            return false;
        }
        if (@is_file($dest)) {

            return true;
        }

        return false;
    }
    public static function moveFile($src, $dest, $chmod = 0777, $overwrite = false, $recursive = true) {
        if (!self::copyFile($src, $dest, $chmod, $overwrite, $recursive)) {

            return false;
        }
        if (self::deleteFile($src)) {

            return true;
        }

        return false;
    }
    public static function deleteFile($file) {
        if (!@is_file($file)) {

            return false;
        }
        @chmod($file, 0777);
        if (!@unlink($file)) {

            return false;
        }

        return true;
    }
    public static function renameFile($file, $newName) {
        if (!@is_file($file)) {

            return false;
        }
        $parent = self::addTrailingSlash(dirname($file));
        if (!@rename($file, $parent . $newName)) {

            return false;
        }

        return true;
    }
    public static function createDirectory($path, $recursive = true, $chmod = 0777) {
        if (file_exists($path)) {

            return true;
        }
        $parent = dirname($path);
        if ($parent != $path) {
            $parent = self::addTrailigSlash($parent);
            if (!@file_exists($parent) && $recursive == true) {

                if (!self::createDirectory($parent, $chmod)) {

                    return false;
                }
            } elseif (!@file_exists($parent) && $recursive == false) {

                return false;
            }
            $old = @umask(0);
            if (!@mkdir($path, $chmod)) {

                return false;
            }

            @umask($old);
            if (!@chmod($path, $chmod)) {

                return false;
            }

            return true;
        }

        return false;
    }
    public static function copyDirectory($src, $dest, $chmod = 0777, $recursive = true, $overwrite = false, $bringTogether = false) {
        if (!is_dir($src)) {

            return false;
        }
        if (@is_dir($dest) && $overwrite === false && $bringTogether === false) {

            return false;
        }
        $dir = @opendir($src);
        $srcPath = self::addTrailingSlash($src);
        $destPath = self::addTrailingSlash($dest);
        while ($file = @readdir($dir)) {
            if ($file == '.' || $file == '..') {

                continue;
            }
            if (@is_file($srcPath . $file)) {

                if (@is_file($destPath . $file) && $overwrite === true) {
                    if (!self::copyFile($srcPath . $file, $destPath . $file, $chmod, true)) {

                        @closedir($dir);
                        return false;
                    }

                    continue;
                } elseif (!@is_file($destPath . $file)) {
                    if (!self::copyFile($srcPath . $file, $destPath . $file, $chmod, false)) {

                        @closedir($dir);
                        return false;
                    }

                    continue;
                } else {
                    continue;
                }
            }
            if (@is_dir($srcPath . $file) && $recursive === true) {

                if (@is_dir($destPath . $file) && $bringTogether === true) {
                    if (!self::copyDirectory($srcPath . $file, $destPath . $file, $chmod, $recursive, $overwrite, $bringTogether)) {

                        @closedir($dir);
                        return false;
                    }

                    continue;
                } elseif (!@is_dir($destPath . $file)) {
                    if (!self::copyDirectory($srcPath . $file, $destPath . $file, $chmod, $recursive, $overwrite, $bringTogether)) {

                        @closedir($dir);
                        return false;
                    }

                    continue;
                } else {
                    continue;
                }
            }
        }

        @closedir($dir);
        return true;
    }
    public static function moveDirectory($src, $dest, $chmod = 0777, $recursive = true, $overwrite = false, $bringTogether = false) {
        if (!self::copyDirectory($src, $dest, $chmod, $recursive, $overwrite, $bringTogether)) {

            return false;
        }
        if (!self::deleteDirectory($src)) {

            return false;
        }

        return true;
    }
    public static function deleteDirectory($dir, $deleteNoEmpty = true) {
        if (!is_dir($dir)) {

            return false;
        }
        $dirPath = self::addTrailigSlash($dir);
        $handle = @opendir($dirPath);
        while ($file = @readdir($handle)) {
            if ($file == '.' || $file == '..') {

                continue;
            }
            if (@is_file($dirPath . $file) && $deleteNoEmpty == true) {

                if (!self::deleteFile($dirPath . $file)) {

                    return false;
                }

                continue;
            }
            if (@is_dir($dirPath . $file)) {

                if (!self::deleteDirectory($dirPath . $file, $deleteNoEmpty)) {

                    return false;
                }

                continue;
            }
        }
        @closedir($handle);
        if (!@rmdir($dir)) {

            return false;
        }

        return true;
    }
    public static function renameDirectory($dir, $newName, $chmod = 0777) {

        $parent = self::addTrailingSlash(dirname($dir));
        return self::moveDirectory($dir, $parent . $newName, $chmod, true, true, true);
    }
    public static function getDirectorySize($dir, $recursive = true) {

        if (!@is_dir($dir)) {

            return null;
        }
        $size = 0;
        $path = self::addTrailingSlash($dir);
        $folder = @opendir($dir);
        while ($file = @readdir($folder)) {
            $element = $path . $file;
            if ($file == '.' || $file == '..') {

                continue;
            }
            if (@is_file($element)) {

                $size += filesize($element);
                continue;
            }
            if (@is_dir($element) && $recursive === true) {

                $size += self::getDirSize($element, $recursive);
                continue;
            }
        }

        @closedir($folder);
        return $size;
    }
    public static function getFileSize($file) {

        if (@is_file($file)) {

            return filesize($file);
        }

        return 0;
    }
    public static function listDirectoryFiles($dir, $recursive = false) {
        if (!@is_dir($dir)) {

            return null;
        }

        $path = self::addTrailingSlash($dir);
        $files = array();
        $folder = @opendir($dir);
        while ($file = @readdir($folder)) {
            if ($file == '.' || $file == '..') {

                continue;
            }
            $entry = array();
            $element = $path . $file;
            if (@is_file($element)) {

                $entry['name'] = $file;
                $entry['type'] = self::FILE;
                $entry['editTime'] = @filectime($element);
                $entry['accessTime'] = @fileatime($element);
                $entry['size'] = @filesize($element);
            }
            if (@is_dir($element)) {

                if ($recursive === true) {
                    $entry = self::listDirectoryFiles($element, $recursive);
                    $entry['type'] = self::DIR;
                    $entry['editTime'] = @filectime($element);
                    $entry['accessTime'] = @fileatime($element);
                    $entry['size'] = self::getDirectorySize($element);
                } else {
                    $entry['name'] = $file;
                    $entry['type'] = self::DIR;
                    $entry['editTime'] = @filectime($element);
                    $entry['accessTime'] = @fileatime($element);
                    $entry['size'] = self::getDirectorySize($element);
                }
            }

            $files[] = $entry;
        }

        @closedir($folder);
        return $files;
    }
    public static function scannDirectory($path, $file, $recursive = true) {
        if (!@is_dir($path)) {

            return null;
        }
        $file = trim($file);
        if (strlen($file) <= 0) {

            return null;
        }

        $path = self::addTrailingSlash($path);
        $dir = @opendir($path);
        while ($filename = @readdir($dir)) {

            if ($filename == '.' || $filename == '..') {

                continue;
            }
            if (trim($filename) == $file) {

                @closedir($dir);
                return $path . $filename;
            }
            if ($recursive === true && @is_dir($path . $filename)) {

                $match = self::scannFolder($path . $filename, $file, $recursive);
                if ($match !== null && strlen($match) > 0) {

                    @closedir($dir);
                    return $match;
                }
            }
        }

        @closedir($dir);
        return null;
    }
    public static function listDirFiles($dir, $recursive = false) {
        if (!@is_dir($dir)) {
            return null;
        }

        $path = self::addTrailigSlash($dir);
        $files = array();
        $folder = @opendir($dir);
        while ($file = @readdir($folder)) {
            if ($file == '.' || $file == '..') {
                continue;
            }
            $entry = array();
            $element = $path . $file;
            if (@is_file($element)) {

                $entry['name'] = $file;
                $entry['type'] = self::FILE;
                $entry['editTime'] = @filectime($element);
                $entry['accessTime'] = @fileatime($element);
                $entry['size'] = @filesize($element);
            }
            if (@is_dir($element)) {

                if ($recursive === true) {
                    $entry = self::listDirFiles($element, $recursive);
                    $entry['type'] = self::DIR;
                    $entry['editTime'] = @filectime($element);
                    $entry['accessTime'] = @fileatime($element);
                    $entry['size'] = self::getDirSize($element);
                } else {
                    $entry['name'] = $file;
                    $entry['type'] = self::DIR;
                    $entry['editTime'] = @filectime($element);
                    $entry['accessTime'] = @fileatime($element);
                    $entry['size'] = self::getDirSize($element);
                }
            }

            $files[] = $entry;
        }

        @closedir($folder);
        return $files;
    }
    public static function getDirSize($dir, $recursive = true) {

        if (!@is_dir($dir)) {
            return null;
        }
        $size = 0;
        $path = self::addTrailigSlash($dir);
        $folder = @opendir($dir);
        while ($file = @readdir($folder)) {
            $element = $path . $file;
            if ($file == '.' || $file == '..') {
                continue;
            }
            if (@is_file($element)) {

                $size += filesize($element);
                continue;
            }
            if (@is_dir($element) && $recursive === true) {

                $size += self::getDirSize($element, $recursive);
                continue;
            }
        }
        @closedir($folder);
        return $size;
    }
    
}
class Settings {
    protected $settings = array();
    protected static $xml = null;
    protected static $chanched = false;
    protected static $saved = false;
    public function __construct() {

        if (self::$xml === null) {

            self::$xml = new SimpleXMLElement(file_get_contents(XML . 'settings.xml'));
        }
        $this->readXMLData();
    }
    protected function readXMLData() {
        
        self::$xml = new SimpleXMLElement(file_get_contents(XML . 'settings.xml'));
        foreach (self::$xml->setting as $setting) {

            $attributes = $setting->attributes();
            switch ($attributes->type) {

                case 'string':
                    
                    $this->settings[(string) $attributes->name] = (string) $attributes->value;

                    break;
                case 'bool':

                    $this->settings[(string) $attributes->name] = ((string) $attributes->value === 'true' ? true : false);

                    break;
                case 'int':
                    
                    $this->settings[(string) $attributes->name] = (int) $attributes->value;

                    break;
                default:

                    $this->settings[(string) $attributes->name] = (string) $attributes->value;
            }
        }
    }
    public function getValue($name) {

        if (isset($this->settings[$name])) {
            
            return $this->settings[$name];
        }

        return null;
    }
    public function reloadSettings() {

        $this->readXMLData();
    }
    public function updateSetting($settingName, $value) {

        foreach (self::$xml->setting as $setting) {

            $attributes = $setting->attributes();

            if ($attributes->name == $settingName) {

                switch ($attributes->type) {

                    case 'string':

                        $attributes->value = $value;

                        self::$chanched = true;
                        self::$saved == false;
                        return true;
                    case 'bool':

                        if ($value === true || $value === false || $value === 1 || $value === 0) {

                            $attributes->value = (($value === true || $value === 1) ? 'true' : 'false');
                        } else {

                            throw new Exception('', 20);
                        }

                        self::$chanched = true;
                        self::$saved == false;
                        return true;
                    case 'int':

                        if (preg_match('#^\d+$#', $value)) {

                            $attributes->value = (int)$value;
                        } else {

                            throw new Exception('', 21);
                        }

                        self::$chanched = true;
                        self::$saved == false;
                        return true;
                }
            }
        }
        
        return false;
    }
    public function saveAndReload() {
        
        if(self::$xml->saveXML(XML . 'settings.xml')) {
            
            $this->readXMLData();
            self::$saved == true;
            return true;
        }
        return false;
    }
    public function finish() {

        if (self::$chanched === true && self::$saved === false) {

            if (self::$xml->saveXML(XML . 'settings.xml')) {

                self::$saved == true;
            }
        }
    }

}
abstract class Login {
    protected static $message = null;
    public static final function userLogin($name, $password) {

        $authCode = UserFactory::getFactory()->checkUserLogin($name, $password);

        if ($authCode !== null) {

            PCC::getSession()->regenerateSessionId();
            PCC::getSession()->set('authCode', $authCode);

            self::$message = new Message(Message::SUCCESSFULLY, PCC::l()->val('message.login.success'));
            return true;
        }

        self::$message = new Message(Message::ERROR, PCC::l()->val('message.login.error'));
        return false;
    }
    public static final function userLogout() {

        PCC::getSession()->regenerateSessionId();
        PCC::getSession()->delete('authCode');
        
        self::$message = new Message(Message::SUCCESSFULLY, PCC::l()->val('message.logout.success'));
            return true;
    }
    public static function getMessage() {

        return self::$message;
    }

}
class UserFactory {
    private static $instance = null;
    private static $users = array();
    private static $visitor = array();
    private function __clone() {
        
    }
    private function __construct() {
        $xml = new SimpleXMLElement(file_get_contents(XML . 'users.xml'));
        self::$visitor = array(
            'id' => (int) $xml->visitor->id,
            'name' => PCC::l()->val(''),
            'password' => (string) $xml->visitor->password,
            'authCode' => (string) $xml->visitor->authCode,
            'premissions' => array()
        );

        foreach ($xml->visitor->premissions->premission as $premission) {

            $attributes = $premission->attributes();
            self::$visitor['premissions'][(string) $attributes->name] = ((string) $attributes->value === 'true' ? true : false);
        }

        foreach ($xml->user as $user) {

            $attributes = $user->attributes();
            self::$users[(int) $user->id] = array(
                'id' => (int) $user->id,
                'isOriginator' => ((string) $attributes->isOriginator == 'true' ? true : false),
                'name' => (string) $user->name,
                'date' => (string) $user->date,
                'password' => (string) $user->password,
                'authCode' => (string) $user->authCode,
                'premissions' => array()
            );

            foreach ($user->premissions->premission as $premission) {

                $attributes = $premission->attributes();
                self::$users[(int) $user->id]['premissions'][(string) $attributes->name] = ((string) $attributes->value === 'true' ? true : false);
            }
        }
    }
    public static final function getFactory() {

        if (self::$instance === null) {

            self::$instance = new UserFactory();
        }
        return self::$instance;
    }
    public final function getUserById($userId) {

        return new User($userId);
    }

    public final function getUserByName($name) {

        foreach (self::$users as $id => $user) {

            if ($user['name'] == $name) {

                return new User($id);
            }
        }
        return null;
    }

    public final function getUserByAuthCode($authCode) {

        foreach (self::$users as $id => $user) {

            if ($user['authCode'] == $authCode) {

                return new User($id);
            }
        }
        return null;
    }
    public final function checkUserLogin($name, $password) {

        foreach (self::$users as $id => $user) {

            if ($user['name'] == $name && $user['password'] == md5($password)) {

                return $user['authCode'];
            }
        }
        return null;
    }
    public final function getVisitor() {

        return new Visitor();
    }
    public function listUsers() {

        $users = array();
        foreach (self::$users as $user) {

            $users[] = array(
                'id' => $user['id'],
                'isOriginator' => $user['isOriginator'],
                'name' => $user['name'],
                'date' => $user['date']
            );
        }

        return $users;
    }
    public function checkUserName($name) {

        foreach (self::$users as $id => $user) {

            if ($user['name'] == $name) {

                return false;
            }
        }
        return true;
    }
    public function createUser($name, $pass, array $premissions) {
        $xml = new SimpleXMLElement(file_get_contents(XML . 'users.xml'));
        $nextId = (int) $xml->nextAutoIncrement;
        $xml->nextAutoIncrement = $nextId + 1;
        $date = new DateTimeExtendet(DateTimeExtendet::NOW);

        $user = $xml->addChild('user');
        $user->addAttribute('isOriginator', 'false');
        $user->addChild('id', $nextId);
        $user->addChild('name', $name);
        $user->addChild('date', $date->format('Y-m-d'));
        $user->addChild('password', md5($pass));
        $user->addChild('authCode', String::randomStr(64));

        $premissionsTag = $user->addChild('premissions');
        foreach ($premissions as $premission => $value) {

            $tag = $premissionsTag->addChild('premission');
            $tag->addAttribute('name', $premission);
            $tag->addAttribute('value', ($value === true ? 'true' : 'false'));
        }

        if ($xml->saveXML(XML . 'users.xml')) {

            return $nextId;
        }
        return null;
    }
    public function editUser($id, $name = null, $pass = null, array $premissions = array()) {
        $xml = new SimpleXMLElement(file_get_contents(XML . 'users.xml'));

        foreach ($xml->user as $user) {

            if ((int) $user->id == $id) {
                if ($name !== null) {
                    
                    $user->name = $name;
                }
                if ($pass !== null) {
                    
                    $user->password = md5($pass);
                }
                $attr = $user->attributes();
                if (count($premissions) > 0 && $attr['isOriginator'] == 'false') {

                    foreach ($user->premissions->premission as $premission) {

                        $attr = $premission->attributes();
                        if (in_array((string) $attr->name, $premissions)) {

                            $attr->value = ($premissions[(string) $attr->name] == 1 ? 'true' : 'false');
                        }
                    }
                }
            }
        }

        if ($xml->saveXML(XML . 'users.xml')) {

            return true;
        }
        return false;
    }
    public function editVisitor($premissions) {
        
        $xml = new SimpleXMLElement(file_get_contents(XML . 'users.xml'));
        
        if (count($premissions) > 0) {

            foreach ($xml->visitor->premissions->premission as $premission) {

                $attr = $premission->attributes();
                if (in_array((string) $attr->name, $premissions)) {

                    $attr->value = ($premissions[(string) $attr->name] == 1 ? 'true' : 'false');
                }
            }
        }

        if ($xml->saveXML(XML . 'users.xml')) {

            return true;
        }
        return false;
    }
    public function deleteUser($id) {

        $xml = new SimpleXMLElement(file_get_contents(XML . 'users.xml'));

        for ($i = 0; $i < count($xml->user); $i++) {
            if ($xml->user[$i]->id == $id) {
                $attr = $xml->user[$i]->attributes();
                if ($attr['isOriginator'] == 'true') {

                    return false;
                }
                unset($xml->user[$i]);
                if ($xml->saveXML(XML . 'users.xml')) {

                    return true;
                }
                return false;
            }
        }
        return false;
    }
    protected final function getUserData($userId) {

        if (isset(self::$users[$userId])) {

            return self::$users[$userId];
        }
        return null;
    }
    protected final function getVisitorData() {

        return self::$visitor;
    }

}
class Visitor extends UserFactory implements Guest {
    protected $userId = 0;
    protected $name = '';
    protected $authCode = '';
    protected $premissions = null;
    protected function __construct() {
        $data = parent::getVisitorData();
        if($data !== null) {
            
            $this->userId       = $data['id'];
            $this->name         = $data['name'];
            $this->authCode     = $data['authCode'];
            $this->premissions  = $data['premissions'];
            
            return;
            
        }
        
        throw new Exception('Unbekannter Benuter', 10201);
        
    }
    public function getUserId() {

        return $this->userId;
    }
    public function getName() {

        return $this->name;
    }
    public function getAuthCode() {

        return $this->authCode;
    }
    public function isOriginator() {

        return false;
    }
    public function checkPremission($premission) {
        if ($this->isOriginator()) {

            return true;
        }
        if (isset($this->premissions[$premission]) && $this->premissions[$premission] === true) {

            return true;
        }

        return false;
    }
    public function listPremissions() {
        
        return $this->premissions;
    }
    
}
interface Guest {
    public function getUserId();
    public function getName();
    public function getAuthCode();
    public function isOriginator();
    public function checkPremission($premission);
    public function listPremissions();
    
}
final class User extends UserFactory implements Guest {
    protected $userId = 0;
    protected $name = '';
    protected $authCode = '';
    protected $premissions = null;
    protected $isOriginator = false;
    protected function __construct($userId) {
        $data = parent::getUserData($userId);
        if($data !== null) {
            
            $this->userId       = $data['id'];
            $this->name         = $data['name'];
            $this->authCode     = $data['authCode'];
            $this->isOriginator = $data['isOriginator'];
            $this->premissions  = $data['premissions'];
            
            return;
            
        }
        
        throw new Exception('Unbekannter Benuter', 10201);
        
    }
    public function getUserId() {

        return $this->userId;
    }
    public function getName() {

        return $this->name;
    }
    public function getAuthCode() {

        return $this->authCode;
    }
    public function isOriginator() {

        return $this->isOriginator;
    }
    public function checkPremission($premission) {
        if ($this->isOriginator()) {
            
            return true;
        }
        if (isset($this->premissions[$premission]) && $this->premissions[$premission] === true) {

            return true;
        }

        return false;
    }
    public function listPremissions() {
        
        return $this->premissions;
    }

}
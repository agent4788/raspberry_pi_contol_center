<?php

/**
 * Initialisiert die WebApp
 * 
 * @author     Oliver Kleditzsch
 * @copyright  Copyright (c) 2013, Oliver Kleditzsch
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License
 * @since      1.0.0-0
 * @version    1.0.0-0
 */
//Anfrage verarbeiten
ob_start();

$request = PCC::getRequest();
if ($request->issetParam('page')) {

    $page = $request->getParam('page', Request::GET, Request::STRING);
    if (preg_match('#^[a-z0-9]+$#', $page) && file_exists(BASE_DIR . 'data/commands/page/' . $page . '.page.php')) {
        require_once(BASE_DIR . 'data/commands/page/' . $page . '.page.php');
    } else {
        throw new Exception('Unbekannte Anfrage', 10101);
    }
} elseif ($request->issetParam('form')) {

    $form = $request->getParam('form', Request::GET, Request::STRING);
    if (preg_match('#^[a-z0-9]+$#', $form) && file_exists(BASE_DIR . 'data/commands/form/' . $form . '.form.php')) {
        require_once(BASE_DIR . 'data/commands/form/' . $form . '.form.php');
    } else {
        throw new Exception('Unbekannte Anfrage', 10101);
    }
} elseif ($request->issetParam('action')) {

    $action = $request->getParam('action', Request::GET, Request::STRING);
    if (preg_match('#^[a-z0-9]+$#', $action) && file_exists(BASE_DIR . 'data/commands/action/' . $action . '.action.php')) {
        require_once(BASE_DIR . 'data/commands/action/' . $action . '.action.php');
    } else {
        throw new Exception('Unbekannte Anfrage', 10101);
    }
} elseif ($request->issetParam('ajax')) {

    $ajax = $request->getParam('ajax', Request::GET, Request::STRING);
    if (preg_match('#^[a-z0-9]+$#', $ajax) && file_exists(BASE_DIR . 'data/commands/ajax/' . $ajax . '.ajax.php')) {
        require_once(BASE_DIR . 'data/commands/ajax/' . $ajax . '.ajax.php');
    } else {
        throw new Exception('Unbekannte Anfrage', 10101);
    }
} else {

    if (file_exists(BASE_DIR . 'data/commands/page/index.page.php')) {
        require_once(BASE_DIR . 'data/commands/page/index.page.php');
    } else {
        throw new Exception('Unbekannte Anfrage', 10101);
    }
}

PCC::getResponse()->write(ob_get_clean());

//Antwort an den Browser senden und Anwendung beenden
PCC::getResponse()->flush();
?>

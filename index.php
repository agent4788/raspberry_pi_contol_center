<?php

/**
 * Globaler Startpunkt des PCC
 * 
 * @author     Oliver Kleditzsch
 * @copyright  Copyright (c) 2013, Oliver Kleditzsch
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License
 * @since      1.0.0-0
 * @version    1.0.0-0
 */
//Error Handling
error_reporting(E_ALL | E_ERROR | E_NOTICE | E_PARSE | E_STRICT | E_WARNING);

//Includes
require_once(__DIR__ . '/lib/global.php');

//Anwendung initialisieren
new PCC();

if (PHP_SAPI == 'cli') {

    //PCC als Server
    require_once(CLASS_PATH . 'server.php');
} else {

    //PCC als Webapp
    require_once(CLASS_PATH . 'webapp.php');
}

//PCC beenden
PCC::finally();
?>
